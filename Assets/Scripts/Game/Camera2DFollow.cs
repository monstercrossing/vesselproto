using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
        public float verticalOffset = 0;
        public float speed = 15;

        public float lookingDownOffset = -2.5f;
        public float fallingOffset = 1.5f;
        public float lookingUpOffset = 4;

        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;

        private Vector3 _desiredPosition;

        // Use this for initialization
        private void Start()
        {
            if (!target)
                target = GameObject.FindGameObjectWithTag("Hero").transform;
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
        }

        public void SetTargetTo(Transform newTarget) {
            target = newTarget;
        }
        public void SetTargetTo(MonoBehaviour callerObject) {
            target = callerObject.transform;
        }
        // Update is called once per frame
        private void FixedUpdate()
        {
            Animator animator = target.GetComponent<Animator>();
            float modifiedVerticalOffset = verticalOffset;
            if (animator) {
                if (animator.GetBool("PressingDown")) modifiedVerticalOffset = lookingDownOffset;
                else if (animator.GetBool("Jumping")) modifiedVerticalOffset = verticalOffset;
                else if (animator.GetBool("Falling")) modifiedVerticalOffset = fallingOffset + (0.1f  * animator.GetFloat("VerticalVelocity"));
                else if(animator.GetFloat("VerticalInput") > 0) modifiedVerticalOffset = verticalOffset + (lookingUpOffset * animator.GetFloat("VerticalInput"));

            }
            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ + new Vector3(0, modifiedVerticalOffset, 0);
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, Time.deltaTime * damping );

            //transform.position = newPos;
            //_desiredPosition = newPos;
            Rigidbody2D _body = GetComponent<Rigidbody2D>();
            if(_body && _body.isKinematic == false) {
                                
                /*
                if(animator) {
                    float charspeed = animator.GetFloat("AbsoluteVelocity");
                    if (charspeed > 0.5f) speed = charspeed;
                }
                */
                _body.velocity = new Vector2( (newPos.x - transform.position.x), newPos.y - transform.position.y) * speed;
            } else {
                transform.position = newPos;
            }
            //transform.position = Vector3.MoveTowards(transform.position, newPos, 1);

            m_LastTargetPosition = target.position;
        }

        /*
        private void OnCollisionStay2D(Collision2D collision) {
            for (int i = 0; i < collision.contactCount; i++) {
                ContactPoint2D contact = collision.GetContact(i);
                //Debug.Log(contact.normalImpulse);
            }
        }
        */
    }
}
