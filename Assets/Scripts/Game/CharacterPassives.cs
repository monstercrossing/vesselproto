﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class LocalPermanentInfo {
    public PermanentDefinition definition;

    public bool discovered;
    public bool queued;
    public bool collected;
    public bool equiped;

    public LocalPermanentInfo(PermanentDefinition def) {
        definition = def;
        discovered = false;
        collected = false;
        equiped = false;
        queued = false;
    }

    public LocalPermanentInfo(SerializablePermanentInfo info, PermanentDefinition def) {
        definition = def;
        discovered = info.discovered;
        queued = info.queued;
        collected = info.collected;
        equiped = info.equiped;
    }
}

[System.Serializable]
public class SerializablePermanentInfo {
    public bool discovered;
    public bool queued;
    public bool collected;
    public bool equiped;
    public int definitionIndex;
    //

    public SerializablePermanentInfo(bool d, bool q, bool c, bool e, int i) {
        this.discovered = d;
        this.queued = q;
        this.collected = c;
        this.equiped = e;
        this.definitionIndex = i;
    }

    public SerializablePermanentInfo(LocalPermanentInfo local, int i) {
        this.discovered = local.discovered;
        this.queued = local.queued;
        this.collected = local.collected;
        this.equiped = local.equiped;
        this.definitionIndex = i;
    }
}

class PermanentComparer : IEqualityComparer<LocalPermanentInfo>
{
    public bool Equals(LocalPermanentInfo x, LocalPermanentInfo y)
    {
        // Two items are equal if their keys are equal.
        return x.definition == y.definition;
    }

    public int GetHashCode(LocalPermanentInfo obj)
    {
        return obj.definition.GetHashCode();
    }
}

public class CharacterPassives : MonoBehaviour {
    public int equipMax = 3;
    public int currentlyQueued = 0;
    public GameEvent OnNewEgoReceived;

    public List<PermanentDefinition> equippedPassives = new List<PermanentDefinition>();
    public List<PermanentDefinition> collectedPassives = new List<PermanentDefinition>();
    public List<PermanentDefinition> egoQueue = new List<PermanentDefinition>();

    public PermanentPool listObject;
    public PermanentPool extraList;
    public List<PermanentDefinition> fullList = new List<PermanentDefinition>();

    [SerializeField]
    private List<LocalPermanentInfo> _passives = new List<LocalPermanentInfo>();

    public bool listBuilt = false;
    // Use this for initialization
    void Awake() {
        //if (extraList && listObject) fullList = listObject.fullList.Concat(extraList.fullList).ToList();
        if (listObject) fullList = listObject.fullList;
        
        //BuildPassivesList();
        //ApplyValues();
    }

    public void Initialize(List<LocalPermanentInfo> list) {
        /*
        if (list.Count > 0)
            _passives = list.Distinct(new PermanentComparer()).ToList();
        else
            BuildPassivesList();
        ApplyValues();
        */
        BuildPassivesList();
        if (list.Count > 0)
            _passives = list.Distinct(new PermanentComparer()).ToList();
        else
            SetPassivesFromLists();
    }

    public List<SerializablePermanentInfo> SerializeList() {
        //_passives = _passives.Distinct(new PermanentComparer()).ToList();
        List<SerializablePermanentInfo> serializedList = new List<SerializablePermanentInfo>();
        for(int i = 0; i < _passives.Count; i++) {
            int index = fullList.IndexOf(_passives[i].definition);

            serializedList.Add(new SerializablePermanentInfo(_passives[i], index) ) ;
        }
        return serializedList;
    }

    public void DeserializeList(List<SerializablePermanentInfo> list) {
        List<LocalPermanentInfo> newList = new List<LocalPermanentInfo>();

        for(int i = 0; i < fullList.Count; i++) {
            if (i < list.Count) {
                Log.Text("Deserializing info:", list[i].discovered);
                newList.Add(new LocalPermanentInfo(list[i], fullList[list[i].definitionIndex]));
            } else {
                LocalPermanentInfo newEgo = new LocalPermanentInfo(fullList[i]);
                if (fullList[i].autoCollect) {
                    newEgo.collected = true;
                    newEgo.discovered = true;
                }
                newList.Add(newEgo);
            }
        }

        _passives = newList;//.Distinct(new PermanentComparer()).ToList();
        //return newList;
    }

    public List<LocalPermanentInfo> passives {
        get {
            return _passives;
        }
    }

    public void TogglePassive(PermanentDefinition ego) {
        //push ego to collected
        Log.Text("<color=red>Will try to toggle this passive: </color",ego.name);
        int equipped = 0;
        for (int i = 0; i < _passives.Count; i++) {
            if(_passives[i].equiped) equipped++;
        }

        for (int i = 0; i < _passives.Count; i++) {
            if (_passives[i].definition == ego) {

                if(_passives[i].equiped) {
                    _passives[i].equiped = false;
                    _passives[i].definition.Unregister(this);
                    //equippedPassives.Remove(_passives[i].definition);
                }else if(_passives[i].collected) {
                    if(equipped < equipMax) {
                        _passives[i].equiped = true;
                        _passives[i].definition.ApplyValues(this);
                        //equippedPassives.Add(_passives[i].definition);
                    }
                }
            }
        }
    }

    public virtual void BuildPassivesList() {
        listBuilt = true;
        _passives = new List<LocalPermanentInfo>();
        for (int i = 0; i < fullList.Count; i++) {
            LocalPermanentInfo ego = new LocalPermanentInfo(fullList[i]);
            _passives.Add(ego);
        }
        //_passives = _passives.Distinct(new PermanentComparer()).ToList();
    }

    public void SetPassivesFromLists() {
        for (int i = 0; i < _passives.Count; i++) {
            LocalPermanentInfo ego = _passives[i];
            if( equippedPassives.Contains(ego.definition)) {
                ego.equiped = true;
                ego.collected = true;
                ego.discovered = true;
            } 
            if(collectedPassives.Contains(ego.definition)) {
                ego.collected = true;
                ego.discovered = true;
            }
            if (egoQueue.Contains(ego.definition)) {
                ego.queued = true;
                ego.discovered = true;
            }
        }
    }

    public void ReceiveEgo(PermanentDefinition ego) {
        //if ego not in collected
        //bool found = false;
        if (QueueFull()) return;
        for (int i = 0; i < _passives.Count; i++) {
            if (_passives[i].definition == ego) {
                
                if(!_passives[i].collected && !_passives[i].queued) {
                    _passives[i].discovered = true;
                    _passives[i].queued = true;
                    egoQueue.Add(ego);
                    currentlyQueued += 1;

                    if (OnNewEgoReceived) OnNewEgoReceived.Raise();
                }
                //found = true;
                break;
            }
        }
        /*
        if (!found) {
            egoQueue.Add(ego);
        }
        */
        //push ego to queue
    }

    public bool HasEquipped(PermanentDefinition passive) {
        for (int i = 0; i < _passives.Count; i++) {
            if (_passives[i].definition == passive && _passives[i].equiped) {
                return true;
            }    
        }
        return false;
    }

    public bool QueueFull() {
        return (currentlyQueued >= equipMax);
    }

    public bool HasEgo(PermanentDefinition ego) {
        for (int i = 0; i < _passives.Count; i++) {
            if (_passives[i].definition == ego && _passives[i].collected) {
                return true;
            }    
        }
        return false;
    }

    public bool IsEgoQueued(PermanentDefinition ego) {
        for (int i = 0; i < _passives.Count; i++) {
            if (_passives[i].definition == ego && _passives[i].queued) {
                return true;
            }
        }
        return false;
    }

    public void ResetQueue() {
        foreach (LocalPermanentInfo perm in _passives) {
            perm.queued = false;
        }
        egoQueue.Clear();
        currentlyQueued = 0;
    }

    public void RedeemEgo(PermanentDefinition ego) {
        //push ego to collected
        for (int i = 0; i < _passives.Count; i++) {
            if (_passives[i].definition == ego && _passives[i].queued) {
                _passives[i].collected = true;
                _passives[i].queued = false;
            }
        }
        
        //collectedPassives.Add(ego);
        //ResetQueue(); 
    }

    public void ApplyValues() {
        //Animator animator = transform.GetOrAddComponent<Animator>();
        /*
        for (int i = 0; i < equippedPassives.Count; i++) {
            //permanents[i].ApplyValuesToAnimator(animator);
            equippedPassives[i].ApplyValues(this);
        }
        */
        for (int i = 0; i < _passives.Count; i++) {
            if(_passives[i].equiped)
                _passives[i].definition.ApplyValues(this);
        }
    }

    public void ReceiveAndEquipEgo(PermanentDefinition ego)
    {
        for (int i = 0; i < _passives.Count; i++)
        {
            if (_passives[i].definition == ego)
            {
                _passives[i].discovered = true;
                _passives[i].queued = false;
                _passives[i].collected = true;
                _passives[i].equiped = true;

                equippedPassives.Add(ego);

                if (OnNewEgoReceived) OnNewEgoReceived.Raise();

                ApplyValues();

                break;
            }
        }
        
    }

}
