﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BaseCharacter))]
public class CharacterLevelScale : MonoBehaviour
{
    public List<Texture2D> palettes;
    public int hpModifier = 1; //base HP + this * level
    // Start is called before the first frame update
    private float demotionChance = 0.3f;
    BaseCharacter character;
    void Start()
    {
        character = GetComponent<BaseCharacter>();
        GameObject gameLevel = GameObject.FindWithTag("Level");
        if(gameLevel) {
            LevelGenerator gen = gameLevel.GetComponent<LevelGenerator>();
            if(gen) {

                int currentLevel = gen.enemyLevel;

                /* Demoção aqui:
                bool demoted;
                do {
                    demoted = Random.value <= demotionChance;
                    if (demoted && currentLevel > 1) currentLevel--;
                } while (demoted && currentLevel > 1);
                */
                bool demoted = Random.value <= demotionChance;
                if (demoted && currentLevel > 1) currentLevel--;

                int currentHP = character.Hitpoints()[0];
                character.ResetHitpoints(currentHP + (hpModifier * (currentLevel-1)) );

                if(palettes[currentLevel-1] != null) {
                    SetNewPallete(palettes[currentLevel-1]);
                }

                PermanentPool[] pools = {
                    gen.generalEgos,
                    gen.lvl1Egos,
                    gen.lvl2Egos,
                    gen.lvl3Egos,
                    gen.lvl4Egos
                };
                int r = Random.Range(0, pools[0].fullList.Count + pools[currentLevel].fullList.Count);
                if(r < pools[0].fullList.Count) {
                    character.ego = pools[0].fullList[r];
                }else {
                    r = r - pools[0].fullList.Count;
                    character.ego = pools[currentLevel].fullList[r];
                }
            }
        }    
    }

    void SetNewPallete(Texture2D palette) {
    SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
        if (sprite) {
            Material material = sprite.material;
            //Debug.Log("Has Material and Palette");
            material.SetTexture("_PaletteTex", palette);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
