﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {
    public GameObject objectToChange;
    public AudioClip clip;
    public PhysicsMaterial2D moving;
    public PhysicsMaterial2D stopped;

    private Collider2D _col;
    private Rigidbody2D _parentBody;
    public void Start() {
         _col = gameObject.GetComponent<Collider2D>();
        _parentBody = transform.parent.GetComponent<Rigidbody2D>();
    }
    public void OnCollisionEnter2D(Collision2D collision) {
        TestCollision(collision.collider);

        if(collision.gameObject.tag == "Platform") {
            //transform.parent.parent = collision.transform;
        }
        if (!objectToChange) objectToChange = gameObject;
    }

    private void OnCollisionExit2D(Collision2D collision) {
        //transform.parent.parent = null;
        Animator animator = gameObject.transform.parent.GetComponent<Animator>();
        if (animator) animator.SetBool("IsGrounded", false);
    }

    public void OnCollisionStay2D(Collision2D collision) {
        //if (!animator.GetBool("Jumping"))
            TestCollision(collision.collider);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        TestCollision(collision);
    }

    private void OnTriggerStay2D(Collider2D collision) {
        TestCollision(collision);
    }

    private void OnTriggerExit2D(Collider2D collision) {
        Animator animator = gameObject.transform.parent.GetComponent<Animator>();
        if (animator) animator.SetBool("IsGrounded", false);
    }

    private void TestCollision(Collider2D collision) {
        Animator animator = gameObject.transform.parent.GetComponent<Animator>();
        if (animator) animator.SetBool("IsGrounded", true);

        if (collision.gameObject.layer == LayerMask.NameToLayer("Terrain") ||
            collision.gameObject.layer == LayerMask.NameToLayer("Traps")) {
            if(animator && animator.GetBool("Stomping")) {
                animator.ResetTrigger("Stomp");
                animator.SetBool("Stomping", false);
            }
            SFXPlayer sfx = gameObject.transform.parent.GetComponent<SFXPlayer>();
            if(animator && animator.GetFloat("VerticalVelocity") < -2) {
                if (sfx && clip) {
                    sfx.PlaySound(clip);
                }
            }
            if (animator && !animator.GetBool("Jumping")) {
                animator.SetInteger("JumpCount", 0);
            }
            if (animator && !animator.GetBool("Dashing")) {
                animator.SetInteger("AirDashCount", 0);
            }

            Rigidbody2D _body = collision.gameObject.GetComponent<Rigidbody2D>();
            
            if (collision.gameObject.tag == "Platform" && animator.GetFloat("AbsoluteVelocity") <= 0) {
                if(!stopped || !_body) return;
                _body.sharedMaterial = stopped;
                _col.sharedMaterial = stopped;
            } else {
                if(!moving || !_body) return;
                _body.sharedMaterial = moving;
                _col.sharedMaterial = moving;
            }
        }
    }
}
