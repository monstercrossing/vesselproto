﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnEnable : MonoBehaviour {
    public UnityEvent onEnable;
    public UnityEvent onDisable;
    // Start is called before the first frame update
    void OnEnable() {
        onEnable.Invoke();
    }

    private void OnDisable() {
        onDisable.Invoke();
    }
}

