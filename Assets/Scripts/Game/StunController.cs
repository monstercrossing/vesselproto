﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunController : MonoBehaviour {
    public bool isStunned = false;
    public bool canBeStunned = true;
    private float stunTimer = 0.0f;
    CharacterEventHub eventHub;
    protected Animator _animator;

    [HideInInspector]
    public float queuedTime = 0;

    private void Awake() {
        eventHub = transform.GetOrAddComponent<CharacterEventHub>();
        _animator = transform.GetOrAddComponent<Animator>();
    }
    // Update is called once per frame
    void Update () {
		if (stunTimer > 0) {
            stunTimer -= Time.deltaTime;
        }
        if(stunTimer <= 0 && isStunned) {
            isStunned = false;
            eventHub.onStunEnd.Invoke();

           // /*  
            _animator.ResetTrigger("Knockback");
            _animator.SetBool("KnockedBack", false);
            _animator.SetBool("CanMove", true);
            //*/
        }
	}

    public void Stun(float time) {
        if (!canBeStunned) return;
        BaseCharacter character = transform.GetOrAddComponent<BaseCharacter>();

        if(character.invulnerable || character.IFrames()[0] > 0) return;
        bool alreadyStunned = isStunned;
        isStunned = true;
        stunTimer = time + queuedTime;
        queuedTime = 0;
        if(eventHub != null) {
            eventHub.onStunBegin.Invoke();
        }
        ///*
        _animator.SetBool("CanMove", false);
        if(!alreadyStunned) {
        _animator.SetTrigger("Knockback");
        _animator.SetBool("KnockedBack", true);
        }
        //*/
    }

    public void StunIfGrounded(float time) {
        if (_animator.GetBool("IsGrounded")) Stun(time);
    }
}
