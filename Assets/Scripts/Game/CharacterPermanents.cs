﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPermanents : CharacterPassives {
    public override void BuildPassivesList() {
        base.BuildPassivesList();
        for (int i = 0; i < passives.Count; i++) {
            passives[i].discovered = true;
        }
    }
}