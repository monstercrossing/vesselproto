﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDisableAfterTime : MonoBehaviour {
    public float timeToLive;
    // Start is called before the first frame update
    private void OnEnable() {
        StartCoroutine(AutoDisable(timeToLive));
    }

    IEnumerator AutoDisable(float time) {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
