﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundList {
    public string name;
    public float chance;
    public List<AudioClip> clips;
}

[RequireComponent(typeof(AudioSource))]
public class SFXPlayer : MonoBehaviour {
    public List<SoundList> lists;

    private AudioSource source;

	public void PlaySound(AudioClip clip) {
        source = GetComponent<AudioSource>();
        if(source) {
            source.clip = clip;
            source.loop = false;
            source.Play();
        }
    }

    public void PlaySoundEvent(string key) 
    {
        int index = -1;
        for (int i=0; i < lists.Count; i++) {
            if(lists[i].name == key) {
                index = i; break;
            }
        }
        if(index < 0) return;
        SoundList list = lists[index];
        bool trigger = Random.value <= list.chance;
        if(!trigger) return;

        AudioClip clip = list.clips[ Random.Range(0, list.clips.Count) ];
        source = GetComponent<AudioSource>();
        if(source) {
            source.clip = clip;
            source.loop = false;
            source.Play();
        }

    }
}
