﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR //Editor only tag
using UnityEditor;
#endif

public class ContactTrigger : MonoBehaviour {
    public UnityEvent OnContact;
    [TagSelector]
    public string[] interactableTags = new string[] { };
    protected virtual void OnTriggerEnter2D(Collider2D collision) {
        bool contains = false;
        for (int i = 0; i < interactableTags.Length; i++) {
            if(interactableTags[i] == collision.gameObject.tag) {
                contains = true;
                break;
            }
        }
        //if (ArrayUtility.Contains(interactableTags, collision.gameObject.tag)) {
        if(contains) { 
            OnContact.Invoke();
        }
    }
}
