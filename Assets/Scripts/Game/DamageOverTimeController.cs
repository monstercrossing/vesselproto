﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DamageOverTimeController : MonoBehaviour {

    private float timer = 0;
    private BaseCharacter character;

    [HideInInspector]
    public GameObject effect;
    public bool initialized = false;

    public void Initialize(GameObject effectObject) {
        if (initialized) return;
        initialized = true;
        character = transform.GetOrAddComponent<BaseCharacter>();

        effect = GameObject.Instantiate(effectObject);
        effect.transform.parent = transform;
        effect.transform.localPosition = new Vector3(0, 0.5f, 0);
    }

    // Update is called once per frame
    void Update() {
        if (timer > 0) {
            timer -= Time.deltaTime;
            if(timer <= 0) {
                if(effect)
                effect.SetActive(false);
            }
        }
    }

    /*
     * 
     * 
     * total  ---- time
     * 1 ---- x
     * x total = time 1
     * x = total / time
     * 
     * 
     * */

    public void ApplyDamageOverTime(int damage, float time) {
        float timePerTick = (float)damage / time;
        Log.Text("<color='magenta'>", damage, time, "</color>");
        if(effect)
            effect.SetActive(true);

        StartCoroutine(Tick(timePerTick, damage));
    }

    IEnumerator Tick(float time, int repeat) {
        for (int i = 0; i < repeat; i++) {
            yield return new WaitForSeconds(time);
            //APPLY DAMAGE
            character.DirectDamage(1, PowerupDefinition.AttackType.DoT);
        }

        if (effect)
            effect.SetActive(false);
    }

}
