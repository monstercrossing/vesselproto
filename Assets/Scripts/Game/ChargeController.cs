﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChargeController : MonoBehaviour {
    public bool isCharging = false;
    public bool canCharge;
    public bool fullCharge = false;
    public float chargeTimer = 1.0f;

    private float chargeClock;
    CharacterEventHub eventHub;

    private GameObject chargingAnimation;
    private GameObject chargedAnimation;

    public void Setup(float chargeTime, GameObject chargingAnimation, GameObject chargedAnimation) {
        this.chargeTimer = chargeTime;
        this.chargingAnimation = GameObject.Instantiate(chargingAnimation, transform.position + new Vector3(0,1,0), transform.rotation);
        this.chargedAnimation = GameObject.Instantiate(chargedAnimation, transform.position + new Vector3(0, 1, 0), transform.rotation);
        
        this.chargingAnimation.transform.parent = transform;
        this.chargedAnimation.transform.parent = transform;
        
        this.chargingAnimation.SetActive(false);
        this.chargedAnimation.SetActive(false);
    }

    public void BeginCharge() {
        if (isCharging) return;

        isCharging = true;
        chargeClock = chargeTimer;
        chargingAnimation.SetActive(true);
        chargedAnimation.SetActive(false);
    }

    public void StopCharge() {
        isCharging = false;
        chargingAnimation.SetActive(false);
        chargedAnimation.SetActive(false);
    }

    public void ReleaseChargedAttack() {
        StopCharge();
        if (!fullCharge) return;

        eventHub.onChargeRelease.Invoke();
        //eventHub.chargeLevel = 0;
        fullCharge = false;
        //ATTACK
    }

    public void ChargeReady() {
        chargingAnimation.SetActive(false);
        chargedAnimation.SetActive(true);
        fullCharge = true;
        eventHub.chargeLevel = 1;
    }

    private void Awake() {
        eventHub = transform.GetOrAddComponent<CharacterEventHub>();
    }
    
	void Update () {
	    if(isCharging && chargeClock > 0) {
            chargeClock -= Time.deltaTime;
        }
        if(isCharging && chargeClock <= 0) {
            ChargeReady();
        }
	}
}
