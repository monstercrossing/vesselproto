﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEventOnTime : MonoBehaviour {
    public float waitTime;
    public UnityEvent OnTimer;
    // Start is called before the first frame update
    void Start() {
        StartCoroutine(CallFunction());
    }

    private void OnEnable() {
        StartCoroutine(CallFunction());
    }

    IEnumerator CallFunction() {
        yield return new WaitForSeconds(waitTime);
        OnTimer.Invoke();
        yield return null;
    }
}
