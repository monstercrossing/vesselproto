﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateOnTrigger : MonoBehaviour
{
    public GameObject objectToInstantiate;
    // Start is called before the first frame update
    public void InstantiateObject(){
        if(!objectToInstantiate) return;
        GameObject obj = Instantiate(objectToInstantiate);
        obj.transform.position = transform.position;
    }
}
