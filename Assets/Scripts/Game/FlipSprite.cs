﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipSprite : MonoBehaviour {
    public bool isPlayer = false;
    public bool facingRight = true; //Depends on if your animation is by default facing right or left
    private Animator _animator;

    private float lockTimer = 0;
    private void Start() {
        _animator = GetComponent<Animator>();
    }

    void Update(){
        if(lockTimer > 0) {
            lockTimer -= Time.deltaTime;
            return;
        }

        float h = 0;
        if (isPlayer) {
            h = Input.GetAxis("Horizontal");
            if (_animator && !_animator.GetBool("CanMove")) h = 0;
        } else {
            if (_animator) {
                h =_animator.GetFloat("HorizontalVelocity");
            }
        }
        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (_animator) {
            _animator.SetBool("FacingRight", facingRight);
        }

        if(GameObject.Find("Canvas"))
        {
            //if(!facingRight)
            {
                Transform panel = transform.Find("Canvas/Panel");
                Transform text = transform.Find("Canvas/Text");
                if(panel)
                    panel.localScale = new Vector3(gameObject.transform.localScale.x, panel.localScale.y, panel.localScale.z);
                if(text)
                text.localScale = new Vector3(gameObject.transform.localScale.x, text.localScale.y, text.localScale.z);
            }
        }
     }

    public void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void Flip(float timer) {
        lockTimer = timer;
        Flip();
    }

}
