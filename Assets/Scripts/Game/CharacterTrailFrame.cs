﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTrailFrame : MonoBehaviour
{
    public float lifeTime = 1.2f;
    private float _timer = 0;

    public void Initialize(float _lifetime) {
        lifeTime = _lifetime;
        
        _timer = lifeTime;
    }

    // Update is called once per frame
    void Update() {
        if (_timer > 0) {
            _timer -= Time.deltaTime;


            if(_timer < 0) {
                Destroy(gameObject);
            }
        }
    }
}
