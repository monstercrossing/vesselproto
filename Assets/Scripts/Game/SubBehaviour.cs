﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SubBehaviour : MonoBehaviour {
    public abstract IEnumerator Run(MonoBehaviour callerObject);
}