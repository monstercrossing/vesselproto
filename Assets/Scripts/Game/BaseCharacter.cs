﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;
using UnityEngine.Events;

public class BaseCharacter : MonoBehaviour {
    public IntVariable maxHitpoints;
    public IntVariable armorValue;
    public FloatVariable moveSpeed;
    public bool invulnerable = false;
    public bool isBoss = false;

    [SerializeField]
    protected int _hp;
    protected float _iframes = .2f;
    protected float _iframesCount = 0f;
    protected float _queuedFrames = 0;
    protected int _armor = 0;
    protected int _comboCounter = 0;

    protected float _moveSpeed = 5;

    public PowerupDefinition startingPowerup;
    protected PowerupDefinition _currentPowerup;
    protected Animator _animator;
    protected Rigidbody2D _body;

    public CharacterEventHub eventHub;
    public GameEvent OnPauseMenuRequest;
    public PermanentPool egoList;
    public PermanentDefinition ego;

    //[HideInInspector]
    public UnityEvent onHitEvent;
    //[HideInInspector]
    public UnityEvent onKillConfirm;
    public UnityEvent onDeath;

    public FloatVariable baseStunValue;
    public FloatVariable baseKnockbackValue;
    public FloatVariable baseInvulnerabilityValue;

    private float _baseStun = 0.5f;
    private float _baseKnockback = 0.63f;
    
    public bool destroyOnDeath = true;
    protected bool alreadyDead = false;

    public int comboCounter {
        get { return _comboCounter; }
    }

    public float[] IFrames() {
        return new float[2] { _iframesCount, _iframes }; 
    }
    public int[] Hitpoints() {
        return new int[2] { _hp, maxHitpoints.Value};
    }

    public virtual float[] CooldownFrames() {
        return new float[2] { 0, 0 };
    }

    public virtual void Awake() {
        _currentPowerup = startingPowerup;
        _animator = GetComponent<Animator>();
        if (armorValue) _armor = armorValue.Value;
        UpdatePowerup();
        ResetHitpoints();
        _body = GetComponent<Rigidbody2D>();

        if (moveSpeed) _moveSpeed = moveSpeed.Value;

        eventHub = transform.GetOrAddComponent<CharacterEventHub>();
        _animator.SetFloat("MoveSpeed", _moveSpeed * eventHub.moveSpeedModifier);

        if (baseInvulnerabilityValue) _iframes = baseInvulnerabilityValue.Value;
        if (baseKnockbackValue) _baseKnockback = baseKnockbackValue.Value;
        if (baseStunValue) _baseStun = baseStunValue.Value;
        //onHitEvent = new UnityEvent();
    }

    public virtual void Start() {
        if(!ego && egoList) {
            int r = Random.Range(0, egoList.fullList.Count);
            ego = egoList.fullList[r];
        }
        CharacterPassives passives = GetComponent<CharacterPassives>();
        if (passives && !passives.listBuilt) {
            passives.BuildPassivesList();
            passives.ApplyValues();
        }

        StartCoroutine(NewEgoLoop());
    }

    IEnumerator NewEgoLoop() {
        Transform hud = transform.Find("hud_thisenemyhasanego");
        while (true) {
            if(!hud) hud = transform.Find("hud_thisenemyhasanego");
            HasNewEgo(hud);
            yield return new WaitForSeconds(1);
        }
    }

    public void HasNewEgo(Transform hud) {
        if (gameObject.tag != "Hero") {
            BasePlayer player = GameObject.FindGameObjectWithTag("Hero").GetComponent<BasePlayer>();
            if (player) {
                CharacterPassives otherPassives = player.GetComponent<CharacterPassives>();
                if (otherPassives) {
                    if (!otherPassives.HasEgo(ego) && !otherPassives.IsEgoQueued(ego) && !otherPassives.QueueFull()) {
                        if (hud) hud.gameObject.SetActive(true);
                    } else {
                        if (hud) hud.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    public virtual void Update() {
        if(alreadyDead) Destroy(gameObject);
        if(_iframesCount > 0) {
            _iframesCount -= Time.deltaTime;         
        }
        _animator.SetFloat("VerticalVelocity", _body.velocity.y);
        _animator.SetFloat("HorizontalVelocity", _body.velocity.x);
        _animator.SetFloat("AbsoluteVelocity", Mathf.Abs(_body.velocity.x));

        _animator.SetFloat("MoveSpeed", _moveSpeed * eventHub.moveSpeedModifier);
    }

    public void ResetHitpoints() {
        _hp = maxHitpoints.Value;
        //Debug.Log("RESET HP?" + _hp);

    }

    public void ResetHitpoints(int newValue) {
        _hp = newValue;
    }

    public void ResetHitpoints(int newValue, IntVariable newMax) {
        maxHitpoints = newMax;
        ResetHitpoints();
    }

    public void ResetIframes() {
        _iframesCount = _iframes + eventHub.iframesModifier + _queuedFrames;
        _queuedFrames = 0;
    }

    public virtual void ReceiveDamageAndUpdatePowerups(int damage, PowerupDefinition newPowerup, BaseCharacter attacker) {
        if (!attacker) {
            if ((_iframesCount > 0 || invulnerable)) return;
        } else if ((_iframesCount > 0 || invulnerable) && attacker.comboCounter <= 0) {
            return;
        }
        Log.Text("Damage Inflicted: ", damage);
        if (attacker) {
            float direction = attacker.transform.position.x > transform.position.x ? -3 : 3;
            TakeKnockback(_baseKnockback * (direction * transform.right) * (damage) * attacker.eventHub.inflictedKnockbackModifier,
                          _baseStun * damage * eventHub.stunModifier * attacker.eventHub.inflictedStunModifier,
                          _iframes * damage * eventHub.invincibilityModifier * attacker.eventHub.inflictedInvulnerabilityModifier);
        }
        _queuedFrames += damage;
        ResetIframes();
        CalculateDamage(damage, newPowerup);
        if (onHitEvent != null)
            onHitEvent.Invoke();

        CheckForDeath(attacker, newPowerup);
    }

    protected virtual void CheckForDeath(BaseCharacter attacker, PowerupDefinition powerup) {
        if (_hp <= 0) {
            if (attacker != null)
                attacker.onKillConfirm.Invoke();
            Die();
        }
    }


    protected void CalculateDamage(int damage, PowerupDefinition powerup) {
        Log.Text("<color=cyan>MITIGATORS FOR", gameObject.name, eventHub.GetDamageMitigatorsFor(powerup.attackType), "</color>");
        if (eventHub && powerup) {
            damage -= eventHub.GetDamageMitigatorsFor(powerup.attackType);
            eventHub.onDamageReceived.Invoke();
        }
        //damage -= _armor;
        if (damage < 0) damage = 0;
        _hp -= (damage - _armor);
    }

    public void DirectDamage(int damage, PowerupDefinition.AttackType type) {
        if (eventHub) {
            damage -= eventHub.GetDamageMitigatorsFor(type);
            eventHub.onDamageReceived.Invoke();
        }

       // damage -= _armor;
        if (damage < 0) damage = 0;
        _hp -= (damage - _armor);
        if(damage > 0 && onHitEvent != null)
                onHitEvent.Invoke();
    }

    public virtual void TakeKnockback(Vector3 direction, float stun, float invincibility) { 
        if (_iframesCount > 0 || invulnerable) return;
        Log.Text("Knockback:", direction);
        Animator animator = GetComponent<Animator>();
        /*
        float direction = 1;
        if (animator)
            direction = animator.GetBool("FacingRight") ? 1 : -1;
        */
        Debug.DrawRay(transform.position, direction, Color.magenta);
        Log.Text("<color=blue>Stunning</color>");
        ///*
        //_animator.SetBool("CanMove", false);
        // _animator.SetTrigger("Knockback");
        //_animator.SetBool("KnockedBack", true);
        //*/
        transform.GetOrAddComponent<StunController>().Stun(stun);
        _queuedFrames += invincibility;
        _body.velocity = new Vector2(0, 0);
        _body.AddForce(new Vector2(direction.x, direction.y), ForceMode2D.Impulse);
    }

    public virtual void Die() {
        onDeath.Invoke();
        alreadyDead = true;

        if(destroyOnDeath)
            GameObject.Destroy(gameObject);
    }

    public void ReceivePowerup(PowerupDefinition newPowerup) {
        _currentPowerup = newPowerup;
        UpdatePowerup();
    }

    public PowerupDefinition CurrentPowerup() {
        return _currentPowerup;
    }

    public void SetVulnerability(bool vulnerable)
    {
        this.invulnerable = !vulnerable;
    }

    public void SetMaxLife(IntVariable maxlife)
    {
        maxHitpoints = maxlife;
        ResetHitpoints();
    }

    public virtual void UpdatePowerup() {
        if (_currentPowerup && _animator) {
            _currentPowerup.ApplyValuesToAnimator(_animator);
        }
        if (_currentPowerup.projectilePrefab) {
            BaseRangedAttack attack = _currentPowerup.projectilePrefab.GetComponent<BaseRangedAttack>();
            ProjectileDefinition projectile = null;
            if (attack) projectile = attack.projectileDefinition;
            if (projectile) projectile.ApplyValuesToAnimator(_animator);
        }

    }

    public virtual void PostAnimShoot() {
        int combo = _animator.GetInteger("ComboCount");
        _comboCounter = combo;
        Attack(false);
    }

    public void Attack() {
        Attack(false);
    }
    public void Attack(bool freeVertical) {
        List<Rigidbody2D> bodies = new List<Rigidbody2D>();
        Rigidbody2D _thisbody = gameObject.GetComponent<Rigidbody2D>();
        if (Mathf.Abs(_thisbody.velocity.y) < 0.1f && !freeVertical)
            _thisbody.velocity = new Vector2(0, _thisbody.velocity.y);
        BaseCharacter baseCharacter = this;
        PowerupDefinition powerup = null;
        GameObject projectile = null;
        if (baseCharacter) powerup = baseCharacter.CurrentPowerup();
        if (powerup) projectile = powerup.projectilePrefab;

        if (projectile) {
            GameObject newProjectile;
            Transform hand = transform.Find("hand");
            Log.Text("Charge Level: ", eventHub.chargeLevel);
            if (eventHub.chargeLevel > 0 && powerup.chargedProjectilePrefab) {
                newProjectile = GameObject.Instantiate(powerup.chargedProjectilePrefab);
                eventHub.chargeLevel = 0;
            } else {
                newProjectile = GameObject.Instantiate(projectile);
            }
            newProjectile.transform.position = hand.position;
            newProjectile.transform.rotation = hand.rotation;
            if (powerup.isMelee) newProjectile.transform.parent = hand;
            Rigidbody2D _projectileBody = newProjectile.GetComponent<Rigidbody2D>();

            if (_projectileBody) {
                bodies.Add(_projectileBody);
            }
            if (newProjectile.transform.childCount > 0) {
                foreach (Transform child in newProjectile.transform) {
                    if (child.GetComponent<Rigidbody2D>())
                        bodies.Add(child.GetComponent<Rigidbody2D>());
                }
            }

            BaseCharacter character = gameObject.GetComponent<BaseCharacter>();
            BaseAttack attack = newProjectile.GetComponent<BaseAttack>();

            if (powerup.isMelee)
                eventHub.onMeleeAttack.Invoke(newProjectile);
            else
                eventHub.onRangedAttack.Invoke(newProjectile);

            if (character && attack) {
                attack.SetOwnership(character);
            }

            Animator animator = GetComponent<Animator>();
            float direction = 1;
            if (animator)
                direction = animator.GetBool("FacingRight") ? 1 : -1;
            
            if (direction == -1) {
                ///*
                Vector3 theScale = newProjectile.transform.localScale;
                theScale.x *= -1;
                newProjectile.transform.localScale = theScale;
                //*/
            }

            float _shootSpeed = 15;
            if (animator)
                 _shootSpeed = animator.GetFloat("ShootSpeed");
            foreach (Rigidbody2D _project in bodies) {
                //_project.rotation = hand.rotation.z;
                _project.AddForce(_project.transform.right * _shootSpeed * direction, ForceMode2D.Impulse);
                if (_project.gameObject.GetComponent<BaseAttack>())
                    _project.gameObject.GetComponent<BaseAttack>().SetOwnership(character);
            }
        }
    }
}
