﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterEvent : UnityEvent<BaseCharacter> { }
public class GameObjectEvent : UnityEvent<GameObject> { }

//TODO: DamageModifier Class, add them to list, use them on GetModifiers
public class DamageModifier : System.IEquatable<DamageModifier> {
    public int Id { get; set; }
    public System.Guid hash;
    public PowerupDefinition.AttackType flags;
    public int value;
    public override bool Equals(object obj) {
        if (obj == null) return false;
        DamageModifier objAsPart = obj as DamageModifier;
        if (objAsPart == null) return false;
        else return Equals(objAsPart);
    }
    public override int GetHashCode() {
        return Id;
    }
    public bool Equals(DamageModifier other) {
        if (other == null) return false;
        return (this.hash.Equals(other.hash));
    }
    public DamageModifier(int modValue, PowerupDefinition.AttackType flags) {
        this.hash = System.Guid.NewGuid();//.ToString("N");
        this.flags = flags;
        this.value = modValue;
    }
    // Should also override == and != operators.
}

public class CharacterEventHub : MonoBehaviour {
    public CharacterEvent onDashImpactApplied = new CharacterEvent();
    public CharacterEvent onDashStart = new CharacterEvent();
    public CharacterEvent onDashEnd = new CharacterEvent();
    public GameObjectEvent onRangedAttack = new GameObjectEvent();
    public GameObjectEvent onMeleeAttack = new GameObjectEvent();
    public UnityEvent onStunBegin = new UnityEvent();
    public UnityEvent onStunEnd = new UnityEvent();

    public CharacterEvent onDamageInflictedTo = new CharacterEvent();
    public CharacterEvent onAreaDamageInflictedTo = new CharacterEvent();
    public CharacterEvent onStunInflictedTo = new CharacterEvent();
    public CharacterEvent onPunchDamageInflictedTo = new CharacterEvent();
    public CharacterEvent onMeleeDamageInflictedTo = new CharacterEvent();
    public CharacterEvent onRangedDamageInflictedTo = new CharacterEvent();
    public CharacterEvent onGrenadeDamageInflictedTo = new CharacterEvent();

    public CharacterEvent onEnemyKill = new CharacterEvent();

    public UnityEvent onDamageReceived = new UnityEvent();

    public float fireRateModifier = 0; //Additive
    public float moveSpeedModifier = 1; //Multiplicative
    public int damageInflictedModifier = 0; //Additive
    public float iframesModifier = 0; //Additive
    public float dotModifier = 1; //Multiplicative
    public float aeoModifier = 0 ;// Additive

    public float timeScale = 1;

    public float stunModifier = 1; //Multiplicative
    public float invincibilityModifier = 1; //Multiplicative
    public float inflictedKnockbackModifier = 1; //Multiplicative
    public float inflictedStunModifier = 1;//Multiplicative
    public float inflictedInvulnerabilityModifier = 1; //Multiplicative
    public float inflictedDOTModifier = 1;

    public bool chargingEnabled = false;
    public bool secretEnabled = false;
    public int chargeLevel = 0;

    public UnityEvent onAttackHold = new UnityEvent();
    public UnityEvent onAttackRelease = new UnityEvent();

    public UnityEvent onChargeBegin = new UnityEvent();
    public UnityEvent onChargeReady = new UnityEvent();
    public UnityEvent onChargeRelease = new UnityEvent();

    public List<DamageModifier> damageModifiers = new List<DamageModifier>();
    public List<DamageModifier> damageMitigators = new List<DamageModifier>();

    public DamageModifier RegisterDamageModifier(int modValue, PowerupDefinition.AttackType flags) {
        DamageModifier mod = new DamageModifier(modValue, flags);
        damageModifiers.Add(mod);
        return mod;
    }
    public void RemoveDamageModifier(DamageModifier mod) {
        damageModifiers.Remove(mod);
    }
    public int GetDamageModifiersFor(PowerupDefinition.AttackType type) {
        int sum = 0;
        damageModifiers.ForEach(mod => {
            int check = (int)type & (int)mod.flags;
            if ( check != 0 || (int)mod.flags == (~0) ) {
                sum += mod.value;
            }
        });
        return sum;
    }

    public DamageModifier RegisterDamageMitigator(int modValue, PowerupDefinition.AttackType flags) {
        Log.Text("<color=yellow> Registering damage mitigator wth value", modValue, "and flags", flags, "</color>");
        DamageModifier mod = new DamageModifier(modValue, flags);
        damageMitigators.Add(mod);
        return mod;
    }
    public void RemoveDamageMitigator(DamageModifier mod) {
        damageMitigators.Remove(mod);
    }
    public int GetDamageMitigatorsFor(PowerupDefinition.AttackType type) {
        int sum = 0;
        damageMitigators.ForEach(mod => {
            int check = (int)type & (int)mod.flags;
            if (check != 0 || (int)mod.flags == (~0)) {
                sum += mod.value;
            }
        });
        return sum;
    }
}
