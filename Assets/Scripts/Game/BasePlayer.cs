﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class BasePlayer : BaseCharacter {
    public FloatVariable jumpHold;
    public IntVariable maxJumpCount;
    public FloatVariable jumpSpeedModifier;

    public GameEvent OnPowerupAbsorption;
    public GameObject DeathEffect;

    public UnityEvent OnInteractionInput;
    public UnityEvent OnBossDeath;
    public int deathCount = 0;

    private float _jumpHold = 5;
    private float _shootingCooldown = 0;
    private int _maxJumps = 2;
    private int _absorptionsLeft = 3;
    private int _maxAbsorptions = 3;
    private float _jumpSpeedMod = 0.5f;
    private bool _lockedInput = false;

    //private Rigidbody2D _body;

    private bool lastFrameJumpInput = false;
    private bool lastFrameDashInput = false;
    private bool lastFrameShootInput = false;
    private bool lastFrameInteractInput = false;
    private bool lastFrameDownInput = false;
    private bool lastFramePauseInput = false;

    private float lastFrameVertical = 0;

    public static BasePlayer Instance;
    private float wallclingtimer = 0;
    
    public int[] Absorptions() {
        return new int[2] { _absorptionsLeft, _maxAbsorptions};
    }

    // Use this for initialization
    public override void Awake() {
        /*
        if (Instance != null) {
            //Instance.gameObject.SetActive(true);
            ResetPlayerFromInstance(Instance);
            Destroy(Instance.gameObject);
        }
        //*/
        LoadPlayerInfoFromSave();
        Instance = this;
        //DontDestroyOnLoad(this);
     
        base.Awake();
        if (jumpSpeedModifier) _jumpSpeedMod = jumpSpeedModifier.Value;

        //_body = GetComponent<Rigidbody2D>();
        eventHub.onChargeRelease.AddListener(Shoot);
    }

    public override void Start() {
        base.Start();
        //GetComponent<CharacterPassives>().ApplyValues();
        //GetComponent<CharacterPermanents>().ApplyValues();
    }

    public void FullHeal() {
        _absorptionsLeft = _maxAbsorptions;
        ResetHitpoints();
    }

    /*
    public override void ReceiveDamageAndUpdatePowerups(int damage, PowerupDefinition newPowerup, BaseCharacter attacker) {
        base.ReceiveDamageAndUpdatePowerups(damage, newPowerup, attacker);
    }
    */

    protected override void CheckForDeath(BaseCharacter attacker, PowerupDefinition newPowerup) {
        if(alreadyDead) return;
        if (_hp <= 0 && _absorptionsLeft > 0) {
            this.ReceivePowerup(newPowerup);
            _absorptionsLeft--;
            ResetHitpoints();
            if (OnPowerupAbsorption) OnPowerupAbsorption.Raise();
            if (attacker != null)
                attacker.onKillConfirm.Invoke();
            if (attacker && attacker.ego) {
                this.GetOrAddComponent<CharacterPassives>().ReceiveEgo(attacker.ego);
            }
            _animator.SetTrigger("Transform");

        } else if (_hp <= 0 && _absorptionsLeft <= 0) {
            if (attacker != null)
                attacker.onKillConfirm.Invoke();
            this.Die(attacker);
        }

    }

    IEnumerator WaitThenDie(BaseCharacter attacker) {
        yield return new WaitForSeconds(0.5f);
        _animator.SetBool("CanMove", false);
        _body.velocity = Vector2.zero;
        _animator.SetTrigger("Die");
        if (DeathEffect) {
            GameObject fx = Instantiate(DeathEffect);
            fx.transform.position = transform.position;
        }

        //gameObject.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(1.5f);
        SaveGame();
        if (attacker != null && attacker.isBoss) {
            onDeath.Invoke();
            OnBossDeath.Invoke();
            //gameObject.SetActive(false);
            //gameObject.tag = "Untagged";
        } else {
            //LoadNewScene
            onDeath.Invoke();
            //gameObject.SetActive(false);
            //gameObject.tag = "Untagged";
            //LoadNewLevel();
        }
    }

    public void LoadNewLevel() {
        gameObject.AddComponent<SaveSlotController>().BeginNewGame(SaveSlotController.currentSlot);
    }

    public void LoadPlayerInfoFromSave() {
        SaveSlot save = SaveSlotController.GetCurrentSave();
        if (save == null) {
            //   SaveSlotController.LoadSlots();
            //   SaveSlotController.currentSlot = 0;
            //   save = SaveSlotController.GetCurrentSave();
            deathCount = 0;
        } else {
            deathCount = save.deathCount;
        }
        CharacterPassives characterPassives = GetComponent<CharacterPassives>();
        //if (save.egos == null) save.egos = new List<LocalPermanentInfo>();
        //characterPassives.Initialize(save.egos);
        characterPassives.BuildPassivesList();
        if (save != null && save.egos != null && !save.empty) {
            Log.Text("~~ Deserializing player passives");
            characterPassives.DeserializeList(save.egos);
        } else {
            characterPassives.SetPassivesFromLists();
        }
        characterPassives.ApplyValues();

        //characterPassives.equippedPassives = save.equippedPassives;
        // characterPassives.collectedPassives = save.collectedPassives;
        CharacterPermanents characterPermanents = GetComponent<CharacterPermanents>();
        //if (save.permanents == null) save.permanents = new List<LocalPermanentInfo>();
        //characterPermanents.Initialize(save.permanents);
        characterPermanents.BuildPassivesList();
        if (save != null && save.permanents != null && !save.empty) {
            characterPermanents.DeserializeList(save.permanents);
        } else {
            characterPermanents.SetPassivesFromLists();
        }
        characterPermanents.ApplyValues();

        //characterPermanents.equippedPassives = save.equippedPermanents;

        characterPassives.ResetQueue();
        characterPermanents.ResetQueue();
    }

    public void SetTutorialCompletion(bool completion) {
         SaveSlot save = SaveSlotController.GetCurrentSave();
         save.tutorialCompleted = completion;
         SaveGame();
    }
    public void MarkBossAsDefeated(int index){
        SaveSlot currentSave = SaveSlotController.GetCurrentSave();
        if(currentSave != null)
            currentSave.defeatedBosses[index] = true;
    }
    public void SaveGame() {
        SaveSlot save = SaveSlotController.GetCurrentSave();
        if (save == null) return;
        save.deathCount = deathCount;// + 1;
        save.permanents = GetComponent<CharacterPermanents>().SerializeList();
        save.egos = GetComponent<CharacterPassives>().SerializeList();
        //save.collectedPassives = GetComponent<CharacterPassives>().collectedPassives;

        save.empty = false;
        SaveSlotController.SetCurrentSave(save);
        SaveSlotController.SaveSlots();

        GameObject steamworksObject = GameObject.Find("steamworks");
        if(steamworksObject) {
            SteamAchievements achievements = steamworksObject.GetComponent<SteamAchievements>();
            if(achievements) {
                int totalEgos = save.egos.Count;
                int totalPermanents = save.permanents.Count;
                int collectedEgos = 0;
                int collectedPermanents = 0;

                for(int i=0; i < totalEgos; i++) {
                    if(save.egos[i].collected) collectedEgos++;
                }

                for(int i=0; i < totalPermanents; i++) {
                    if(save.permanents[i].collected) collectedPermanents++;
                }
                achievements.SetEgosCollected(collectedEgos);
                achievements.SetPermanentsCollected(collectedPermanents);
                achievements.SetDaysPlayed(save.deathCount);
            }
        }
    }

    public int[] Lives() {
        return new int[2] { _absorptionsLeft, _maxAbsorptions};
    }

    public override float[] CooldownFrames() {
        return new float[2] { _shootingCooldown, _animator.GetFloat("FireRate")};
    }

    public void ResetPlayerFromInstance(BasePlayer other) {
        deathCount = other.deathCount + 1;
        Destroy(GetComponent<CharacterPassives>());
        Destroy(GetComponent<CharacterPermanents>());

        CharacterPassives characterPassives = gameObject.AddComponent(other.GetComponent<CharacterPassives>());
        CharacterPermanents characterPermanents = gameObject.AddComponent(other.GetComponent<CharacterPermanents>());
        
        characterPassives.ResetQueue();
        characterPermanents.ResetQueue();
    }

    public void SetInputLockState(bool state) {
        _lockedInput = state;
    }

    public void StopPlayer() {
        _body.velocity = Vector2.zero;
        _animator.SetFloat("MoveSpeed",0);
        _animator.SetFloat("VerticalVelocity", 0);
        _animator.SetFloat("HorizontalVelocity", 0);
        _animator.SetFloat("AbsoluteVelocity", 0);
    }

    // Update is called once per frame
    public override void Update () {
        if(!_animator.GetBool("Dashing") && !UIPauseMenuController.anyMenuOpen && !LevelGenerator.generatingLevel && Time.timeScale <= 0) Time.timeScale = 1;
        if (alreadyDead || _lockedInput) return;
        
        //if (maxJumpCount) _maxJumps = maxJumpCount.Value;
        _maxJumps = _animator.GetInteger("MaxJumps");
       // _moveSpeed = _animator.GetFloat("MoveSpeed");
        //if (moveSpeed) _moveSpeed = moveSpeed.Value;
        if (jumpHold) _jumpHold = jumpHold.Value;
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        bool inShoot = Input.GetButton("Shoot");
        bool inJump = Input.GetButton("Jump");
        bool inDash = Input.GetButton("Dash");
        bool inAttack = Input.GetButton("Melee");
        bool inInteract = Input.GetButton("Interact") || Input.GetAxis("Vertical") > 0.5;
        bool inDown = vertical < -0.9f;//Input.GetButton("Down");
        bool inPause = Input.GetButtonUp("Pause");
        bool autoShoot = _animator.GetBool("AutoShoot");        
        bool canAttackWhileMoving = _animator.GetBool("CanAttackWhileMoving");

        if (inPause && !lastFramePauseInput && OnPauseMenuRequest
           && !_animator.GetBool("Jumping")
           && !_animator.GetBool("Dashing")
           && !_animator.GetBool("Falling"))
            OnPauseMenuRequest.Raise();
        lastFramePauseInput = inPause;

        if (Time.timeScale <= 0) return;

        int jumpCount = _animator.GetInteger("JumpCount");

        if(_animator.GetBool("IsGrounded")) _animator.SetBool("IsHuggingWall", false);

        if (Mathf.Abs(horizontal) > 0 && _animator.GetBool("IsHuggingWall")) {
            wallclingtimer += Time.deltaTime;
            if (wallclingtimer > 0.025f)
                _animator.SetTrigger("ClingToWall");
        } else {
            wallclingtimer = 0;
        }
         //_animator.SetBool("IsHuggingWall", false);

        _animator.ResetTrigger("Stomp");
        if(inJump && inDown && !lastFrameJumpInput)
            _animator.SetTrigger("Stomp");

        if (_animator.GetBool("IsHuggingWall") && Mathf.Abs(_body.velocity.x) == 0 && Mathf.Abs(horizontal) == 0) {
            _animator.SetBool("IsHuggingWall", false);
            _animator.SetBool("IsClinging", false);
        }
        if (_animator.GetBool("IsHuggingWall")) {
            _body.velocity = new Vector2(0, _body.velocity.y);
        }


        if (inJump && !inDown && !lastFrameJumpInput && 
        (jumpCount < _maxJumps || _animator.GetBool("IsClinging") || _animator.GetBool("IsHuggingWall")) ) {
            //_animator.SetInteger("JumpCount", jumpCount + 1);
            if (!_animator.GetBool("IsClinging") && _animator.GetBool("IsHuggingWall")) {
                Log.Text("Is Hugging but not clinging");
                if (horizontal == _animator.GetFloat("WallDirection")) {
                    Log.Text(_body.velocity.x, horizontal);
                    _animator.SetTrigger("Jump");
                } else if (jumpCount < _maxJumps){
                    _animator.SetBool("IsHuggingWall", false);
                    _animator.SetBool("IsClinging", false);
                    _animator.SetTrigger("Jump");
                } else {
                    _animator.SetBool("IsHuggingWall", false);
                    _animator.SetBool("IsClinging", false);
                }
            } else if (_animator.GetBool("IsClinging")){
                if (Mathf.Abs(_body.velocity.x) == 0 && Mathf.Abs(horizontal) > 0) {
                    _animator.SetTrigger("Jump");
                }
            } else {
                _animator.SetBool("IsHuggingWall", false);
                _animator.SetBool("IsClinging", false);
                _animator.SetTrigger("Jump");
            }
        }

        if(inDash && !lastFrameDashInput) {
            _animator.SetTrigger("Dash");
        }
        else {
            _animator.ResetTrigger("Dash");
        }

        if (inInteract && !lastFrameInteractInput)
            OnInteractionInput.Invoke();

        //if (inDown && !lastFrameDownInput)
            _animator.SetBool("PressingDown", inDown);
        vertical = Mathf.Lerp(lastFrameVertical, vertical, Time.deltaTime * 10);
        _animator.SetFloat("VerticalInput",vertical);

        if (_shootingCooldown > 0) {
            _shootingCooldown -= Time.deltaTime;
            _animator.SetBool("Attacking", true);
        }
        else {
            _animator.SetBool("Attacking", false);
        }

        bool validShootInput = !lastFrameShootInput || (autoShoot && !eventHub.chargingEnabled);
        if(inShoot && !lastFrameShootInput && eventHub.chargingEnabled) {
            eventHub.onAttackHold.Invoke();
        }
        if(lastFrameShootInput && !inShoot && eventHub.chargingEnabled) {
            eventHub.onAttackRelease.Invoke();
        }
        
        if(inShoot && _shootingCooldown <= 0 && validShootInput) {
            Shoot();
        } else {
           // _animator.ResetTrigger("Shoot");
        }

        base.Update();
        lastFrameJumpInput = inJump;
        lastFrameDashInput = inDash;
        lastFrameShootInput = inShoot;
        lastFrameInteractInput = inInteract;
        lastFrameDownInput = inDown;
        lastFrameVertical = vertical;
    }

    public void Shoot() {
        Log.Text("~~SHOOT~~");
      //  Debug.Break();
        //if (_animator.GetBool("Shooting"))
        //    _animator.SetInteger("ComboCount", combo + 1);
        _animator.SetTrigger("Shoot");      
    }

    public override void PostAnimShoot() {
        int combo = _animator.GetInteger("ComboCount");
        _comboCounter = combo;
        Log.Text("Combo:", _comboCounter);
        Attack();
        //Debug.Break();
        _shootingCooldown = _animator.GetFloat("FireRate") + eventHub.fireRateModifier;
    }

    public override void UpdatePowerup() {
        base.UpdatePowerup();
        
        SpriteRenderer sprite = gameObject.GetComponent<SpriteRenderer>();
        if (sprite && _currentPowerup.customPalette) {
            Material material = sprite.material;
            //Debug.Log("Has Material and Palette");
            material.SetTexture("_PaletteTex", _currentPowerup.customPalette);
        }

        TransformHero(_currentPowerup);
    }

    private void TransformHero(PowerupDefinition newPowerup) { 
        Transform trail = transform.Find("Trail");
        if (trail) {
            trail.GetOrAddComponent<TrailRenderer>().material.SetColor("_Color", newPowerup.customColor);
        }

        CharacterTrail ctrail = GetComponent<CharacterTrail>();
        if(ctrail) {
            ctrail.SetFrameColor(newPowerup.customColor);
        }
    }

    public void FixedUpdate() {
        if (alreadyDead || _lockedInput) return;
        bool inJump = Input.GetButton("Jump");
        float horizontal = Input.GetAxisRaw("Horizontal");

        if (_animator.GetBool("CanMove") && !_animator.GetBool("WallJumping") && !_animator.GetBool("KnockedBack")) {
            float direction = 0;
            if (horizontal > 0) direction = 1f;
            else if (horizontal < 0) direction = -1f;

            float speedMod = 1.0f;
            if (_animator.GetBool("Jumping") || _animator.GetBool("Falling")) speedMod = _jumpSpeedMod;
            _body.velocity = new Vector2(direction * _moveSpeed * speedMod, _body.velocity.y);
        }

        if ( (inJump && _animator.GetBool("Jumping"))){
            Vector3 ximpulse = new Vector3(_body.velocity.x * _jumpHold, 0, 0);
            _body.AddForce((_animator.transform.up * _jumpHold), ForceMode2D.Force);
        }
        if((inJump && _animator.GetBool("Falling") && _animator.GetBool("CanGlide"))) {
            _body.AddForce((_animator.transform.up * _jumpHold * 1.3f), ForceMode2D.Force);
        }
    }
    public void Die(BaseCharacter attacker) {
        if (alreadyDead) return;
        deathCount++;
        alreadyDead = true;
        StartCoroutine(WaitThenDie(attacker));
    }

    public override void Die() {
        if(alreadyDead) return;
        deathCount++;
        alreadyDead = true;
        StartCoroutine(WaitThenDie(this));
        /* 
        onDeath.Invoke();
        if(DeathEffect) {
            GameObject fx = Instantiate(DeathEffect);
            fx.transform.position = transform.position;
        }
        
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        */
    }
}