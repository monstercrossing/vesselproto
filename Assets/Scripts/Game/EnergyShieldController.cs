﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnergyShieldController : MonoBehaviour {
    public bool isProtected = true;
    public float timeout = 2.0f;

    public CharacterEvent onShieldLoss = new CharacterEvent();
    public CharacterEvent onShieldGain = new CharacterEvent();

    private float timer = 0;
    private GameObject shield;
    private BaseCharacter character;
    // Use this for initialization

    public void Start() {
        character = transform.GetOrAddComponent<BaseCharacter>();
    }

    public void Create(GameObject effectObject, float time) {
        shield = GameObject.Instantiate(effectObject);
        shield.transform.parent = transform;
        shield.transform.localPosition = new Vector3(0, 0.5f, 0);
        this.timeout = time;
        onShieldGain.Invoke(character);
    }
	
	// Update is called once per frame
	void Update () {
		if(timer > 0) {
            timer -= Time.deltaTime;
        }
        if (timer <= 0 && !isProtected) {
            Reactivate();
        }
        //shield.transform.position = new Vector3(0, 0.5f, 0);
    }

    public void TemporaryLoss() {
        isProtected = false;
        shield.SetActive(isProtected);
        timer = timeout;
        onShieldLoss.Invoke(character);
    }

    public void PermamentLoss() {
        Destroy(shield);
        Destroy(this);
        onShieldLoss.Invoke(character);
    }

    public void Reactivate() {
        isProtected = true;
        shield.SetActive(isProtected);
        timer = 0;
        onShieldGain.Invoke(character);
    }
}
