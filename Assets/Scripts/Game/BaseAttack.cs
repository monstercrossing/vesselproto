﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAttack : MonoBehaviour {
 	private BaseCharacter _owner;
	public void SetOwnership(BaseCharacter newOwner) {
		_owner = newOwner;
	}

    public BaseCharacter GetOwner() {
        return _owner;
    }
}
