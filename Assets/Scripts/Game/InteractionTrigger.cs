﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractionTrigger : ContactTrigger {
    public GameObject tutorialPrompt;
    public UnityEvent onTriggerEnter;
    public UnityEvent onTriggerExit;

    private bool isEnabled = true;

    protected override void OnTriggerEnter2D(Collider2D collision) {
        if (!isEnabled) return;
        bool contains = false;
        for (int i = 0; i < interactableTags.Length; i++) {
            if (interactableTags[i] == collision.gameObject.tag) {
                contains = true;
                break;
            }
        }
        //base.OnTriggerEnter2D(collision);
        if (contains) {
            if (tutorialPrompt)
                tutorialPrompt.SetActive(true);

            BasePlayer player = collision.transform.GetOrAddComponent<BasePlayer>();
            if (player)
                player.OnInteractionInput.AddListener(TriggerInteraction);

            onTriggerEnter.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        bool contains = false;
        for (int i = 0; i < interactableTags.Length; i++) {
            if (interactableTags[i] == collision.gameObject.tag) {
                contains = true;
                break;
            }
        }

        if (contains) {
            BasePlayer player = collision.transform.GetOrAddComponent<BasePlayer>();
            if (player)
                player.OnInteractionInput.RemoveListener(TriggerInteraction);

            if (tutorialPrompt)
                tutorialPrompt.SetActive(false);

            onTriggerExit.Invoke();
        }
    }

    void TriggerInteraction() {
        OnContact.Invoke();
    }

    public void SetEnabled(bool state) {
        isEnabled = state;
    }
}
