﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterTrail : MonoBehaviour
{
    public float afterburnTime = 0.6f;
    public float frameLifetime = 1.2f;
    public float timeBetweenFrames = 0.25f;
    public Material frameMaterial;
    public Color initialColor;
    public bool useDefaultColor = false;

    private Color _customColor = Color.gray;

    private Animator _animator;
    private SpriteRenderer _renderer;
    private bool lastFrameDashing = false;
    private float _afterburning = 0;
    private float lastFrameTime = 0;
    public void Start() {
        _animator = GetComponent<Animator>();
        _renderer = GetComponent<SpriteRenderer>();
        if (useDefaultColor) SetFrameColor(initialColor);
    }

    public void FixedUpdate() {
        bool isDashing = _animator.GetBool("Dashing");
        if (isDashing) {
            if(!lastFrameDashing)
                _afterburning = afterburnTime;
            CreateFrame();           
        }

        if(_afterburning > 0) {
            _afterburning -= Time.deltaTime;
            CreateFrame();
        }


        lastFrameDashing = isDashing;
    }

    public void CreateFrame() {
        float currentTimestamp = Time.timeSinceLevelLoad;
        if (currentTimestamp - lastFrameTime < timeBetweenFrames) return;
        lastFrameTime = Time.timeSinceLevelLoad;

        GameObject frame = new GameObject("trailFrame", typeof(SpriteRenderer));
        frame.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
        SpriteRenderer faderSprite = frame.GetComponent<SpriteRenderer>();
        faderSprite.flipX = _renderer.flipX;
        faderSprite.flipY = _renderer.flipY;
        if(frameMaterial)
            faderSprite.material = frameMaterial;
        faderSprite.sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
        frame.transform.localScale = transform.localScale;

        faderSprite.color = Color.Lerp(_customColor, Color.white, (_afterburning/afterburnTime)/2);

        CharacterTrailFrame tframe = frame.transform.GetOrAddComponent<CharacterTrailFrame>();
        tframe.Initialize(frameLifetime);
    }

    public void SetFrameColor(Color c) {_customColor = c;}
}
