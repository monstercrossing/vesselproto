﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;

public class TrapCollision : MonoBehaviour {
    public IntVariable trapDamage;
    public FloatVariable knockbackImpulse;
    public PowerupDefinition powerupDefinition;
    public float stunTime = 0.5f;
    public float iframes = 0.7f;

    private int _trapDamage = 0;
    private float _knockback = 1.0f;

    void OnCollisionStay2D(Collision2D collision) {
        Collide(collision.collider);
    }

    private void OnTriggerStay2D(Collider2D collision) {
        Collide(collision);
    }

    void Collide(Collider2D collision) {
        Animator _animator = gameObject.GetComponent<Animator>();
        if (trapDamage) _trapDamage = trapDamage.Value;
        if (knockbackImpulse) _knockback = knockbackImpulse.Value;

        if (collision.gameObject.tag != gameObject.tag) {

            BaseCharacter other = collision.gameObject.GetComponent<BaseCharacter>();
            BaseCharacter thisCharacter = gameObject.GetComponent<BaseCharacter>();
            if (!other) return;

            Vector3 dir = (collision.transform.position - transform.localPosition).normalized;

            other.TakeKnockback(dir * _knockback, stunTime, iframes);
            other.ReceiveDamageAndUpdatePowerups(_trapDamage, powerupDefinition, thisCharacter);
        }
    }
}
