﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCollision : MonoBehaviour {
    void OnCollisionStay2D(Collision2D collision) {
        Collide(collision.collider);
    }

    void Collide(Collider2D collision) {
        BaseCharacter owner = GetComponent<BaseCharacter>();
        Animator _animator = gameObject.GetComponent<Animator>();

        if (owner && _animator.GetBool("Dashing")) {
            if (collision.gameObject.tag != owner.gameObject.tag) {
                BaseCharacter other = collision.gameObject.GetComponent<BaseCharacter>();
                if (!other) return;
                CharacterEventHub hub = owner.GetOrAddComponent<CharacterEventHub>();
                hub.onDashImpactApplied.AddListener(Test);
                hub.onDashImpactApplied.Invoke(other);
            }
        }

        if(collision.gameObject.layer == LayerMask.NameToLayer("Terrain") && _animator.GetBool("CanWallJump") && !_animator.GetBool("IsGrounded")) {
            //Debug.Log(collision.relativeVelocity.magnitude);

            ContactPoint2D[] contacts = new ContactPoint2D[6];
            int size = collision.GetContacts(contacts);
            for(int i = 0; i < size; i++)  {
             ContactPoint2D contact = contacts[i];
             float angle = Vector3.Angle(contact.normal, Vector3.up);
                
               // if(Mathf.Approximately(angle, 0))// back
               // if(Mathf.Approximately(angle, 180))// front
               // if(Mathf.Approximately(angle, 90)){
                Vector3 cross = Vector3.Cross(Vector3.up,contact.normal);
                if(Mathf.Abs(cross.z) > 0.5){
                    _animator.SetFloat("WallDirection", cross.z);
                    _animator.SetBool("IsHuggingWall", true);
                    //_animator.SetTrigger("ClingToWall");
                } 
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision) {
         Animator _animator = gameObject.GetComponent<Animator>();
         _animator.SetBool("IsHuggingWall", false);
    }


    void Test(BaseCharacter character) {
        
    }

    void OnTriggerStay2D(Collider2D other)
    {
       Collide(other);
    }

}
