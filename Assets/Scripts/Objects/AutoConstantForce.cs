﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoConstantForce : MonoBehaviour {
    public float horizontalForce;
    public float verticalForce;

    ConstantForce2D constant;
    // Start is called before the first frame update
    void Start() {
        constant = transform.GetOrAddComponent<ConstantForce2D>();
    }

    private void FixedUpdate() {
        constant.force = new Vector2( horizontalForce * transform.localScale.x, verticalForce * transform.localScale.y );
    }
}
