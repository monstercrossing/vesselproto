﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SelfPropel : MonoBehaviour
{
    public Vector3 direction;
    public float rotation;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        body.AddForce(direction, ForceMode2D.Impulse);
        body.AddTorque(rotation);
    }

}
