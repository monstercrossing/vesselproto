﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTransform : MonoBehaviour
{
    public void SetPosition(Transform newTransform) {
        transform.position = newTransform.position;
    }
}
