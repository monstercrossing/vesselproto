﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapMechanics : MonoBehaviour {
    public float offset;
    public float speed;
    public float delay;

    public ContactTrigger triggerObject;
    public float triggeredSpeed = 5;

    private float _timer;
    private bool _initialState = true;
    private Vector3 _initialPosition;
    private Vector3 _targetPosition;
    private bool isMoving = true;
    private float _movingSpeed;

    private bool _isTriggered;
    private UnityEngine.Events.UnityEvent _triggerEvent;

    [ExecuteInEditMode]
    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Vector3 start = transform.position + new Vector3(0, 0, 0);
        Vector3 direction = transform.up;

        Debug.DrawRay(start, direction * offset, Color.yellow);

        if(triggerObject) {
            Vector3 diffDir = triggerObject.transform.position - start;
            Debug.DrawRay(transform.position, diffDir, Color.blue);
        }
    }
    // Use this for initialization
    void Start () {
        _timer = delay;
        _movingSpeed = speed;

        _initialPosition = transform.position;
        _targetPosition = transform.position + (transform.up * offset);

        if(triggerObject) {
            isMoving = false;
            _isTriggered = true;
            _triggerEvent = triggerObject.OnContact;
            _triggerEvent.AddListener(Activate);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (isMoving) {
            _timer -= Time.deltaTime;
            if (_timer < 0) {
                _timer = delay;

                Toggle();
            }
        }

        Vector3 target = _initialState ? _initialPosition : _targetPosition;
        transform.position = Vector3.Slerp(transform.position, target, Time.deltaTime * _movingSpeed);
	}

    void Toggle() {
        _movingSpeed = speed;
        _initialState = !_initialState;
        if (_initialState && _isTriggered) isMoving = false;
    }

    void Activate() {
        _movingSpeed = triggeredSpeed;
        _initialState = false;
        isMoving = true;
    }
}
