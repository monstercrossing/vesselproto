﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class DestroyableObject : MonoBehaviour
{
    public bool destroyOnExplosion;
    public bool destroyOnStomp;

    public UnityEvent OnDestroy;
    // Start is called before the first frame update

    private bool _destroyed = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionStay2D(Collision2D collision) {
        if (_destroyed) return;
        //BaseCharacter owner = GetComponent<BaseCharacter>();
        //Animator _animator = gameObject.GetComponent<Animator>();
        Log.Text("Collided: ", collision.gameObject.tag);
        //BaseCharacter other = collision.otherCollider.gameObject.GetComponent<BaseCharacter>();
        if (collision.gameObject.tag == "Hero") {
            Log.Text("Collided with character");

            Animator _animator = collision.gameObject.GetComponent<Animator>();
            if(_animator) {
                try {
                Log.Text("Other Has Anim", _animator.GetCurrentAnimatorClipInfo(0)[0].clip.name);
                }catch(System.Exception e) {}
                bool isDashing = _animator.GetBool("PostStomping") || _animator.GetBool("Stomping");
                if (isDashing && destroyOnStomp) {
                    Log.Text("Other Is Dashing");
                    TriggerDestruction();
                }
            }
        }
    }

    public void TriggerDestruction() {
        //Rigidbody2D body = GetComponent<Rigidbody2D>();
        //body.isKinematic = false;
        OnDestroy.Invoke();
        gameObject.layer = LayerMask.NameToLayer("CollidesWithTerrain");
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<ParticleSystem>().Play();
        _destroyed = true;
    }

}
