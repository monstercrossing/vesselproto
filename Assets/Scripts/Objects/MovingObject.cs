﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MovingObject : MonoBehaviour {
    public float speed;
    public float delay;
    public bool stayOpenOnTrigger;
    public bool stopAtEachWaypoint;
    public bool cycleAtEnd;
    public bool continuousAfterTrigger;
    public bool returnOnCollision;

    public int activationMode; //GUI attribute only

    public Vector3[] waypoints;

    public ContactTrigger triggerObject;
    public float triggeredSpeed = 5;

    private float _timer;
    private float _mover;
    private bool _initialState = true;
    private Vector3 _initialPosition;
    private Vector3 _targetPosition;
    private bool isMoving = false;
    private bool isWaiting = true;
    private float _movingSpeed;
    private int nextWaypoint = 0;
    private bool movingForward = true;
    private bool _active;

    public bool _isTriggered;
    private UnityEngine.Events.UnityEvent _triggerEvent;

    // Use this for initialization
    void Start() {
        _timer = delay;
        _movingSpeed = speed;

        for(int i=0; i < waypoints.Length; i++) {
            waypoints[i] = transform.TransformPoint(waypoints[i]);
        }

        _initialPosition = transform.position;
        Debug.Log(waypoints.Length);
        try {
            _targetPosition = waypoints[nextWaypoint];
        }catch(System.Exception e) {
            _targetPosition = transform.position;
        }
        
        if (_isTriggered) {
            isMoving = false;
            isWaiting = !isMoving;
            //_isTriggered = true;
            if (triggerObject) {
                _triggerEvent = triggerObject.OnContact;
                _triggerEvent.AddListener(Activate);
            }
            _active = false;
        }
        else {
            _active = true;
            isMoving = true;
            isWaiting = !isMoving;
        }

        if (activationMode == 0) {
            continuousAfterTrigger = false;
            _isTriggered = false;
        } else if (activationMode == 1) {
            continuousAfterTrigger = false;
            _isTriggered = true;
        } else if (activationMode == 2) {
            continuousAfterTrigger = true;
            _isTriggered = true;
        }
    }

    // Update is called once per frame
    public void Update() {
        if (isWaiting && _active) {
            _timer -= Time.deltaTime;
            if (_timer < 0) {
                Toggle();
            }
        }

        if (Mathf.Abs(Vector3.Distance(transform.position, _targetPosition)) < 0.005f && isMoving) {
            Toggle();
        }
    }

    private void FixedUpdate() {
        Vector3 target = _initialState ? _initialPosition : _targetPosition;
        Rigidbody2D _body = transform.GetOrAddComponent<Rigidbody2D>();

        if (isMoving)
         _body.MovePosition(Vector3.MoveTowards(transform.position, _targetPosition, Time.deltaTime * _movingSpeed));
         //transform.position = Vector3.Lerp(transform.position, _targetPosition, Time.deltaTime * _movingSpeed);
         
    }

    void Toggle() {
        bool shouldStop = (nextWaypoint < 0 || nextWaypoint+1 >= waypoints.Length) && !cycleAtEnd;
        if (stopAtEachWaypoint || shouldStop) {
            _timer = delay;
            _movingSpeed = speed;
            //_initialState = !_initialState;
            //if (_initialState && _isTriggered) isMoving = false;
            isMoving = !isMoving;
            isWaiting = !isMoving;
        }

        if (nextWaypoint + 1 == waypoints.Length && movingForward && _isTriggered && stayOpenOnTrigger) {
            _active = false;
        }
        else if (isMoving) {
            SetNextTarget();
        }
    }


    public void Activate (float timeToWait)
    {
        if (_active) return;

        StartCoroutine(ActivateMove(timeToWait));
    }

    public void Activate() {
        Activate(0f);
    }

    private IEnumerator ActivateMove(float time)
    {
        yield return new WaitForSeconds(time);
        
        if (continuousAfterTrigger)
        {
            isMoving = true;
            _active = true;
            isWaiting = !isMoving;
            _isTriggered = false;
        }
        else
        {
            _movingSpeed = triggeredSpeed;
            _initialState = false;
            isMoving = true;
            isWaiting = false;
            _active = true;
            movingForward = true;
            nextWaypoint = 0;
            _targetPosition = waypoints[nextWaypoint];
        }
    }

    public void ReturnToOriginalState() {
        _active = true;
        movingForward = false;
        nextWaypoint = -1;
        _targetPosition = _initialPosition;
        isMoving = true;
        isWaiting = false;
    }

    void SetNextTarget() {
        //Moving forward, has more waypoints
        if (movingForward && nextWaypoint + 1 < waypoints.Length) {
            nextWaypoint++;
            if (nextWaypoint < 0) nextWaypoint = 0;
            try
            {
                _targetPosition = waypoints[nextWaypoint];
            } catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
        //Moving forward, at last waypoint but is cyclical
        else if (movingForward && nextWaypoint + 1 >= waypoints.Length && cycleAtEnd) {
            nextWaypoint = -1;
            _targetPosition = _initialPosition;
        }
        //Moving forward, at last waypoint, should turn back
        else if(movingForward && nextWaypoint + 1 >= waypoints.Length) {
            nextWaypoint--;
            movingForward = false;
            if (nextWaypoint >= 0) {
                _targetPosition = waypoints[nextWaypoint];
            } else {
                _targetPosition = _initialPosition;
            }
        }
        //Moving backward, no more waypoints, should turn forward
        else if (!movingForward && nextWaypoint <= 0) {
            nextWaypoint--;
            _targetPosition = _initialPosition;
            movingForward = true;
            if (_isTriggered && !continuousAfterTrigger) _active = false;
        }
        //Moving backward, has points
        else {
            nextWaypoint--;
            movingForward = false;
            try {
               _targetPosition = waypoints[nextWaypoint];
            } catch (UnityException e) {
                nextWaypoint = 0;
                movingForward = true;
                if (stayOpenOnTrigger) _active = false;
                _targetPosition = waypoints[nextWaypoint];
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if(returnOnCollision && collision.gameObject.layer == LayerMask.NameToLayer("Terrain")) {
            //nextWaypoint--;
            movingForward = !movingForward;
            SetNextTarget();
        }
    }
}
