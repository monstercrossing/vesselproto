﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirepillarGrowth : MonoBehaviour {
    public float growthRateX;
    public float growthRateY;
    public float maxHeight;
    public float maxWidth;

    private BoxCollider2D _col;
    private SpriteRenderer _sprite;

    private void Start() {
        _col = transform.GetOrAddComponent<BoxCollider2D>();
        _sprite = transform.GetOrAddComponent<SpriteRenderer>();
    }
    // Update is called once per frame
    void Update()   {
        if (_sprite.size.y < maxHeight) {
            _sprite.size = new Vector2(_sprite.size.x, _sprite.size.y + growthRateY);
            _col.size = new Vector2( _col.size.x, _col.size.y + growthRateY);
        }
        if (_sprite.size.x < maxWidth) {
            _sprite.size = new Vector2(_sprite.size.x + growthRateX, _sprite.size.y);
            _col.size = new Vector2(_col.size.x + growthRateX, _col.size.y);
        }

        _col.offset = new Vector2(0, _col.size.y / 2);
    }
}
