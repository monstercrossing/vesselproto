﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetachFromParentOnEnable : MonoBehaviour
{
    void OnEnable(){
        transform.parent = null;
    }
}
