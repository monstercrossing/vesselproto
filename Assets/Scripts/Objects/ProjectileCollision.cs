﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCollision : MonoBehaviour {
    private float lifetime = 1f;
    public GameEvent OnHit;
    public bool alwaysDestroyOnHit = true;
    public GameObject destructionEffect;
    public AudioClip impactSFX;

    //[HideInInspector]
    public bool piercing = false;
    public bool alwaysUserDestructionFX = false;

    void Start() {
        BaseRangedAttack attack = GetComponent<BaseRangedAttack>();
        if (attack && attack.projectileDefinition) lifetime = attack.projectileDefinition.lifeTime;
    }

    void Update() {
        lifetime -= Time.deltaTime;
        if(lifetime <= 0) {
            OnHit = null;
            DestroyThisProjectile();
        }
    }

    // Use this for initialization
    void OnCollisionEnter2D(Collision2D collision){
        Collide(collision.collider);   
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Collide(collision);
        /*
        BaseRangedAttack attack = GetComponent<BaseRangedAttack>();
        BaseCharacter owner = null;
        if (attack) owner = attack.GetOwner();
        else return;

        if (collision.gameObject.tag != owner.gameObject.tag) {
            PowerupDefinition newPowerup = owner.CurrentPowerup();
            BaseCharacter other = collision.gameObject.GetComponent<BaseCharacter>();
            if(!other) return;
            other.ReceiveDamageAndUpdatePowerups(attack.projectileDefinition.damage, newPowerup, owner);
            if (!piercing)
                DestroyThisProjectile();
        }
        */
    }

    private void Collide(Collider2D collision) {
        BaseRangedAttack attack = GetComponent<BaseRangedAttack>();
        BaseCharacter owner = null;
        if (attack) owner = attack.GetOwner();
        else return;

        if(!owner) {
            DestroyThisProjectile();
            return;
        }

        if (collision.gameObject.tag != owner.gameObject.tag) {
            PowerupDefinition newPowerup = owner.CurrentPowerup();
            ProjectileDefinition projectile = gameObject.GetComponent<BaseRangedAttack>().projectileDefinition;
            BaseCharacter other = collision.gameObject.GetComponent<BaseCharacter>();

            if (other) {
                if(projectile && projectile.stunTime > 0) {
                    //other.GetOrAddComponent<StunController>().Stun(projectile.stunTime);
                    other.GetOrAddComponent<StunController>().queuedTime += projectile.stunTime;
                } else {
                    switch(newPowerup.attackType) {
                        case PowerupDefinition.AttackType.Punch:
                            owner.eventHub.onPunchDamageInflictedTo.Invoke(other);break;
                        case PowerupDefinition.AttackType.Melee:
                            owner.eventHub.onMeleeDamageInflictedTo.Invoke(other); break;
                        case PowerupDefinition.AttackType.Ranged:
                            owner.eventHub.onRangedDamageInflictedTo.Invoke(other); break;
                        case PowerupDefinition.AttackType.Grenade:
                            owner.eventHub.onGrenadeDamageInflictedTo.Invoke(other); break;
                    }
                    owner.eventHub.onDamageInflictedTo.Invoke(other);
                }
                int damage = attack.projectileDefinition.damage;// + owner.comboCounter;
                
                if(owner.eventHub != null)
                    damage += owner.eventHub.GetDamageModifiersFor(newPowerup.attackType);
                other.ReceiveDamageAndUpdatePowerups(damage, newPowerup, owner);
                if (!piercing || other.gameObject.layer == LayerMask.NameToLayer("Terrain"))
                    DestroyThisProjectile(true);
            } else if (alwaysDestroyOnHit) {
                OnHit = null;
                DestroyThisProjectile();
            }
        }
    }

    public void DestroyThisProjectile() {
        DestroyThisProjectile(false);
    }
    public void DestroyThisProjectile(bool enemyImpact) {
        if(OnHit)
            OnHit.Raise();
        ProjectileDefinition projectile = gameObject.GetComponent<BaseRangedAttack>().projectileDefinition;
        if(projectile.areaOfEffect > 0) {
            BaseRangedAttack attack = GetComponent<BaseRangedAttack>();
            BaseCharacter owner = attack.GetOwner();
            projectile.areaOfEffect += owner.eventHub.aeoModifier;
            
            foreach (Collider2D col in Physics2D.OverlapCircleAll(transform.position, projectile.areaOfEffect, LayerMask.GetMask("Characters"))) {
                if (col.gameObject.tag != "Enemy" && col.gameObject.tag != "Hero") {
                    DestroyableObject destroyable = col.gameObject.GetComponent<DestroyableObject>();
                    if(destroyable && destroyable.destroyOnExplosion) {
                        destroyable.TriggerDestruction();
                    }

                    continue;
                } 
                ///*
                if(Physics2D.Linecast(transform.position, col.transform.position + new Vector3(0,0.3f,0), LayerMask.GetMask("Terrain"))) {
                    continue;
                }
                //*/

                BaseCharacter other = col.gameObject.GetComponent<BaseCharacter>();
                if (other && other != owner) {
                    PowerupDefinition newPowerup = owner.CurrentPowerup();
                    owner.eventHub.onAreaDamageInflictedTo.Invoke(other);
                    int damage = attack.projectileDefinition.damage;
                    if (owner.eventHub != null)
                        damage += owner.eventHub.GetDamageModifiersFor(PowerupDefinition.AttackType.AoE);
                    other.ReceiveDamageAndUpdatePowerups(damage, newPowerup, owner);
                }
            }
        }
        if (destructionEffect && (lifetime > 0 || alwaysUserDestructionFX) ) {
            GameObject effect = GameObject.Instantiate(destructionEffect);
            effect.transform.position = transform.position;
        }
        if(impactSFX && enemyImpact) {
            transform.GetOrAddComponent<AudioSource>().PlayOneShot(impactSFX);
        }

        GameObject.Destroy(gameObject);
    }

}
