﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/DOT Applier")]
public class ApplyDamageOverTime : HookBehaviour {
    public GameObject effectObject; //Poison Particles, for example
    public float timeout;
    public int damage;

    CharacterEventHub hub;
    CharacterEventHub otherHub;
    DamageOverTimeController dotController;
    public override void Run(MonoBehaviour callerObject, MonoBehaviour parent) {
        base.Run(callerObject);
        //callerObject is the enemy
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        otherHub = parent.transform.GetOrAddComponent<CharacterEventHub>();
        dotController = callerObject.transform.GetOrAddComponent<DamageOverTimeController>();

        int totalDamage = damage + otherHub.GetDamageModifiersFor(PowerupDefinition.AttackType.DoT) - hub.GetDamageMitigatorsFor(PowerupDefinition.AttackType.DoT);
        float time = timeout * otherHub.inflictedDOTModifier * hub.dotModifier;
        Log.Text("~~DOT Time:",hub.inflictedDOTModifier);
        if (dotController != null && effectObject != null) {
                dotController.Initialize(effectObject);
            //hub.onDamageReceived.AddListener(dotController.TemporaryLoss);
            //dotController.onShieldGain.AddListener(GainShield);
            //dotController.onShieldLoss.AddListener(LoseShield);
        }

        dotController.ApplyDamageOverTime(totalDamage, timeout);

    }

    public override void Remove(MonoBehaviour callerObject) {
        
    }
}
