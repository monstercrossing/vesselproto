﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Damage Over Time Modifier")]
public class DOTTimeHook : HookBehaviour {
    public float modifier;

    private CharacterEventHub hub;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.inflictedDOTModifier *= modifier;
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.inflictedDOTModifier /= modifier;
    }
}
