﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Secret Enabler")]
public class EnableSecretsHook : HookBehaviour {
    CharacterEventHub hub;
    // Start is called before the first frame update
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
            hub.secretEnabled = true;
    }

    // Update is called once per frame
    public override void Remove(MonoBehaviour callerObject) {
        hub.secretEnabled = false;
    }
}
