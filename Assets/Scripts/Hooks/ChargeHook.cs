﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Charge")]
public class ChargeHook : HookBehaviour {
    public GameObject chargingEffect;
    public GameObject chargedEffect;
    public float chargeTimer;

    //TODO: Fazer...
    CharacterEventHub hub;
    ChargeController charger;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.chargingEnabled = true;
        charger = callerObject.transform.GetOrAddComponent<ChargeController>();

        charger.Setup(chargeTimer, chargingEffect, chargedEffect);

        //hub.onAreaDamageInflictedTo.AddListener(ApplyStun);
        hub.onAttackHold.AddListener(charger.BeginCharge);
        hub.onAttackRelease.AddListener(charger.ReleaseChargedAttack);
        hub.onChargeReady.AddListener(charger.ChargeReady);
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.chargingEnabled = false;
        hub.onAttackHold.RemoveListener(charger.BeginCharge);
        hub.onAttackRelease.RemoveListener(charger.ReleaseChargedAttack); 
        hub.onChargeReady.RemoveListener(charger.ChargeReady);
    }

    //*/
}
