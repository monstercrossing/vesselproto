﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Damage On Dash")]
public class DamageOnDash : HookBehaviour {
    public PowerupDefinition powerupOnDamage;
    public int dashDamage;

    private BaseCharacter owner;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        //Debug.Log(callerObject);
        CharacterEventHub hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        owner = callerObject.transform.GetOrAddComponent<BaseCharacter>();

        hub.onDashImpactApplied.AddListener(ApplyDamage);
    }

    void ApplyDamage(BaseCharacter other) {
        other.ReceiveDamageAndUpdatePowerups(dashDamage, powerupOnDamage, owner);
    }
}
