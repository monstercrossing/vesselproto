﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Hitpoint Modifier")]
public class ModifyHitpointsHook : HookBehaviour
{
    public GameEvent OnHPChange;
    public IntVariable newMaxLife;
    BaseCharacter thisCharacter;
    IntVariable originalMaxLife;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        thisCharacter = callerObject.GetComponent<BaseCharacter>();
        originalMaxLife = thisCharacter.maxHitpoints;
        thisCharacter.maxHitpoints = newMaxLife;

        if(OnHPChange) OnHPChange.Raise();
    }

    public override void Remove(MonoBehaviour callerObject) {
        thisCharacter.maxHitpoints = originalMaxLife;
        if(OnHPChange) OnHPChange.Raise();
    }
}
