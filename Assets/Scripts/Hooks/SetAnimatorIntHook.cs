﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Set Animator Int")]
public class SetAnimatorIntHook : HookBehaviour {
    public string attributeName;
    public int attriuteValue;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        Animator animator = callerObject.transform.GetOrAddComponent<Animator>();
        animator.SetInteger(attributeName, attriuteValue);
    }
}
