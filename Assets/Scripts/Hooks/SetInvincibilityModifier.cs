﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Invincibility Modifier")]
public class SetInvincibilityModifier : HookBehaviour {
    public float modifier;
    private CharacterEventHub hub;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.iframesModifier *= modifier;
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.iframesModifier /= modifier;
    }
}
