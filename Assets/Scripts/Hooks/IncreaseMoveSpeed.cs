﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Move Speed Modifier")]
public class IncreaseMoveSpeed : HookBehaviour {
    public float modifier;
    private CharacterEventHub hub;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.moveSpeedModifier *= modifier;
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.moveSpeedModifier /= modifier;
    }

}
