﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Attack Speed Modifier")]
public class IncreaseAttackSpeed : HookBehaviour {
    public float modifier;
    private CharacterEventHub hub;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.fireRateModifier += modifier;
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.fireRateModifier -= modifier;
    }

}
