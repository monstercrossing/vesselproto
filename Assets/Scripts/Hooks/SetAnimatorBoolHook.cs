﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Set Animator Bool")]
public class SetAnimatorBoolHook : HookBehaviour {
    public string attributeName;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        Animator animator = callerObject.transform.GetOrAddComponent<Animator>();
        animator.SetBool(attributeName, true);
    }
}
