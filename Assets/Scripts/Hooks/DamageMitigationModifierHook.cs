﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Damage Mitigation Modifier")]
public class DamageMitigationModifierHook : HookBehaviour {
    public int modifier;

    [EnumFlagAttribute]
    public PowerupDefinition.AttackType modifiedAttackTypes;

    private CharacterEventHub hub;
    private DamageModifier mod;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        //Debug.Log("Damage modifier types" + modifiedAttackTypes);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        //hub.damageInflictedModifier += modifier;
        mod = hub.RegisterDamageMitigator(modifier, modifiedAttackTypes);
    }

    public override void Remove(MonoBehaviour callerObject) {
        //hub.damageInflictedModifier -= modifier;
        hub.RemoveDamageMitigator(mod);
    }
}
