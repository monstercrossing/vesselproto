﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Energy Shield Applier")]

public class ApplyEnergyShieldHook : HookBehaviour {
    public GameObject effectObject;
    public float timeout = 2;
    public HookBehaviour mitigationHook;

    CharacterEventHub hub;
    EnergyShieldController shield;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        if (mitigationHook) mitigationHook.Run(callerObject);

        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        shield = callerObject.transform.GetOrAddComponent<EnergyShieldController>();
        if (shield != null && effectObject != null) {
            shield.Create(effectObject, timeout);
            hub.onDamageReceived.AddListener(shield.TemporaryLoss);
            shield.onShieldGain.AddListener(GainShield);
            shield.onShieldLoss.AddListener(LoseShield);
        }
    }

    public override void Remove(MonoBehaviour callerObject) {
        if (shield) {
            hub.onDamageReceived.RemoveListener(shield.TemporaryLoss);
            shield.PermamentLoss();
            shield = null;
        }
        if(mitigationHook && shield) {
            shield.onShieldGain.RemoveListener(GainShield);
            shield.onShieldLoss.RemoveListener(LoseShield);
        }
    }

    public void GainShield(MonoBehaviour callerObject) {
        if (mitigationHook) mitigationHook.Run(callerObject);

    }

    public void LoseShield(MonoBehaviour callerObject) {
        if (mitigationHook) mitigationHook.Remove(callerObject);
    }
}
