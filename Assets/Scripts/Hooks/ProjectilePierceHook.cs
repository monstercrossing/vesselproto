﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Projectile Pierce")]
public class ProjectilePierceHook : HookBehaviour {
    CharacterEventHub hub;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();

        hub.onRangedAttack.AddListener(OnShoot);
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.onRangedAttack.RemoveListener(OnShoot);
    }

    void OnShoot(GameObject projectile) {
        if (projectile.transform.childCount > 0) {
            foreach (Transform child in projectile.transform) {
                OnShoot(child.gameObject);
           }
        }

        ProjectileCollision collision = projectile.transform.GetComponent<ProjectileCollision>();
        if (collision != null) {
            Collider2D collider = projectile.transform.GetOrAddComponent<Collider2D>();
            collider.isTrigger = true;
            collision.piercing = true;
        }
    }
}
