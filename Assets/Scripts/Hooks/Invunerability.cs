﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Invulnerability")]
public class Invunerability : HookBehaviour
{
    private BaseCharacter character;
     public override void Run(MonoBehaviour callerObject) {
         base.Run(callerObject);
        character = callerObject.transform.GetOrAddComponent<BaseCharacter>();
        character.invulnerable = true;
    }

    public override void Remove(MonoBehaviour callerObject) {
        character.invulnerable = false;
    }
}
