﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Stun Applier")]
public class ApplyStunHook : HookBehaviour {
    public float stunTime;
    public bool areaDamageOnly = true;

    CharacterEventHub hub;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();

        hub.onAreaDamageInflictedTo.AddListener(ApplyStun);
        if (!areaDamageOnly)
            hub.onDamageInflictedTo.AddListener(ApplyStun);
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.onAreaDamageInflictedTo.RemoveListener(ApplyStun);
        hub.onDamageInflictedTo.RemoveListener(ApplyStun);
    }

    void ApplyStun(BaseCharacter other) {
        hub.onStunInflictedTo.Invoke(other);
        other.GetOrAddComponent<StunController>().Stun(stunTime);
    }
}
