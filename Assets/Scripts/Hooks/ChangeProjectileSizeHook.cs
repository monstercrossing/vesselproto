﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Projectile Size")]
public class ChangeProjectileSizeHook : HookBehaviour {
    CharacterEventHub hub;
    public float scale = 1; //multiplier
    // Use this for initialization
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();

        hub.onRangedAttack.AddListener(ChangeSize);
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.onRangedAttack.RemoveListener(ChangeSize);
    }

    void ChangeSize(GameObject projectile) {
        projectile.transform.localScale *= scale;
    }
}
