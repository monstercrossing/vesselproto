﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Area of Effect Modifier")]
public class IncreaseAOE : HookBehaviour
{
    public float modifier;

    private CharacterEventHub hub;

    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.aeoModifier += modifier;
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.aeoModifier -= modifier;
    }
}
