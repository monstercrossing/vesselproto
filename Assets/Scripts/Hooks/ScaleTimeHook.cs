﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Time Scaler")]
public class ScaleTimeHook : HookBehaviour {
    public float scale;

    CharacterEventHub hub;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();
        hub.timeScale = scale;
        Time.timeScale = scale;
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.timeScale = 1;
        Time.timeScale = 1;
    }
}
