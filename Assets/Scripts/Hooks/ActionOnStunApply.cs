﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Stun Action")]
public class ActionOnStunApply : HookBehaviour {
    public HookBehaviour hook;

    CharacterEventHub hub;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();

        hub.onStunInflictedTo.AddListener(ApplyEffect);
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.onStunInflictedTo.RemoveListener(ApplyEffect);
    }

    public void ApplyEffect(MonoBehaviour callerObject) {
        if (hook) hook.Run(callerObject, hub.gameObject.GetComponent<BaseCharacter>());
    }

}
