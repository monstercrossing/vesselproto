﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Dash Action")]
public class ActionDuringDashHook : HookBehaviour {
    public HookBehaviour hook;

    CharacterEventHub hub;
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        hub = callerObject.transform.GetOrAddComponent<CharacterEventHub>();

        hub.onDashStart.AddListener(TurnOn);
        hub.onDashEnd.AddListener(TurnOff);
    }

    public override void Remove(MonoBehaviour callerObject) {
        hub.onDashStart.RemoveListener(TurnOn);
        hub.onDashEnd.RemoveListener(TurnOff);
    }

    public void TurnOn(MonoBehaviour callerObject) {
        if (hook) hook.Run(callerObject);
    }
    public void TurnOff(MonoBehaviour callerObject) {
        if (hook) hook.Remove(callerObject);
    }
}
