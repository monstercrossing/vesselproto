﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class StamOverlay : MonoBehaviour
{
    public GameEvent OnSteamOverlayOpen;
    public GameEvent OnSteamOverlayClose;
    protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;
    private float localTimescale;
	private void OnEnable() {
		if (SteamManager.Initialized) {
			m_GameOverlayActivated = Callback<GameOverlayActivated_t>.Create(OnGameOverlayActivated);
		}
	}

    private void OnGameOverlayActivated(GameOverlayActivated_t pCallback) {
		if(pCallback.m_bActive != 0) {
			Debug.Log("Steam Overlay has been activated");
            if(OnSteamOverlayOpen) OnSteamOverlayOpen.Raise();
		}
		else {
			Debug.Log("Steam Overlay has been closed");
            //if(OnSteamOverlayClose) OnSteamOverlayClose.Raise();
			//Time.timeScale = 1;
		}
	}
}
