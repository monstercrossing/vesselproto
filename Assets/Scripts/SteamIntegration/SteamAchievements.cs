﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamAchievements : MonoBehaviour
{
private enum Achievement : int {
		ach_defeat_dog,
		ach_defeat_machine,
		ach_defeat_anton,
		ach_defeat_demon,
		ach_powers,
	};


	private Achievement_t[] m_Achievements = new Achievement_t[] {
		new Achievement_t(Achievement.ach_defeat_dog, "Dog", ""),
		new Achievement_t(Achievement.ach_defeat_machine, "Copier", ""),
		new Achievement_t(Achievement.ach_defeat_anton, "Anton", ""),
        new Achievement_t(Achievement.ach_defeat_demon, "Demon", ""),
		new Achievement_t(Achievement.ach_powers, "Powers", "")
	};

	// Our GameID
	private CGameID m_GameID;

	// Did we get the stats from Steam?
	private bool m_bRequestedStats;
	private bool m_bStatsValid;

	// Should we store stats this frame?
	private bool m_bStoreStats;

	// Persisted Stat details
	private int m_nTotalPowersAbsorbed;
	private int m_nTotalEgosCollected;
	private int m_nTotalPermanentsCollected;
	private int m_nTotalDaysPlayed;

	protected Callback<UserStatsReceived_t> m_UserStatsReceived;
	protected Callback<UserStatsStored_t> m_UserStatsStored;
	protected Callback<UserAchievementStored_t> m_UserAchievementStored;

	void OnEnable() {
		if (!SteamManager.Initialized)
			return;

		// Cache the GameID for use in the Callbacks
		m_GameID = new CGameID(SteamUtils.GetAppID());

		m_UserStatsReceived = Callback<UserStatsReceived_t>.Create(OnUserStatsReceived);
		m_UserStatsStored = Callback<UserStatsStored_t>.Create(OnUserStatsStored);
		m_UserAchievementStored = Callback<UserAchievementStored_t>.Create(OnAchievementStored);

		// These need to be reset to get the stats upon an Assembly reload in the Editor.
		m_bRequestedStats = false;
		m_bStatsValid = false;

        UpdateBossAchievements();
	}

	private void Update() {
		if (!SteamManager.Initialized)
			return;

		if (!m_bRequestedStats) {
			// Is Steam Loaded? if no, can't get stats, done
			if (!SteamManager.Initialized) {
				m_bRequestedStats = true;
				return;
			}
			
			// If yes, request our stats
			bool bSuccess = SteamUserStats.RequestCurrentStats();

			// This function should only return false if we weren't logged in, and we already checked that.
			// But handle it being false again anyway, just ask again later.
			m_bRequestedStats = bSuccess;
		}

		if (!m_bStatsValid)
			return;

		// Get info from sources

		// Evaluate achievements
		foreach (Achievement_t achievement in m_Achievements) {
			if (achievement.m_bAchieved)
				continue;

            /* CHECK HERE
			switch (achievement.m_eAchievementID) {
				case Achievement.ACH_WIN_ONE_GAME:
					if (m_nTotalNumWins != 0) {
						UnlockAchievement(achievement);
					}
					break;
				case Achievement.ACH_WIN_100_GAMES:
					if (m_nTotalNumWins >= 100) {
						UnlockAchievement(achievement);
					}
					break;
				case Achievement.ACH_TRAVEL_FAR_ACCUM:
					if (m_flTotalFeetTraveled >= 5280) {
						UnlockAchievement(achievement);
					}
					break;
				case Achievement.ACH_TRAVEL_FAR_SINGLE:
					if (m_flGameFeetTraveled >= 500) {
						UnlockAchievement(achievement);
					}
					break;
			}
            //*/
		}

		//Store stats in the Steam database if necessary
		if (m_bStoreStats) {
            // already set any achievements in UnlockAchievement
            Log.Text("Will Save Steam Stat - Powers:", m_nTotalPowersAbsorbed);
            // set stats
            SteamUserStats.SetStat("stat_powers", m_nTotalPowersAbsorbed);
			SteamUserStats.SetStat("stat_egos", m_nTotalEgosCollected);
			SteamUserStats.SetStat("stat_perms", m_nTotalPermanentsCollected);
			SteamUserStats.SetStat("stat_days", m_nTotalDaysPlayed);

			bool bSuccess = SteamUserStats.StoreStats();
			// If this failed, we never sent anything to the server, try
			// again later.
			m_bStoreStats = !bSuccess;
		}
	}

	//-----------------------------------------------------------------------------
	// Purpose: Accumulate distance traveled
	//-----------------------------------------------------------------------------
	public void AddPowersTotal() {
		m_nTotalPowersAbsorbed += 1;
        m_bStoreStats = true;
	}

	public void SetEgosCollected(int egos) {
		if(egos > m_nTotalEgosCollected)
			m_nTotalEgosCollected = egos;

			 m_bStoreStats = true;
	}

	public void SetPermanentsCollected(int perms) {
		if(perms > m_nTotalPermanentsCollected)
			m_nTotalPermanentsCollected = perms;

			 m_bStoreStats = true;
	}

	public void SetDaysPlayed(int days) {
		if(days > m_nTotalDaysPlayed)
			m_nTotalDaysPlayed = days;

			 m_bStoreStats = true;
	}

    public void UpdateBossAchievements(){
        SaveSlot currentSave = SaveSlotController.GetCurrentSave();
        if(currentSave == null) return;
        // Evaluate achievements
		foreach (Achievement_t achievement in m_Achievements) {
			if (achievement.m_bAchieved)
				continue;

			switch (achievement.m_eAchievementID) {
				case Achievement.ach_defeat_dog:
					if (currentSave.defeatedBosses[0]) {
						UnlockAchievement(achievement);
					}
					break;
				case Achievement.ach_defeat_machine:
					if (currentSave.defeatedBosses[1]) {
						UnlockAchievement(achievement);
					}
					break;
				case Achievement.ach_defeat_anton:
					if (currentSave.defeatedBosses[2]) {
						UnlockAchievement(achievement);
					}
					break;
				case Achievement.ach_defeat_demon:
					if (currentSave.defeatedBosses[3]) {
						UnlockAchievement(achievement);
					}
					break;
			}
        }
        
    }

	//-----------------------------------------------------------------------------
	// Purpose: Unlock this achievement
	//-----------------------------------------------------------------------------
	private void UnlockAchievement(Achievement_t achievement) {
		achievement.m_bAchieved = true;

		// the icon may change once it's unlocked
		//achievement.m_iIconImage = 0;

		// mark it down
		SteamUserStats.SetAchievement(achievement.m_eAchievementID.ToString());

		// Store stats end of frame
		m_bStoreStats = true;
	}
	
	//-----------------------------------------------------------------------------
	// Purpose: We have stats data from Steam. It is authoritative, so update
	//			our data with those results now.
	//-----------------------------------------------------------------------------
	private void OnUserStatsReceived(UserStatsReceived_t pCallback) {
		if (!SteamManager.Initialized)
			return;

		// we may get callbacks for other games' stats arriving, ignore them
		if ((ulong)m_GameID == pCallback.m_nGameID) {
			if (EResult.k_EResultOK == pCallback.m_eResult) {
				Debug.Log("Received stats and achievements from Steam\n");

				m_bStatsValid = true;

				// load achievements
				foreach (Achievement_t ach in m_Achievements) {
					bool ret = SteamUserStats.GetAchievement(ach.m_eAchievementID.ToString(), out ach.m_bAchieved);
					if (ret) {
						ach.m_strName = SteamUserStats.GetAchievementDisplayAttribute(ach.m_eAchievementID.ToString(), "name");
						ach.m_strDescription = SteamUserStats.GetAchievementDisplayAttribute(ach.m_eAchievementID.ToString(), "desc");
					}
					else {
						Debug.LogWarning("SteamUserStats.GetAchievement failed for Achievement " + ach.m_eAchievementID + "\nIs it registered in the Steam Partner site?");
					}
				}

				// load stats
				SteamUserStats.GetStat("stat_powers", out m_nTotalPowersAbsorbed);
				SteamUserStats.GetStat("stat_perms", out m_nTotalPermanentsCollected);
				SteamUserStats.GetStat("stat_egos", out m_nTotalEgosCollected);
				SteamUserStats.GetStat("stat_days", out m_nTotalDaysPlayed);
                Log.Text("Steam Stat - Powers:", m_nTotalPowersAbsorbed);
			}
			else {
				Debug.Log("RequestStats - failed, " + pCallback.m_eResult);
			}
		}
	}

	//-----------------------------------------------------------------------------
	// Purpose: Our stats data was stored!
	//-----------------------------------------------------------------------------
	private void OnUserStatsStored(UserStatsStored_t pCallback) {
		// we may get callbacks for other games' stats arriving, ignore them
		if ((ulong)m_GameID == pCallback.m_nGameID) {
			if (EResult.k_EResultOK == pCallback.m_eResult) {
				Debug.Log("StoreStats - success");
			}
			else if (EResult.k_EResultInvalidParam == pCallback.m_eResult) {
				// One or more stats we set broke a constraint. They've been reverted,
				// and we should re-iterate the values now to keep in sync.
				Debug.Log("StoreStats - some failed to validate");
				// Fake up a callback here so that we re-load the values.
				UserStatsReceived_t callback = new UserStatsReceived_t();
				callback.m_eResult = EResult.k_EResultOK;
				callback.m_nGameID = (ulong)m_GameID;
				OnUserStatsReceived(callback);
			}
			else {
				Debug.Log("StoreStats - failed, " + pCallback.m_eResult);
			}
		}
	}

	//-----------------------------------------------------------------------------
	// Purpose: An achievement was stored
	//-----------------------------------------------------------------------------
	private void OnAchievementStored(UserAchievementStored_t pCallback) {
		// We may get callbacks for other games' stats arriving, ignore them
		if ((ulong)m_GameID == pCallback.m_nGameID) {
			if (0 == pCallback.m_nMaxProgress) {
				Debug.Log("Achievement '" + pCallback.m_rgchAchievementName + "' unlocked!");
			}
			else {
				Debug.Log("Achievement '" + pCallback.m_rgchAchievementName + "' progress callback, (" + pCallback.m_nCurProgress + "," + pCallback.m_nMaxProgress + ")");
			}
		}
	}
	
	private class Achievement_t {
		public Achievement m_eAchievementID;
		public string m_strName;
		public string m_strDescription;
		public bool m_bAchieved;

		/// <summary>
		/// Creates an Achievement. You must also mirror the data provided here in https://partner.steamgames.com/apps/achievements/yourappid
		/// </summary>
		/// <param name="achievement">The "API Name Progress Stat" used to uniquely identify the achievement.</param>
		/// <param name="name">The "Display Name" that will be shown to players in game and on the Steam Community.</param>
		/// <param name="desc">The "Description" that will be shown to players in game and on the Steam Community.</param>
		public Achievement_t(Achievement achievementID, string name, string desc) {
			m_eAchievementID = achievementID;
			m_strName = name;
			m_strDescription = desc;
			m_bAchieved = false;
		}
    }   
	
}
