﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamRichPresence : MonoBehaviour
{
    private int day = 0;
    public void Start() {
        if(SaveSlotController.GetCurrentSave() != null)
            day = SaveSlotController.GetCurrentSave().deathCount;

        StartCoroutine(UpdateRichPresence());
    }

    IEnumerator UpdateRichPresence() {
        bool keepGoing = true;
        while(keepGoing)
        {
            float timer = Time.timeSinceLevelLoad;
            string minutes = Mathf.Floor(timer / 60).ToString("00");
            string seconds = (timer % 60).ToString("00");
            string timeCode = minutes + ":" + seconds;
            if(SteamManager.Initialized) {
                SteamFriends.SetRichPresence("run_time", timeCode);
                SteamFriends.SetRichPresence("run_day", day.ToString());
                SteamFriends.SetRichPresence("steam_display", "#state_run: #day %run_day%, %run_time%");
            }
            yield return new WaitForSeconds(1);
        }
    }

    public void SetString(string textToShow)
    {
        
        if(SteamManager.Initialized) {
            Log.Text("Testing Rich Presence");
            SteamFriends.SetRichPresence("steam_display", textToShow);
        }
    }

    public void OnDestroy() {
       if(SteamManager.Initialized) {
            SteamFriends.ClearRichPresence();
        }
    }
}
