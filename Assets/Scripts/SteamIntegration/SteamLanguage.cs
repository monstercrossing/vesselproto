﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamLanguage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(SteamManager.Initialized) {
            if(!PlayerPrefs.HasKey("longway.lang")) {
                string lang = SteamUtils.GetSteamUILanguage();
                if(lang == "portuguese")
                    PlayerPrefs.SetString("longway.lang", "pt_BR");
                else
                    PlayerPrefs.SetString("longway.lang", "en_US");
            }
        }   
    }
}
