﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "MonsterCrossing/Level/Chunk")]
public class LevelChunk : MonoBehaviour {
    public Biome biome;

    public Tilemap map;
    public TileBase commonTile;
    private TileBase biomeTile;

    public Tilemap background;
    public TileBase commonBg;
    private TileBase biomeBg;

    public bool canRepeat;
    private static bool hasBeenUsed = false;

    public int blocksX = 1;
    public int blocksY = 1;
    public string previousChunk = "";
    public string realName = "";
    public int enteredBy = -1;
    public int exitedBy = -1;
    public int chunkListIndex = -1;
    public List<int> matrixCoordX = new List<int>();
    public List<int> matrixCoordY = new List<int>();

    public bool[] allowedEntrances = { false, false, false, false }; //N, E, S, W
    public bool[] allowedExits = { false, false, false, false }; //N, E, S, W

    public PermanentDefinition[] unlockedPermanents;

    public TileBase[] doorPlaceholders = new TileBase[4];

    public bool[] directions = {false,false,false,false}; //N, E, S, W    

    public void Start()
    {
        if (biome)
        {
            biomeTile = biome.biomeTile;
            biomeBg = biome.biomeBg;

            Initialize();
        }
    }

    public void Initialize() {
        BoundsInt bounds = map.cellBounds;
        TileBase[] allTiles = map.GetTilesBlock(bounds);

        foreach (var pos in map.cellBounds.allPositionsWithin) {
            Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
            Vector3 place = map.CellToWorld(localPlace);
            if (map.HasTile(localPlace)) {
                TileBase tile = map.GetTile(localPlace);
                for (int d = 0; d < 4; d++) {
                    if (tile == doorPlaceholders[d] && !directions[d]) {
                        map.SetTile(localPlace, biomeTile);
                    } else if (tile == doorPlaceholders[d] && directions[d]) {
                        map.SetTile(localPlace, null);
                    }
                }

                if (tile == commonTile)
                    map.SetTile(localPlace, biomeTile);
            }
        }

        //BG
        if (background) {
            BoundsInt bgbounds = background.cellBounds;

            foreach (var pos in bgbounds.allPositionsWithin) {
                Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
                Vector3 place = background.CellToWorld(localPlace);
                if (background.HasTile(localPlace)) {
                    TileBase tile = background.GetTile(localPlace);
                    if (tile == commonBg)
                        background.SetTile(localPlace, biomeBg);
                }
            }
        }

        //Props
        foreach(BiomeProp prop in GetComponentsInChildren<BiomeProp>()) {
            prop.SetBiome(biome);
        }

        foreach(ConditionalProp prop in GetComponentsInChildren<ConditionalProp>()) {
            prop.Initialize(unlockedPermanents);
        }
    }

    public void MutateChunk(){
        GameObject camera = Camera.main.gameObject;
        if (!camera) return;
        AudioSource audio = camera.GetComponent<AudioSource>();
        if (!audio) return;
        audio.Pause();
    }

    public void UnMutateChunk()
    {
        GameObject camera = Camera.main.gameObject;
        if (!camera) return;
        AudioSource audio = camera.GetComponent<AudioSource>();
        if (!audio) return;
        audio.UnPause();
    }

    public Bounds GetBounds()
    {
        // Generate bounds
        //Bounds trueBounds = new Bounds(transform.position, Vector3.zero);
        Bounds trueBounds = new Bounds(Vector3.zero, Vector3.zero);

        foreach (Transform child in transform)
        {
            if (child.tag == "Tilemap") {
                Bounds baux = child.GetComponent<TilemapRenderer>().bounds;
                baux.center = Vector3.zero;
                baux.size = new Vector3(Mathf.Floor(baux.extents.x) * 2, Mathf.Floor(baux.extents.y) * 2, Mathf.Floor(baux.extents.z) * 2);
                trueBounds.Encapsulate(baux);
                //trueBounds.Encapsulate(child.GetComponent<TilemapRenderer>().bounds);
            }
        }

        trueBounds.center = Vector3.zero;
        return trueBounds;
    }

    public List<int> GetExits()
    {
        List<int> exits = new List<int>();
        for (int i = 0; i < allowedExits.Length; i++)
        {
            if (allowedExits[i]) { exits.Add(i); }
        }
        return exits;
    }

    public List<int> GetEntrances()
    {
        List<int> entrances = new List<int>();
        for (int i = 0; i < allowedEntrances.Length; i++)
        {
            if (allowedEntrances[i]) { entrances.Add(i); }
        }
        return entrances;
    }

    public List<PermanentDefinition> GetRequirements()
    {
        List<PermanentDefinition> requirements = new List<PermanentDefinition>();
        for (int i = 0; i < unlockedPermanents.Length; i++)
        {
            if (unlockedPermanents[i]) { requirements.Add(unlockedPermanents[i]); }
        }
        return requirements;
    }
}
