﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;

[CreateAssetMenu(menuName = "MonsterCrossing/Hooks/Trap Collision")]
public class AddTrapCollisionHook : HookBehaviour
{
    public IntVariable trapDamage;
    public FloatVariable knockBackImpulse;
    TrapCollision thorns;
    // Start is called before the first frame update
    public override void Run(MonoBehaviour callerObject) {
        base.Run(callerObject);
        thorns = callerObject.transform.GetOrAddComponent<TrapCollision>();
        thorns.powerupDefinition = callerObject.GetComponent<BaseCharacter>().CurrentPowerup();

        if(trapDamage)
            thorns.trapDamage = trapDamage;
        if(knockBackImpulse)
            thorns.knockbackImpulse = knockBackImpulse;
    }

    public override void Remove(MonoBehaviour callerObject) {
        Destroy(thorns);
    }
}
