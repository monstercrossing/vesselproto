﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/GameObjectList")]
public class GameObjectPool : ScriptableObject
{
    public List<GameObject> fullList;
}
