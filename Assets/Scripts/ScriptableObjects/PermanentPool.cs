﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/PermanentList")]
public class PermanentPool : ScriptableObject
{
    public List<PermanentDefinition> fullList;
}
