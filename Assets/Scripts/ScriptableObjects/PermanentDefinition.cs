﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterCrossing.System.UI;

[CreateAssetMenu(menuName = "MonsterCrossing/Permanent")]
public class PermanentDefinition : ScriptableObject {
    public Sprite icon;
    public bool autoCollect = false;

    public HookBehaviour[] hooks;

    public LocalizationTemplate template;
    public StringTable language;
       
    [LocalizedText]
    public int ingameName;
    [LocalizedText]
    public int ingameDescription;
    //textObject.text = language.translatedTexts[stringIndex];

    [LocalizedText]
    public int fallbackName;
    [LocalizedText]
    public int fallbackDescription;

    public void ApplyValues(MonoBehaviour callerObject) {
        for (int i = 0; i < hooks.Length; i++) {
            hooks[i].Run(callerObject);
        }
    }

    public void Unregister(MonoBehaviour callerObject) {
        for (int i = 0; i < hooks.Length; i++) {
            hooks[i].Remove(callerObject);
        }
    }
}
