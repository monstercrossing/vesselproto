﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Projectile")]
public class ProjectileDefinition : ScriptableObject {
    public float shootSpeed;
    public float fireRate;
    public float lifeTime = 1f;
    public int damage = 1;
    public float stunTime = 0.0f;
    public float areaOfEffect = 0;

    public void ApplyValuesToAnimator(Animator animator) {
        animator.SetFloat("ShootSpeed", shootSpeed);
        animator.SetFloat("FireRate", fireRate);
    }
 }
