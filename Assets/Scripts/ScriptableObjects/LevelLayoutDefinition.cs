﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoomDefinition {
    [System.Flags]
    ///*
    public enum RoomFlags {
        None = 0x00,
        NeedsDoubleJump = 0x01,
        NeedsDash = 0x02,
        NeedsWallClimb = 0x04,
        HasSecretPassage = 0x08,
        HasTreasure = 0x10,
        MainPath = 0x20,
        Boss = 0x40,
        Starting = 0x80,
        Ending = 0x100
    }

    [EnumFlagAttribute]
    public RoomFlags flags;
    public bool[] openings = { false, false, false, false }; //N,E,S,W

    public int numOpenings {
        get {
            int _num = 0;
            for(int i=0;i<openings.Length;i++) {
                if (openings[i]) _num++;
            }
            return _num;
        }
    }
}

[CreateAssetMenu(menuName = "MonsterCrossing/Level/Layout")]
public class LevelLayoutDefinition : ScriptableObject {
    public Vector2 levelSize = new Vector2 (3,2);
    public Vector2 startingRoom = new Vector2 (0,0);
    private Vector2 endingRoom = new Vector2(2,1);
    public int minPathSize = 5;

    [HideInInspector]
    public bool generated;

    private RoomDefinition[,] _rooms;
    public RoomDefinition[,] rooms {
        get {
            return _rooms;
        }
    }

    public void GenerateMap() {
        Debug.Log("Generate the Map");

        _rooms = new RoomDefinition[(int)levelSize.x, (int)levelSize.y];
        for(int i=0;i<_rooms.GetLength(0);i++) {
            for (int j = 0; j < _rooms.GetLength(1); j++) {
                _rooms[i, j] = new RoomDefinition();
            }
        }
               
        _rooms[(int)startingRoom.x, (int)startingRoom.y].flags = RoomDefinition.RoomFlags.Starting;
        //_rooms[(int)endingRoom.x, (int)endingRoom.y].flags = RoomDefinition.RoomFlags.Ending;

        bool pathFound = false;
        //do {
            pathFound = FindPath((int)startingRoom.x, (int)startingRoom.y, null, 0);
        //} while (!pathFound);
        generated = true;
    }

    public bool FindPath(int x, int y, RoomDefinition previous, int distanceTraveled) {
        if (!ValidOpening(x, y)) return false;
        RoomDefinition current = _rooms[x, y];
        //if ((current.flags & RoomDefinition.RoomFlags.Ending) != 0) return true;
        if (distanceTraveled >= minPathSize) {
            current.flags |= RoomDefinition.RoomFlags.Ending;
            return true;
        }
        //current.flags |= RoomDefinition.RoomFlags.MainPath;
        int lookup;
        if (distanceTraveled == 0) {
            lookup = 1;
        } else {
            
                lookup = (int)Mathf.FloorToInt(Random.Range(0, 2.4f));
            
        }

        if (lookup == 0) {
            if (FindPath(x, y + 1, current, distanceTraveled + 1)) { //N
                current.flags |= RoomDefinition.RoomFlags.MainPath;
                _rooms[x, y + 1].openings[2] = true;
                current.openings[0] = true;
                return true;
            } else {
                lookup++;
            }
        }
        if (lookup == 1) {
            if (FindPath(x + 1, y, current, distanceTraveled + 1)) { //E
                current.flags |= RoomDefinition.RoomFlags.MainPath;
                _rooms[x + 1, y].openings[3] = true;
                current.openings[1] = true;
                return true;
            } else {
                lookup++;
            }
        }
        if (lookup == 2) {
            if (FindPath(x, y - 1, current, distanceTraveled + 1)) { //S
                current.flags |= RoomDefinition.RoomFlags.MainPath;
                _rooms[x, y - 1].openings[0] = true;
                current.openings[2] = true;
                return true;
            } else {
                lookup=0;
            }
        }
        if (lookup == 3) {
            if (FindPath(x - 1, y, current, distanceTraveled + 1)) { //W
                current.flags |= RoomDefinition.RoomFlags.MainPath;
                _rooms[x - 1, y].openings[1] = true;
                current.openings[3] = true;
                return true;
            } else {
                lookup = 0;
            }
        }

        current.flags &= RoomDefinition.RoomFlags.MainPath;
        if (distanceTraveled >= minPathSize) {
            current.flags |= RoomDefinition.RoomFlags.Ending;
            return true;
        } else {
            FindPath(x, y, current, distanceTraveled);
        }
        return false;
    }



    public bool ValidOpening(int x, int y) {
        if (x < 0 || x >= (int)levelSize.x || y < 0 || y >= (int)levelSize.y) return false;
        if ((_rooms[x, y].flags & RoomDefinition.RoomFlags.MainPath) != 0) return false; //already on main path
        return true;
    }

}
