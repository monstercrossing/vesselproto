﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "MonsterCrossing/Level/Biome")]
public class Biome : ScriptableObject
{
    public TileBase biomeTile;
    public TileBase biomeBg;

}
