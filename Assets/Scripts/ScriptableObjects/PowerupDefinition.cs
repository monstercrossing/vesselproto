﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

[CreateAssetMenu(menuName = "MonsterCrossing/Powerup")]
public class PowerupDefinition : ScriptableObject {
    [System.Flags]
    ///*
    public enum AttackType {
        None = 0x00,
        Punch = 0x01,
        Melee = 0x02,
        Ranged = 0x04,
        Grenade = 0x08,
        Shield = 0x10,
        AoE = 0x20,
        DoT = 0x40
    }

    public Sprite icon;

    [EnumFlagAttribute]
    public AttackType attackType;
    public bool canShoot;
    public bool canMove;
    public bool isMelee = false;
    public bool isPunch = false;
    public bool canAttackWhileMoving = false;
    public bool autoShoot = false;

    public int damageOnDash = 0;

    public GameObject projectilePrefab;
    public GameObject chargedProjectilePrefab;

    public Texture2D customPalette;
    public Color customColor;

    public void ApplyValuesToAnimator(Animator animator) {
        animator.SetBool("CanShoot", canShoot);
        animator.SetBool("CanMove", canMove);
        animator.SetBool("IsMelee", isMelee);
        animator.SetBool("IsPunch", isPunch);

        animator.SetBool("CanAttackWhileMoving", canAttackWhileMoving);
        animator.SetBool("AutoShoot", autoShoot);
        //animator.SetFloat("MoveSpeed", moveSpeed);
    }
}
