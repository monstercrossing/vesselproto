﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMController : MonoBehaviour {
	private int bgmnow = 1;
	private string bgmname = "";
	private AudioClip current;
	private string[] sBGMS = { "bgm_1", "bgm_2", "bgm_3", "bgm_4" };

	public AudioClip[] songs;
	public float transitionTime = 0.1f;

	private AudioSource[] aSources;

	private AudioSource bgm_1;
	private AudioSource bgm_2;

	private Coroutine bgmRoutine;
	private bool decreasingPitch = false;
    private int bgmaux = 2;

    // Use this for initialization
    void Start () {

		aSources = GetComponents<AudioSource> ();
		bgm_1 = aSources[0];
		bgm_2 = aSources[1];
		bgmnow = 1;
	}
	
	// Update is called once per frame
	void Update () {
		if (decreasingPitch && bgm_1.pitch > 0.2f) {	
				bgm_2.pitch -= transitionTime;
				bgm_1.pitch -= transitionTime;
		} else if(!decreasingPitch && bgm_1.pitch < 1f){
				bgm_2.pitch += transitionTime;
				bgm_1.pitch += transitionTime;
		}

        if (bgmaux == 1) {
            if (bgm_2.volume < 1f) {
                bgm_2.volume += (transitionTime * Time.deltaTime);
                bgm_1.volume -= (transitionTime * Time.deltaTime);
            }
        } else {
            if (bgm_1.volume < 1f) {
                bgm_1.volume += (transitionTime * Time.deltaTime);
                bgm_2.volume -= (transitionTime * Time.deltaTime);
            }
        }
    }
	
	public void BGMChange (int songIndex) {
        string newbgm = bgmname;
		AudioClip newClip;
		if(songIndex >= 0)	
			 newClip = songs[songIndex];
		else
			newClip = null;

		if (bgmnow == 1) {
			bgm_2.volume = 0f;
			bgm_2.clip = newClip;//(AudioClip)Resources.Load (string.Concat ("BGM/", newbgm));
			bgm_2.loop = true;
			bgm_2.time = bgm_1.time;
			bgm_2.Play(0);
            bgmaux = 1;
			//bgmRoutine = StartCoroutine(BGMTransition(transitionTime/100,bgmnow));
			bgmnow = 2;
		} else {
			bgm_1.volume = 0f;
			bgm_1.clip = newClip; //(AudioClip)Resources.Load (string.Concat ("BGM/", newbgm));
			bgm_1.loop = true;
			bgm_1.time = bgm_2.time;
			bgm_1.Play(0);
            //bgmRoutine = StartCoroutine(BGMTransition(transitionTime/100,bgmnow));
            bgmaux = 2;
            bgmnow = 1;
		}		
	}

	public void ReducePitch(){
		Debug.Log("PITCH REDUCE");
		//StartCoroutine(PitchTransition(transitionTime, 0));
		decreasingPitch = true;
	}
	public void IncreasePitch(){
		//StartCoroutine(PitchTransition(transitionTime, 1));
		decreasingPitch = false;
	}

	IEnumerator PitchTransition(float time, int bgmaux) {
		//float elapsedTime = 0;
		if (bgmaux == 1) {
			while (bgm_1.pitch < 1f) {
				
					bgm_2.pitch += 0.01f;
					bgm_1.pitch += 0.01f;
				
				yield return new WaitForSeconds (time);
			}
		} else {
			while (bgm_1.pitch > 0.2f) {
				//if(elapsedTime > 0.5f) {
					bgm_1.pitch -= 0.01f;
					bgm_2.pitch -= 0.01f;
				//}
				//elapsedTime += time;
				yield return new WaitForSeconds (time);
			}
		}
	}

	IEnumerator BGMTransition(float time, int bgmaux)
	{
		Debug.Log (bgm_2.volume);

		if (bgmaux == 1) {
			while (bgm_2.volume < 1f) {
				bgm_2.volume += 0.1f;
				bgm_1.volume -= 0.1f;

				yield return new WaitForSeconds (time);
			}
		} else {
			while (bgm_1.volume < 1f) {
				bgm_1.volume += 0.1f;
				bgm_2.volume -= 0.1f;

				yield return new WaitForSeconds (time);
			}
		}
	}
}
