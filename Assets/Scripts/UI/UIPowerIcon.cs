﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPowerIcon : MonoBehaviour
{
    public BaseCharacter character;
    private UnityEngine.UI.Image icon;
    private void Start() {
        if (!character)
            character = GameObject.FindGameObjectWithTag("Hero").GetComponent<BaseCharacter>();

        icon = GetComponent<UnityEngine.UI.Image>();
    }
    private void Update() {
        try
        {
            icon.sprite = character.CurrentPowerup().icon;
        }
        catch(System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}
