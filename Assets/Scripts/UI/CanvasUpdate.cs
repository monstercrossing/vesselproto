﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasUpdate : MonoBehaviour
{
    private bool facingRight = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        FaceRight();
    }

    private void FaceRight()
    {
        GameObject pai = gameObject.transform.parent.gameObject;
        bool face = pai.GetComponent<FlipSprite>().facingRight;

        Vector3 canvasScale = gameObject.transform.localScale;
        if (face != facingRight)
        {
            canvasScale.x *= -1;
        }
        gameObject.transform.localScale = canvasScale;
        facingRight = face;
    }
}