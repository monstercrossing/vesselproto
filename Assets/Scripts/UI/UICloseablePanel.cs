﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICloseablePanel : MonoBehaviour {
    public GameObject parentMenu;
    // Update is called once per frame
    void Update() {
        if(Input.GetButtonUp("Cancel") && gameObject.activeSelf) {
            gameObject.SetActive(false);

            if (parentMenu) parentMenu.SetActive(true);
        }
    }
}
