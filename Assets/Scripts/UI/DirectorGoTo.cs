﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

[RequireComponent(typeof(PlayableDirector))]
public class DirectorGoTo : MonoBehaviour {
    PlayableDirector director;

    public void GoToFrame(float frame) {
        if(!director)
        director = GetComponent<PlayableDirector>();
        director.time = frame;
    }

    public void Stop() {
        if (!director)
            director = GetComponent<PlayableDirector>();

        director.Stop();
        director.Evaluate();
    }
}
