﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class SaveSlot {
    public bool empty = true;
    public bool bossRushEnabled = false;
    public bool tutorialCompleted = false;

    public bool[] defeatedBosses = {false,false,false,false}; //in order: dog, copier, half demon, full demon;
    public int deathCount = 1;
    public float playTime = 0;

    public List<SerializablePermanentInfo> permanents;
    public List<SerializablePermanentInfo> egos;

    public void Wipe() {
        empty = true;
        bossRushEnabled = false;
        tutorialCompleted = false;

        for(int i = 0; i < defeatedBosses.Length; i ++) {
            defeatedBosses[i] = false;
        }
        //in order: dog, copier, half demon, full demon;
        deathCount = 1;
        playTime = 0;

        permanents = new List<SerializablePermanentInfo>();
        egos = new List<SerializablePermanentInfo>();
    }
}

public class SaveSlotController : MonoBehaviour {
    public string sceneToLoad;
    public GameObject panel;
    public GameObject slotTemplate;
    public GameEvent OnLoadDone;
    public GameEvent OnSaveDone;
    public GameEvent OnNewGame;
    public GameEvent OnLoadGame;

    [HideInInspector]
    public static List<SaveSlot> savedGames;
    public static int currentSlot;
    public static GameEvent OnLoadDoneEvent;
    public static GameEvent OnSaveDoneEvent;
    public static GameEvent OnNewGameRequest;
    public static GameEvent OnLoadGameRequest;

    public static bool shouldLoadTutorial = false;

    public void Start() {
        if(OnLoadDone) OnLoadDoneEvent = OnLoadDone;
        if(OnSaveDone) OnSaveDoneEvent = OnSaveDone;
        if (OnNewGame) OnNewGameRequest = OnNewGame;
        if(OnLoadGame) OnLoadGameRequest = OnLoadGame;
        if(savedGames == null)
            savedGames = new List<SaveSlot>(3);
        LoadSlots();
        /*
        for(int i = 0; i < 3; i++) {
            GameObject slot = Instantiate(slotTemplate);
            slot.transform.parent = panel.transform;
            slot.transform.localScale = new Vector3(1, 1, 1);
        }
        */
    }

    public void LoadTutorialScene() {
        shouldLoadTutorial = true;
        StartCoroutine(AsyncLoad("Level"));
    }

    public void LoadMenuScene() {
        SceneManager.LoadScene("IntroScene");
    }

    IEnumerator AsyncLoad(string scene) {
        Time.timeScale = 1;
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }

    public void BeginNewGame(int slot) {
        currentSlot = slot;
        if(savedGames[currentSlot].empty) {
            if(OnNewGameRequest)
                OnNewGameRequest.Raise();
            //SceneManager.LoadScene(1);
        }else{
            shouldLoadTutorial = !savedGames[slot].tutorialCompleted;
            if(OnLoadGameRequest)
                OnLoadGameRequest.Raise();
            //SceneManager.LoadScene("Level");
            StartCoroutine(AsyncLoad("Level"));
            //StartCoroutine(AsyncLoad(Random.Range(2, 4)));
        }
    }

    public static void SaveSlots() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, savedGames);
        file.Close();

        if(OnSaveDoneEvent) OnSaveDoneEvent.Raise();
    }

    public static void LoadSlots() {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            try {
                SaveSlotController.savedGames = (List<SaveSlot>)bf.Deserialize(file);
            }catch(System.Exception error) {
                Log.Text(error);
            }
            file.Close();
            foreach(SaveSlot slot in SaveSlotController.savedGames) {
                if(!slot.empty) {
                    Log.Text("Save Slot info:", slot.deathCount, slot.egos.Count, slot.permanents.Count);
                    //Verify Achievements here
                    GameObject steamworksObject = GameObject.Find("steamworks");
                    if(steamworksObject) {
                        SteamAchievements achievements = steamworksObject.GetComponent<SteamAchievements>();
                        if(achievements) {
                            int totalEgos = slot.egos.Count;
                            int totalPermanents = slot.permanents.Count;
                            int collectedEgos = 0;
                            int collectedPermanents = 0;

                            for(int i=0; i < totalEgos; i++) {
                                if(slot.egos[i].collected) collectedEgos++;
                            }

                            for(int i=0; i < totalPermanents; i++) {
                                if(slot.permanents[i].collected) collectedPermanents++;
                            }
                            achievements.SetEgosCollected(collectedEgos);
                            achievements.SetPermanentsCollected(collectedPermanents);
                            achievements.SetDaysPlayed(slot.deathCount);
                        }
                    }
                }
            }
        } else {
            for(int i=0; i< 3; i++) {
                savedGames.Add(new SaveSlot());
            }
            SaveSlots();
        }
        if(OnLoadDoneEvent) OnLoadDoneEvent.Raise();
        
    }

    public static SaveSlot GetCurrentSave() {
        try
        {
            return savedGames[currentSlot];
        }
        catch(System.Exception e)
        {
            Debug.Log(e.Message);
            return null;
        }
    }

    public static void SetCurrentSave(SaveSlot newSlot) {
        savedGames[currentSlot] = newSlot;
    }
}
