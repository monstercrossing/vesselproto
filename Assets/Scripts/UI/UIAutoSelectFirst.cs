﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIAutoSelectFirst : MonoBehaviour {
    public GameObject firstObject;

    void Start() {
       StartCoroutine( SelectInitial());
    }

    private void OnEnable() {
        StartCoroutine( SelectInitial() );
    }

    IEnumerator SelectInitial() {
        EventSystem.current.SetSelectedGameObject(null);
        yield return new WaitForEndOfFrame();
        if(firstObject) {
            try {
                EventSystem.current.SetSelectedGameObject(firstObject);
            } catch (System.Exception e) {

            }
            yield break;
        }

        int index = 0;
        //Debug.Log("CHILD COUNT = " + container.childCount);
        while (!transform.GetChild(index).gameObject.activeSelf && index + 1 < transform.childCount) {
            //Debug.Log("INDEX = " + index);
            index++;
        }

        try {
            EventSystem.current.SetSelectedGameObject(transform.GetChild(index).gameObject);
        } catch (System.Exception e) {

        }

        yield break;
    }

}
