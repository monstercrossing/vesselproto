﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkSprite : MonoBehaviour
{

    public Color blinkColor;
    public Color invincibilityColor;
    public Material blinkMaterial;

    private Material _originalMaterial;

    public float flashTime = 0.25f;
    public bool invulnerabilityBlink = false;

    private Color _originalColor;
    private bool isFlashing = false;

    public void BlinkOnce() {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        if(renderer) {
            _originalMaterial = renderer.material;
           // renderer.material = blinkMaterial;
            StartCoroutine(Blink(renderer, blinkColor, true));
        }
    }

    public IEnumerator Blink(SpriteRenderer renderer, Color color, bool once) {
        //renderer.material = blinkMaterial;
        isFlashing = true;
        if (once) {
            renderer.color = Color.black;
            yield return new WaitForSeconds(flashTime);
        }
        renderer.color = color;
        
        yield return new WaitForSeconds(flashTime);

        renderer.color = _originalColor;
        //renderer.material = _originalMaterial;
        yield return new WaitForSeconds(flashTime);

        isFlashing = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        if (renderer) {
            _originalColor = renderer.color;
            _originalMaterial = renderer.material;  
        }
    }

    // Update is called once per frame
    void Update()
    {
        BaseCharacter character = GetComponent<BaseCharacter>();
        if(character) {
            SpriteRenderer renderer = GetComponent<SpriteRenderer>();
            float iframes = character.IFrames()[0];
            if ((iframes > 0 && !isFlashing) ||
                (invulnerabilityBlink && character.invulnerable)){
                
                if (renderer) {
                    StartCoroutine(Blink(renderer, invincibilityColor, false));
                }
            }
        }
    }
}
