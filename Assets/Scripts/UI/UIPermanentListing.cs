﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIPermanentListing : MonoBehaviour {
    public GameObject character;
    public Transform container;
    public GameObject template;
    public bool canInteract = false;
    public bool listPassives = true;
    public bool queueOnly = false;

    public Text fallbackText;
    
    private CharacterPermanents characterPermanents;
    private CharacterPassives characterLists;

    private List<GameObject> children = new List<GameObject>();
    public UnityEngine.Events.UnityEvent onSelection;

    private void Update() {
        if (Input.GetButtonUp("Submit") && canInteract) {
            if (queueOnly) {
                UIPassiveElement el = null;
                try {
                    el = EventSystem.current.currentSelectedGameObject.GetComponent<UIPassiveElement>();
                   /// SceneManager.LoadScene(Random.Range(0, SceneManager.sceneCount));
                } catch (System.Exception e) {

                }
                if (el != null && el.enabled) {
                    characterLists.RedeemEgo(el.permanentInfo.definition);
                    Time.timeScale = 1;
                    UIPauseMenuController.menuShown = false;
                    onSelection.Invoke();
                }
                
            } else {
                UIPassiveElement el = EventSystem.current.currentSelectedGameObject.GetComponent<UIPassiveElement>();
                if (el) {
                    characterLists.TogglePassive(el.permanentInfo.definition);
                    //Debug.Log(el.permanentInfo.definition);
                }
            }
        }

        if (listPassives && queueOnly) {
            int count = 0;
            foreach(GameObject slot in children) {
                UIPassiveElement passive = slot.GetComponent<UIPassiveElement>();
                bool active = passive.permanentInfo.queued;
                slot.SetActive(active);
                passive.enabled = active;
                if(active) count++;
            }
            if(count <= 0 && fallbackText) fallbackText.gameObject.SetActive(true);
        }
    }
    // Use this for initialization
    void Awake () {
        if (!character)
            character = GameObject.FindGameObjectWithTag("Hero");

        characterLists = character.GetComponent<CharacterPassives>();
        characterPermanents = character.GetComponent<CharacterPermanents>();
       
        int count = listPassives ? characterLists.fullList.Count : characterPermanents.fullList.Count;
        for (int i = 0; i < count; i++) {
            GameObject slot = GameObject.Instantiate(template);
            slot.transform.parent = container;
            slot.transform.localScale = new Vector3(1, 1, 1);

            if (listPassives && queueOnly)
                slot.transform.GetOrAddComponent<UIPassiveElement>().permanentInfo = characterLists.passives[i];// count - i - 1];
            else if(listPassives)
                slot.transform.GetOrAddComponent<UIPassiveElement>().permanentInfo = characterLists.passives[count - i - 1];
            else
                slot.transform.GetOrAddComponent<UIPassiveElement>().permanentInfo = characterPermanents.passives[i];

            children.Add(slot);
            if (listPassives && queueOnly && !characterLists.passives[i].queued) slot.SetActive(false);
        }

        //SelectInitial();
	}

    public void Start() {
        SelectInitial();
    }

    private void OnEnable() {
        SelectInitial();
    }

    void SelectInitial() {
        Log.Text("Select Initial::::");
        int index = 0;
        if (queueOnly) {
            //Debug.Log("CHILD COUNT = " + container.childCount);
            while (!container.GetChild(index).gameObject.activeSelf && index+1 < container.childCount) {
                //Debug.Log("INDEX = " + index);
                index++;
            }
            
        }
        try {
            //EventSystem.current.SetSelectedGameObject(null);
            EventSystem.current.SetSelectedGameObject(container.GetChild(index).gameObject);
            
        } catch(System.Exception e) {
            Debug.LogError(e);
        }
        UIPassiveElement el = container.GetChild(index).gameObject.GetComponent<UIPassiveElement>();
        if (el) el.SelectThis();
    }
	
}
