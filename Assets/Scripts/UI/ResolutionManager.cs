﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionManager : MonoBehaviour
{
    public Dropdown dropdownToPopulate;
    Resolution[] resolutions;

    private void Start() {
        resolutions = Screen.resolutions;
        List<string> resolutionNames = new List<string>();
        for(int i=0; i < resolutions.Length; i++) {
            string name = resolutions[i].width + "x" + resolutions[i].height;
            resolutionNames.Add(name);
        }

        dropdownToPopulate.AddOptions(resolutionNames);
    }

    public void SetResolution(int option) {
        Resolution desiredResolution = resolutions[option];

        Screen.SetResolution(desiredResolution.width, desiredResolution.height, Screen.fullScreenMode);
    }
}
