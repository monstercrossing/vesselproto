﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class TypeWrite : MonoBehaviour
{
    // Adjust text speed.
    [Header("Text Speed Adjustment")]
    public float textSpeed = 0.1f;
    public bool autoStart = true;
    public GameObject originalText;
    public GameObject renderText;
    public UnityEvent OnDismiss;
    public UnityEvent OnFinish;

    // Text Gen Manager
    private int index = -1;
    private bool finished = true;
    private bool rendered = false;
    private bool dismissed = false;

    private AudioSource audio;
    private float audioPitch = 1f;

    private void Start()
    {
        rendered = false;
        audio = GetComponent<AudioSource>();
        audioPitch = audio.pitch;
    }

    // Text speed adjust
    IEnumerator AutoRepeat()
    {
        finished = false;
        index = -1;

        float thisSpeed = textSpeed;

        while (!finished)
        {
            index++;
            char[] characters = originalText.GetComponent<Text>().text.ToCharArray();

            string next = characters[index].ToString();
            if (next.Trim() == ".")
            {
                thisSpeed *= 5;
            }
            else
            {
                if (next.Trim() == "" && index > 0)
                {
                    if (characters[index - 1].ToString().Trim() == "")
                    {
                        next = "\n\n";
                        thisSpeed *= 10;
                    }
                    else
                    {
                        thisSpeed = textSpeed;
                    }
                }
                else
                {
                    thisSpeed = textSpeed;
                }
            }

            renderText.GetComponent<Text>().text = renderText.GetComponent<Text>().text + next;

            if (index % 2 == 0)
            {
                audio.pitch = audioPitch - Random.value / 10;
                audio.Play();
            }

            // Checking for when text is finished.
            if (index >= (characters.Length - 1))
            {
                finished = true;
                OnFinish.Invoke();
            }

            yield return new WaitForSeconds(thisSpeed);
        }
    }

    private IEnumerator DoDisplay(float time)
    {
        yield return new WaitForSeconds(time);

        renderText.GetComponent<Text>().text = "";

        StartCoroutine("AutoRepeat");
    }

    public void Display(float time)
    {
        dismissed = false;
        gameObject.SetActive(true);
        if (rendered && index < 0)
        {
            StartCoroutine("DoDisplay", time);
        }
    }

    private IEnumerator DoDismiss(float time)
    {
        yield return new WaitForSeconds(time);

        OnDismiss.Invoke();
        dismissed = true;
        gameObject.SetActive(false);
    }

    public void Dismiss(float time)
    {
        StartCoroutine("DoDismiss",time);
    }

    private void Update()
    {
        if (!dismissed)
        {
            try
            {
                if (!originalText.GetComponent<LocalizedTextRenderer>().GetRendered())
                {
                    rendered = false;
                }
                else
                {
                    rendered = true;

                    if (autoStart)
                    {
                        Display(0f);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }
}