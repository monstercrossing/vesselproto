﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPauseMenuController : MonoBehaviour {
    public GameObject menuObject;
    public static bool menuShown = false;
    private float originalTimeScale = 1;
    public bool blockCancel = false;
    public GameEvent OnClose;
    public GameEvent OnOpen;

    private bool releasedInputOnce = false;
    private static float lastOnState;
    public static bool anyMenuOpen = false;

    public void Awake() {
        lastOnState = Time.realtimeSinceStartup;
        anyMenuOpen = false;
    }

    public void UnlockCancel() { blockCancel = false; }

    private void Update() { 
        if (Input.GetButtonDown("Cancel") && menuObject && menuObject.activeSelf && !blockCancel) {
            HideMenu();
            if (OnClose) OnClose.Raise();
        } else {
            releasedInputOnce = true;
        }
    }

    public void ToggleMenu() {
        if(menuShown) {
            HideMenu();
        } else {
            ShowMenu();
        }
    }

	public void ShowMenu() {
        if(menuObject.activeSelf || anyMenuOpen) return;
        if(Time.realtimeSinceStartup - lastOnState < 1.2f) return;
        releasedInputOnce = false;
        if (OnOpen) OnOpen.Raise();
        menuShown = true;
        menuObject.SetActive(true);

        if(Time.timeScale > 0)
            originalTimeScale = Time.timeScale;
        
        Time.timeScale = 0;
        lastOnState = Time.realtimeSinceStartup;
        anyMenuOpen = true;
    }

    public void HideMenu() {
        if (!menuObject.activeSelf) return;
        //if(Time.realtimeSinceStartup - lastOnState < 0.7f) return;
        anyMenuOpen = false;
        menuShown = false;
        menuObject.SetActive(false);
        if (OnClose) OnClose.Raise();
        Time.timeScale = originalTimeScale;
        releasedInputOnce = true;

        //lastOnState = Time.realtimeSinceStartup;
    }
}
