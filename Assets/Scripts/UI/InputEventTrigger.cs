﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InputEventTuple {
    public string input;
    public GameEvent customEvent;
}

public class InputEventTrigger : MonoBehaviour {
    public List<InputEventTuple> inputs;
    
    // Update is called once per frame
    public void Update() {
        foreach(InputEventTuple tuple in inputs) {
            if (tuple.customEvent) {
                bool keydown = Input.GetButtonDown(tuple.input);
                if (keydown) tuple.customEvent.Raise();
            }
        }
    }

}

