﻿using UnityEngine;
using System.Collections;

public class ScreenShaker : MonoBehaviour
{
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;
	
	// How long the object should shake for.
	public float shakeDuration = 0f;
	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	private float _shakeTimer = 0f;
    private float originalZ;
	Vector3 originalPos;
	
	void Awake()
	{
		if (camTransform == null)
		{
			camTransform = GetComponent(typeof(Transform)) as Transform;
		}
	}
	
	void OnEnable()
	{
		originalPos = camTransform.position;
        originalZ = camTransform.position.z;
	}

	public void Shake(float magnitude){
		shakeAmount = magnitude;
		originalPos = camTransform.position;
		_shakeTimer = shakeDuration;
	}

	void Update() {
        originalPos = camTransform.position;
        if (_shakeTimer > 0) {
			camTransform.position = originalPos + Random.insideUnitSphere * shakeAmount;
            transform.position = new Vector3(camTransform.localPosition.x, camTransform.localPosition.y, originalZ);
			
			_shakeTimer -= Time.deltaTime * decreaseFactor;
		}
		else {
			_shakeTimer = 0f;
			camTransform.position = originalPos;
            transform.position = new Vector3(camTransform.localPosition.x, camTransform.localPosition.y, originalZ);
        }

	}
}