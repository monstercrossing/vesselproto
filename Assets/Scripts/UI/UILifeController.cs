﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILifeController : MonoBehaviour
{
    public BasePlayer character;
    public GameObject template;
    public GameObject container;

    public Sprite[] slotImages;
    public Sprite[] pipImages;
    public Sprite[] bgImages;

    private List<GameObject> _pips;
    private int _lives, _hpPerLife, _totalPips;

    [SerializeField]
    private int _currentHP, _currentLives;

    private void Start() {
        SetupBar();
    }

    public void SetupBar(){
         if(!character)
            character = GameObject.FindGameObjectWithTag("Hero").GetComponent<BasePlayer>();

        _lives = character.Absorptions()[1] + 1;
        _hpPerLife = character.Hitpoints()[1];

        _totalPips = _lives * _hpPerLife;
        _currentHP = _hpPerLife;
        _currentLives = _lives;

        _pips = new List<GameObject>();
        foreach(Transform child in transform) {
            Destroy(child.gameObject);
        }

        for(int i = 0; i < _lives; i++) {
            for(int l = 0; l < _hpPerLife; l++) {
                GameObject newPip = Instantiate(template);
                newPip.transform.parent = gameObject.transform;
                newPip.transform.localScale = new Vector3(1, 1, 1);

                newPip.AddComponent<UILifePip>().controller = this;
                _pips.Add(newPip);
            }

            if (i+1 < _lives) {
                GameObject separator = new GameObject();
                separator.transform.parent = transform;
                separator.transform.localScale = new Vector3(0.8f, 1, 1);
                separator.AddComponent<Image>().enabled = false; }
        }
    }

    public void Update() {
        _currentHP = character.Hitpoints()[0];
        _currentLives = character.Absorptions()[0] + 1;

        container.GetComponent<Image>().sprite = CurrentBackgroundImage();

        int currentPips = ((_currentLives - 1) * _hpPerLife) + _currentHP;
        for(int p = 0; p < _totalPips; p++) {
            if (p < currentPips) _pips[p].GetComponent<UILifePip>().alight = true;
            else _pips[p].GetComponent<UILifePip>().alight = false;
        }

    }

    public Sprite CurrentSlotImage() {
        int index = 0;
        if (_currentLives < _lives && _currentLives > 0) index = 1;
        if (_currentLives <= 1) index = 2;
        return slotImages[index];
    }

    public Sprite CurrentPipImage() {
        int index = 0;
        if (_currentLives < _lives && _currentLives > 0) index = 1;
        if (_currentLives <= 1) index = 2;
        return pipImages[index];
    }

    public Sprite CurrentBackgroundImage() {
        int index = 0;
        if (_currentLives < _lives && _currentLives > 0) index = 1;
        if (_currentLives <= 1) index = 2;
        return bgImages[index];
    }
}
