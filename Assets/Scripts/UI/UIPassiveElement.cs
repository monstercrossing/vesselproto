﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UIPassiveElement : MonoBehaviour, ISelectHandler, IDeselectHandler {
    public GameEvent OnUIPassiveSelected;
    public LocalPermanentInfo permanentInfo;
    public Image icon;
    public Text equippedLabel;
    public GameObject Highlight;

	// Update is called once per frame

    private void OnGUI() {
        icon.enabled = permanentInfo.discovered;
        icon.sprite = permanentInfo.definition.icon;

        if (permanentInfo.collected && !permanentInfo.discovered) permanentInfo.discovered = true;

        if(equippedLabel)
        equippedLabel.enabled = permanentInfo.equiped;

        float alpha = permanentInfo.collected ? 1 : 0.4f;
        Color color = new Color(1, 1, 1, alpha);
        icon.color = color;
    }

    public void OnSelect(BaseEventData eventData) {
        Log.Text("On Permanent Select");

            if (OnUIPassiveSelected) OnUIPassiveSelected.Raise(this);
        if (Highlight) Highlight.SetActive(true);
    }
    public void OnDeselect(BaseEventData eventData) {
        if (Highlight) Highlight.SetActive(false);
    }

    public void SelectThis() {
        if (OnUIPassiveSelected) OnUIPassiveSelected.Raise(this);
        if (Highlight) Highlight.SetActive(true);
    }
}
