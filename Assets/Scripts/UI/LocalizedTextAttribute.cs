﻿using UnityEngine;

public class LocalizedTextAttribute : PropertyAttribute {
    public bool UseDefaultTagFieldDrawer = false;
}
