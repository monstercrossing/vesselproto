﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UISaveSlot : MonoBehaviour {
    public GameObject emptyObject;
    public GameObject contentsObject;
    public GameObject deletionBar;
    public int slot;

    [HideInInspector]
    public int deathCount = 0;
    [HideInInspector]
    public int collectedEgos = 0;
    [HideInInspector]
    public int totalEgos = 0;
    [HideInInspector]
    public int collectedPermanents = 0;
    [HideInInspector]
    public int totalPermanents = 0;

    public Image barFill;
    [HideInInspector]
    public int completion = 5;

    private float deleteTimer = 0;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        float barscale;
        if (SaveSlotController.savedGames != null && EventSystem.current.currentSelectedGameObject == gameObject.transform.Find("Slot").gameObject ) {
            SaveSlot saveFile = SaveSlotController.savedGames[slot];
            if (saveFile != null && !saveFile.empty && deletionBar) {
                if(Input.GetButton("Shoot") ) {
                    deleteTimer += Time.deltaTime;
                    barscale = Mathf.Clamp01(deleteTimer);
                    deletionBar.transform.localScale = new Vector3(barscale, 1, 1);
                    if (deleteTimer >= 1) {
                        saveFile.Wipe();
                        SaveSlotController.SaveSlots();
                        UpdateSlotInfo();
                    }
                    return;
                }
            }
        }
        deleteTimer = 0;
        barscale = Mathf.Clamp01(deleteTimer);
        deletionBar.transform.localScale = new Vector3(barscale, 1, 1);
    }

    public void UpdateSlotInfo(){
        if(SaveSlotController.savedGames != null) {
            SaveSlot saveFile = SaveSlotController.savedGames[slot];
            if(saveFile != null && !saveFile.empty) {
                deathCount = saveFile.deathCount;

                totalEgos = saveFile.egos.Count;
                totalPermanents = saveFile.permanents.Count;
                collectedEgos = 0;
                collectedPermanents = 0;

                for(int i=0; i < totalEgos; i++) {
                    if(saveFile.egos[i].collected) collectedEgos++;
                }

                for(int i=0; i < totalPermanents; i++) {
                    if(saveFile.permanents[i].collected) collectedPermanents++;
                }

                emptyObject.SetActive(false);
                contentsObject.SetActive(true);

                int collectiblesTotal = totalEgos+totalPermanents+saveFile.defeatedBosses.Length; //4 bosses
                int collectiblesGathered = collectedEgos + collectedPermanents;
                for(int i = 0; i < saveFile.defeatedBosses.Length; i++) {
                    if(saveFile.defeatedBosses[i]) collectiblesGathered++;
                }

                float completionFloat = ((float)collectiblesGathered / (float)collectiblesTotal);
                completion = Mathf.FloorToInt(completionFloat*100);
                if(barFill) barFill.fillAmount = (float)completionFloat;
            } else {
                emptyObject.SetActive(true);
                contentsObject.SetActive(false);
            }
        }
    }


}
