﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using MonsterCrossing.System.UI;
using System.Reflection;

[System.Serializable]
public class TextParam {
    public MonoBehaviour type;
    public String parameter;
}

public class LocalizedTextRenderer : MonoBehaviour {
    public LocalizationTemplate template;
    public StringTable language;
    public Text textObject;
    public bool randomizeOnStart = false;

    [LocalizedText]
    public int stringIndex = 0;

    public TextParam[] formatParams;

    private string lang;
    private bool rendered = false;

    private void Start() {
        if( !PlayerPrefs.HasKey("longway.lang")) PlayerPrefs.SetString("longway.lang","pt_BR");
        if(randomizeOnStart) {
            stringIndex = UnityEngine.Random.Range(0, language.translatedTexts.Length - 1);
        }
        /*
        if(textObject && language)
        textObject.text = language.translatedTexts[stringIndex];
        */
        textObject.text = "";
        UpdateLanguage();
    }

    private void UpdateLanguage() {

        rendered = false;

        if (!template || template.translations == null) return;
        lang = PlayerPrefs.GetString("longway.lang");
        for (int i = 0; i < template.translations.Length; i++) {
            if (template.translations[i].key == lang) {
                language = template.translations[i].table;
                break;
            }
        }

        UpdateText();
    }

    public bool GetRendered()
    {
        return rendered;
    }

    private void Update() {
        if (textObject && language) {

            if (lang != PlayerPrefs.GetString("longway.lang")) UpdateLanguage();

        }
    }

    public void UpdateText()
    {
        if (formatParams.Length > 0)
        {
            List<String> strings = new List<string>();
            for (int i = 0; i < formatParams.Length; i++)
            {
                TextParam current = formatParams[i];
                FieldInfo field = current.type.GetType().GetField(current.parameter, BindingFlags.Instance | BindingFlags.Public);
                try{
                strings.Add(field.GetValue(current.type).ToString());
                }catch(Exception e) {
                    Debug.LogWarning(e);
                }
                //Debug.Log("Field name: " + current.parameter + " field:" + field.GetValue(current.type));
            }

            String unformatted = language.translatedTexts[stringIndex];
            String formatted = string.Format(unformatted, strings.ToArray());
            textObject.text = formatted;
        }
        else
        {
            textObject.text = language.translatedTexts[stringIndex];
        }

        rendered = true;
    }

    public void ChangeSourceStrings(LocalizationTemplate newTemplate, StringTable newTable, int newIndex) {
        template = newTemplate;
        language = newTable;
        stringIndex = newIndex;
    }

    public void ParsePassiveName(MonoBehaviour callerObject) {
        UIPassiveElement passive = callerObject as UIPassiveElement;
        Debug.ClearDeveloperConsole();
        //Debug.Log(passive);
        
        try {
            PermanentDefinition definition = passive.permanentInfo.definition;
            if(passive.permanentInfo.discovered)
                ChangeSourceStrings(definition.template, definition.language, definition.ingameName);
            else
                ChangeSourceStrings(definition.template, definition.language, definition.fallbackName);

        } catch(Exception e) {
            Debug.LogWarning(e);
        }
        UpdateLanguage();
        UpdateText();
    }
    public void ParsePassiveDescription(MonoBehaviour callerObject) {
        UIPassiveElement passive = callerObject as UIPassiveElement;
        Debug.ClearDeveloperConsole();
        Debug.Log(passive);

        try {
            PermanentDefinition definition = passive.permanentInfo.definition;
            if (passive.permanentInfo.discovered)
                ChangeSourceStrings(definition.template, definition.language, definition.ingameDescription);
            else
                ChangeSourceStrings(definition.template, definition.language, definition.fallbackDescription);

        } catch (Exception e) {
            Debug.LogWarning(e);
        }
        UpdateLanguage();
        UpdateText();
    }
}
