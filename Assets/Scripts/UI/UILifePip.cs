﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILifePip : MonoBehaviour
{
    public UILifeController controller;
    public bool alight = false;
    // Update is called once per frame
    void Update()
    {
        if(alight)
            GetComponent<UnityEngine.UI.Image>().sprite = controller.CurrentPipImage();
        else
            GetComponent<UnityEngine.UI.Image>().sprite = controller.CurrentSlotImage();
    }
}
