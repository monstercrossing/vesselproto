﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Translation {
    public string key;
    public MonsterCrossing.System.UI.StringTable table;
}

namespace MonsterCrossing.System.UI {
    [CreateAssetMenu(menuName = "MonsterCrossing/Localization/Template")]
    public class LocalizationTemplate : ScriptableObject {
        public string[] stringKeys;

        public Translation[] translations;
    }
}