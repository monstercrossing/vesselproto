﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MonsterCrossing.System.UI {

    [CreateAssetMenu(menuName ="MonsterCrossing/Localization/Language")]
    public class StringTable : ScriptableObject {
        public string language;
        public LocalizationTemplate template;

        public string[] translatedTexts;
    }
}