﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBalloonFeedback : MonoBehaviour {

    public Transform feedbackBalloon;
    public GameObject target;

	// Use this for initialization
	void Start ()
    {
        if (!target)
            target = GameObject.FindGameObjectWithTag("Hero");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowFeedback()
    {
        Transform clone = Instantiate(feedbackBalloon, target.transform);
        clone.position = new Vector3(clone.position.x + 0f, clone.position.y + 2.25f, 0f);

        StartCoroutine(DismissFeedback(clone));
    }

    private IEnumerator DismissFeedback(Transform feedbackBalloon)
    {
        yield return new WaitForSeconds(2f);
        Destroy(feedbackBalloon.gameObject);
    }

    public void HideFeedback()
    {
        Destroy(gameObject);
    }
}
