﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;
using Panda;

public class DefaultEnemyTasks : MonoBehaviour {
    public FloatVariable viewRangeConstant;
    public FloatVariable shootRangeConstant;
    public FloatVariable patrolRangeConstant;
    public FloatVariable hearingRangeConstant;

    public float viewRange;
    public float shootRange;
    public float patrolRange;
    public float hearingRange;

    public GameEvent OnAttackAlert;

    [HideInInspector]
    public GameObject target;
    public float chanceToShoot;

    protected Animator _animator;
    protected float _cooldown = 0;
    protected FlipSprite _flipComponent;
    protected float _moveSpeed = 0f;
    protected Rigidbody2D _body;
    protected bool _targetSpotted = false;
    protected Vector3 _patrolStart;
    protected Vector3 _patrolEnd;
    protected bool _patrolDirectionForward = true;

    public float _maxAttackStamina = 3.0f;
    protected float _currentAttackStamina;
    protected bool _tired = false;

    protected float _waitTimer = 0;
    protected bool _isWaiting = false;

    public bool tired {
        get {
            return _tired;
        }
    }

    public float cooldown { get { return _cooldown;  } }
    public Animator animator {
        get {
            return _animator;
        }
    }

    // Use this for initialization
    void Start () {
        if (viewRangeConstant) viewRange = viewRangeConstant.Value;
        if (shootRangeConstant) shootRange = shootRangeConstant.Value;
        if (patrolRangeConstant) patrolRange = patrolRangeConstant.Value;

        if (!target)
            target = GameObject.FindGameObjectWithTag("Hero");

        _animator = GetComponent<Animator>();
        _flipComponent = GetComponent<FlipSprite>();
        /*
        if (_animator) {
            _cooldown = _animator.GetFloat("FireRate");
        }
        */
        _body = GetComponent<Rigidbody2D>();

        _patrolStart = transform.position + new Vector3(0, 0.75f, 0);
        _patrolEnd = transform.position + new Vector3(patrolRange, 0.75f, 0);
        //Debug.Log(_patrolEnd);

        BaseCharacter character = GetComponent<BaseCharacter>();
        if (character)
            character.onHitEvent.AddListener(SpotTargetOnHit);

        _currentAttackStamina = _maxAttackStamina;
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Vector3 start = transform.position + new Vector3(0, 0.75f, 0);
        Vector3 direction = transform.right;
        float distance = patrolRange;

        Debug.DrawRay(start, direction * distance, Color.yellow);
    }

    // Update is called once per frame
    void Update () {
        if (_currentAttackStamina < _maxAttackStamina) {
            _currentAttackStamina += Time.deltaTime;
            if(_currentAttackStamina >= _maxAttackStamina) {
                _currentAttackStamina = _maxAttackStamina;
                _tired = false;
            }
        }

        if (_cooldown > 0) {
            _cooldown-= Time.deltaTime;

            if(_cooldown <= 0) {
                _animator.ResetTrigger("Shoot");
            }
        }
        else {
            if (_animator.GetBool("Attacking")) {
                _animator.SetBool("Attacking", false);
                return;
            }
            //_cooldown = _animator.GetFloat("FireRate");
            
        }

        if(_waitTimer > 0) {
            _waitTimer -= Time.deltaTime;
            _isWaiting = true;
        }
        if(_waitTimer < 0) {
            _isWaiting = false;
        }
    }

    void SpotTargetOnHit() {
        _targetSpotted = true;
    }

    public void SpendStamina(float ammount) {
        _currentAttackStamina -= ammount;
        _cooldown = _animator.GetFloat("FireRate");
            if(_currentAttackStamina < 0) {
                _tired = true;
            }
    }

    public bool IsTargetInLine(float distance) {
        Vector3 start = transform.position + new Vector3(0, 1.0f, 0);
        Vector3 direction = (target.transform.position - transform.position).normalized;

        Debug.DrawRay(start, direction * distance, Color.cyan);
        RaycastHit2D sightTest = Physics2D.Raycast(start, direction, distance, target.layer << 8);
        if (sightTest.collider != null) {
            //Debug.Log("Rigidbody collider is: " + sightTest.collider);
            if (sightTest.collider.gameObject == target.gameObject ||
                sightTest.collider.transform.parent == target.transform) {
                //Debug.Log("Rigidbody collider is: " + sightTest.collider);
                return true;
            }
        }

        return false; 
    }

    protected bool HaveHoles() {
        int dir = _flipComponent.facingRight ? 1 : -1;
        float dist = 2.5f;
        float longdist = 4.50f;
        Vector3 start = transform.position + new Vector3(0, 1.5f, 0);
        Vector3 end = transform.position + new Vector3(0.5f * dir, 0, 0);
        Vector3 far = transform.position + new Vector3(1.0f * dir, 0, 0);
        Vector3 direction = (end - start).normalized;
        Vector3 longDirection = (far - start).normalized;
        Debug.DrawRay(start, direction * dist, Color.magenta);
        Debug.DrawRay(start, longDirection * longdist, Color.blue);
        RaycastHit2D sightTest = Physics2D.Raycast(start, direction, dist, LayerMask.GetMask("Terrain"));
        RaycastHit2D farSightTest = Physics2D.Raycast(start, longDirection, longdist, LayerMask.GetMask("Terrain"));
        if(farSightTest.collider != null && farSightTest.collider.tag == "Traps") return true;
        if (sightTest.collider != null) {
            //Debug.Log("Rigidbody collider is: " + sightTest.collider);
           
            return false;
        }

        if (farSightTest.collider == null) {
            
            //Debug.Log("Rigidbody collider is: " + sightTest.collider);

            return true;
        }

        return false;
    }

    public bool HasObstacleInFront() {
        int dir = _flipComponent.facingRight ? 1 : -1;
        float dist = 1.0f;
        Vector3 start = transform.position + new Vector3(0, 0.5f, 0);
        Vector3 front = transform.position + new Vector3(dist * dir, 0.5f, 0);
        Vector3 direction = (front - start).normalized;
        Debug.DrawRay(start, direction * dist, Color.green);

        RaycastHit2D sightTest = Physics2D.Raycast(start, direction, dist, LayerMask.GetMask("Terrain"));
        if (sightTest.collider != null) {
            //Debug.Log("Rigidbody collider is: " + sightTest.collider);

            return true;
        }

        return false;
    }

    protected bool IsJumpableObstacle() {
        int dir = _flipComponent.facingRight ? 1 : -1;
        float dist = 1.5f;
        Vector3 start = transform.position + new Vector3(0, 1.5f, 0);
        Vector3 end = transform.position + new Vector3(0.5f * dir, 1.5f, 0);
        Vector3 direction = (end - start).normalized;

        Debug.DrawRay(start, direction * dist, Color.red);
        RaycastHit2D sightTest = Physics2D.Raycast(start, direction, dist, LayerMask.GetMask("Terrain"));
        if (sightTest.collider != null) {
            //Debug.Log("Rigidbody collider is: " + sightTest.collider);

            return false;
        }
        return true;
    }

    protected Vector3 GetCurrentTarget() {
        Vector3 currentTarget = Vector3.zero;
        
        if (_patrolDirectionForward) {
            currentTarget = _patrolEnd;
        } else {
            currentTarget = _patrolStart;
        }

        return currentTarget;
    }

    protected void LookAt(Vector3 position) {
        bool atRight = position.x > transform.position.x;
        if (atRight && !_flipComponent.facingRight) {
            _flipComponent.Flip();
        }
        else if (!atRight && _flipComponent.facingRight) {
            _flipComponent.Flip();
        }
    }

    public void MoveTowardPosition(Vector3 position) {
        _moveSpeed = _animator.GetFloat("MoveSpeed");
        float direction = position.x > transform.position.x ? 1 : -1;

        if (_body && _animator.GetBool("CanMove")) {
            _body.velocity = new Vector2(direction * _moveSpeed, _body.velocity.y);
            _animator.SetFloat("VerticalVelocity", _body.velocity.y);
            _animator.SetFloat("HorizontalVelocity", _body.velocity.x);
            _animator.SetFloat("AbsoluteVelocity", Mathf.Abs(_body.velocity.x));
        }
    }

    public void SpotEnemy() {
        _targetSpotted = true;
    }

    public void ResetStamina() {
        _currentAttackStamina = _maxAttackStamina;
        _tired = false;
    }

    [Task]
    bool Jump() {
        int jumpCount = _animator.GetInteger("JumpCount");
        if (_body && jumpCount < 1) {
            _animator.SetInteger("JumpCount", jumpCount + 1);
            _animator.SetTrigger("Jump");
        }
        
        return true;
    }

    [Task]
    bool EnemyInRange() {
        //Debug.Log(target);
        if (!target) return false;
        return IsTargetInLine(shootRange);
    }

    [Task]
    bool EnemyInSight() {
        if (!target) return false;

        return IsTargetInLine(viewRange);
    }

    [Task]
    bool EnemyInVicinity() {
        if (!target) return false;

        return IsTargetInLine(hearingRange);
    }

    [Task]
    bool AvoidHoles() {
        return !HaveHoles();
    }

    [Task]
    bool AvoidObstacle() {
        return !HasObstacleInFront();
    }

    [Task]
    bool IsJumpable() {
        return IsJumpableObstacle();
    }

    [Task]
    bool ChangePatrolDirection() {
        _patrolDirectionForward = !_patrolDirectionForward;
        return true;
    }

    [Task]
    bool HasStaminaToAttack() {
        return Mathf.Approximately(_maxAttackStamina, _currentAttackStamina);
    }

    [Task]
    bool DecideAttackChance() {
        bool chance = (Random.value <= chanceToShoot && _cooldown <= 0 && !_tired);

        if ( chance) {
            return true;
        }
        return false;
    }

    [Task]
    bool ShootWithoutAsking() {
        SpendStamina(1);
        //BaseCharacter character = GetComponent<BaseCharacter>();
        //if (character) character.Attack();
        _animator.SetTrigger("Shoot");
        _cooldown = _animator.GetFloat("FireRate");
        return true;
    }

    [Task]
    bool ShootEnemy() {
       // Log.Text(_maxAttackStamina, _currentAttackStamina);
       
        bool chance = (Random.value <= chanceToShoot && _cooldown <= 0 && !_tired);

       if (Mathf.Approximately(_maxAttackStamina, _currentAttackStamina) || chance) {
            SpendStamina(1);
            //BaseCharacter character = GetComponent<BaseCharacter>();
            //if (character) character.Attack();
            _animator.SetTrigger("Shoot");
            _cooldown = _animator.GetFloat("FireRate");
        //Debug.Break();
        return true;
        }
        return false;
    }

    [Task]
    bool MoveTowardEnemy() {
        if (target) {
            MoveTowardPosition(target.transform.position);
            return true;
        }

        return false;
    }

    [Task]
    bool SetTargetPosition() {
        Vector3 currentTarget = GetCurrentTarget();
        
        if( Mathf.Abs(transform.position.x - currentTarget.x) < 0.5f ) {
            _patrolDirectionForward = !_patrolDirectionForward;
            return false;
        }
        
        return true;
    }

    [Task]
    bool StopMoving() {
        if(!_animator.GetBool("KnockedBack"))
         _body.velocity = new Vector2(0, _body.velocity.y);
        _animator.SetFloat("VerticalVelocity", _body.velocity.y);
        _animator.SetFloat("HorizontalVelocity", _body.velocity.x);
        _animator.SetFloat("AbsoluteVelocity", Mathf.Abs(_body.velocity.x));
        return true;
    }

    [Task]
    bool LookAtTarget() {
        if (target) {
            LookAt(target.transform.position);
            return true;
        }
        return false;
    }

    [Task]
    bool IsLookingAtTarget() {
        if (target) {
            bool atRight = target.transform.position.x > transform.position.x;
            if (atRight && _flipComponent.facingRight) {
                return true;
            }
            else if (!atRight && !_flipComponent.facingRight) {
                return true;
            }
        }
        return false;
    }

    [Task]
    bool LookAtTargetPosition() {
        Vector3 currentTarget = GetCurrentTarget();
        LookAt(currentTarget);

        return true;
    }

    [Task]
    public bool MoveTowardTargetPosition() {
        Vector3 currentTarget = GetCurrentTarget();

        MoveTowardPosition(currentTarget);
        return true;
    }

    [Task]
    bool SpotTarget() {
        _targetSpotted = true;
        return true;
    }

    [Task]
    bool LoseTarget() {
        _targetSpotted = false;
        return true;
    }

    [Task]
    bool IsTargetSpotted() {
        return _targetSpotted;
    }

    [Task]
    bool WaitForTime(float time) {
        _waitTimer = time;
        return true;
    }

    [Task]
    bool IsWaiting() {
        return _isWaiting;
    }

    [Task]
    public bool IsTired() {
        return _tired || _cooldown > 0;
    }

    [Task]
    public bool AlertAttack() {
        if (!Mathf.Approximately(_maxAttackStamina, _currentAttackStamina)) return true;
        if (OnAttackAlert) OnAttackAlert.Raise();

        return true;
    }
}
