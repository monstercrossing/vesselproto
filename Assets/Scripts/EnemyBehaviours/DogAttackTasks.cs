﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;
using Panda;

[RequireComponent(typeof(DefaultEnemyTasks))]
public class DogAttackTasks : MonoBehaviour {
    public float jumpAttackRange;
    public float preparationTime;
    public float chanceToJumpAttack;
    public float stamina;

    private Animator _animator;
    private DefaultEnemyTasks parentTasks;
    private bool isWaitingToAttack = false;
    private float _prepareTimer;
    private bool _rechargingStamina = false;
    private bool _timedout = false;
    [SerializeField]
    private float _staminaMeter;

    // Use this for initialization
    void Start () {
        _prepareTimer = preparationTime;
        _animator = GetComponent<Animator>();
        parentTasks = GetComponent<DefaultEnemyTasks>();

        _staminaMeter = stamina;
    }

    private void Update() {
        if(isWaitingToAttack) {
            _prepareTimer -= Time.deltaTime;
            if(_prepareTimer <= 0) {
                //_prepareTimer = preparationTime;
                _timedout = true;
                //isWaitingToAttack = false;
            }
        }

        if (_rechargingStamina) _staminaMeter += Time.deltaTime * 7;
        if(_staminaMeter > stamina) {
            _staminaMeter = stamina;
            _rechargingStamina = false;
        }
    }

    [Task]
    bool SpendStamina() {
        if (_rechargingStamina) return false;
        _staminaMeter -= Time.deltaTime * 10;

        if (_staminaMeter <= 0) {
            _rechargingStamina = true;
            return false;
        }
        else return true;
    }

    [Task]
    bool WaitingForAttack() {
        return isWaitingToAttack || _animator.GetBool("WaitingForAttack");
    }

    [Task]
    bool WaitTimedOut() {
        return _timedout;
    }

    [Task]
    bool JumpAttack() {
        BaseCharacter character = GetComponent<BaseCharacter>();
        if (character) character.Attack();

        isWaitingToAttack = false;
        _animator.SetBool("IsJumpAttack", true);
        _animator.SetTrigger("Attack");
        return false;
    }

    [Task]
    bool PrepareForBite() {
        if (Random.value <= chanceToJumpAttack && parentTasks.cooldown <= 0) {
            _prepareTimer = preparationTime;
            _timedout = false;
            isWaitingToAttack = true;

            _animator.SetTrigger("PrepareForBite");
            return true;
        }
        return false;
    }

    [Task]
    bool ReturnToIdle() {
        isWaitingToAttack = false;
        _animator.SetBool("WaitingForAttack", false);
        return false;
    }

    [Task]
    bool EnemyInJumpRange() {
         if (parentTasks && !parentTasks.target) return false;

        return parentTasks.IsTargetInLine(jumpAttackRange);
    }
}
