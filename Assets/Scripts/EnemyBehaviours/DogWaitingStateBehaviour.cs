﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogWaitingStateBehaviour : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("CanMove", false);
        animator.SetBool("WaitingForAttack", true);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("CanMove", true);
        animator.SetBool("WaitingForAttack", false);
    }
}
