﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCharacter : MonoBehaviour {
    public Transform trackedCharacter;
    public float chanceToMove = 1f;

    private float _moveSpeed = 0f;
    private Rigidbody2D _body;
    private Animator _animator;

    void Start() {
        _animator = GetComponent<Animator>();
        _body = GetComponent<Rigidbody2D>();

        if (!trackedCharacter) {
            trackedCharacter = GameObject.FindWithTag("Hero").transform;
        }
    }
    // Update is called once per frame
    void Update () {
        _moveSpeed = _animator.GetFloat("MoveSpeed");
        if(trackedCharacter && Random.value <= chanceToMove) {
            float delta =  _moveSpeed;
            float direction =  trackedCharacter.position.x > transform.position.x ? 1 : -1;

            if (_animator.GetBool("CanMove")) {
                _body.velocity = new Vector2(direction * delta, _body.velocity.y);
                _animator.SetFloat("VerticalVelocity", _body.velocity.y);
                _animator.SetFloat("HorizontalVelocity", _body.velocity.x);
                _animator.SetFloat("AbsoluteVelocity", Mathf.Abs(_body.velocity.x));

            }
        }
	}
}
