﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;
using Panda;

[RequireComponent(typeof(DefaultEnemyTasks))]
public class FullDemonTasks : MonoBehaviour {
    public PowerupDefinition stompPowerup;
    public PowerupDefinition punchPowerup;
    public PowerupDefinition fireballPowerup;
    public PowerupDefinition pillarPowerup;

    public GameObject halfDemonPillar;
    public GameEvent OnDashStart;

    [Range(0,1)]
    public float chanceToStomp = 1;
    [Range(0, 1)]
    public float chanceToShootFireball = 1;
    [Range(0, 1)]
    public float chanceToAirDash = 1;
    [Range(0, 1)]
    public float chanceToSlap = 1;
    [Range(0, 1)]
    public float chanceToWait = 0;

    public float stompHeight = 5;

    private DefaultEnemyTasks parentTasks;
    private BaseCharacter character;
    private bool passedAbovePlayer = false;
    private Vector3 jumpStartPosition;
    private CharacterEventHub eventHub;

    private bool airdashIntent = false;

    void Start() {
        parentTasks = GetComponent<DefaultEnemyTasks>();
        character = GetComponent<BaseCharacter>();
        eventHub = transform.GetOrAddComponent<CharacterEventHub>();
    }

    public bool IsAlignedWith(float threshold) {
        float diff = transform.position.x - parentTasks.target.transform.position.x;
        return Mathf.Abs(diff) <= threshold;
    }

    public float HeightFromFloor() {
        Vector3 start = transform.position;
        Vector3 end = transform.position + new Vector3(0, -1000, 0);
        Vector3 direction = (end - start).normalized;
        Debug.DrawRay(start, direction * 1000, Color.red);

        RaycastHit2D sightTest = Physics2D.Raycast(start, direction, 1000, LayerMask.GetMask("Terrain"));
        if (sightTest.collider != null) {
            //Debug.Log("Rigidbody collider is: " + sightTest.collider);
            float distance = Vector3.Distance(start, sightTest.point);

            return distance;
        }

        return Mathf.Infinity;
    }

    [Task]
    bool IsAirborne() {
        return !parentTasks.animator.GetBool("IsGrounded");//parentTasks.animator.GetBool("Jumping") || parentTasks.animator.GetBool("Falling");
    }

    [Task]
    bool MoveForward() {
            //if (parentTasks.HasObstacleInFront()) return false;
            float direction = parentTasks.animator.GetBool("FacingRight") ? 1 : -1;
            Vector3 safePoint = (transform.position + new Vector3(direction, 0, 0));
            parentTasks.MoveTowardPosition(safePoint);

            //Debug.Log(direction);
            return true;
    }

    [Task]
    bool StompWithChance() {
        bool willStomp = Random.value <= chanceToStomp;
        if (!willStomp) return false;

        if (stompPowerup) character.ReceivePowerup(stompPowerup);
        parentTasks.animator.SetTrigger("Stomp");
        return true;
    }

    [Task]
    bool ChanceToWait() {
        bool willWait = Random.value <= chanceToWait;
        return willWait;
    }


    [Task]
    bool IsStomping() {
        return parentTasks.animator.GetBool("Stomping");
    }

    [Task]
    bool IsShooting() {
        return parentTasks.animator.GetBool("Shooting");
    }

    [Task]
    bool IsAtStompHeight() {
        return HeightFromFloor() >= stompHeight;
    }

    [Task]
    bool ChooseJumpIntent() {
        airdashIntent = Random.value <= chanceToAirDash && !parentTasks.IsTired();
        return true;
    }

    [Task]
    bool IntendsToAirdash() {
        return airdashIntent;
    }

    [Task]
    bool ChanceToAirdash() {
        return Random.value <= chanceToAirDash;
    }

    [Task]
    bool Airdash() {
        if (OnDashStart) OnDashStart.Raise();
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        parentTasks.animator.SetTrigger("Dash");
        return true;
    }

    [Task]
    bool IsDashing() {
        return parentTasks.animator.GetBool("Dashing");
    }

    [Task]
    bool IsPreparing() {
        return parentTasks.animator.GetBool("PreparingAttack");
    }

    [Task]
    bool ShootFireball() {
        bool willShoot = Random.value <= chanceToShootFireball;
        if (!willShoot) return false;

        parentTasks.SpendStamina(1);
        if (fireballPowerup) character.ReceivePowerup(fireballPowerup);
        parentTasks.animator.SetTrigger("Shoot");
        return true;

    }

    [Task]
    bool ShootPillar() {
        bool willShoot = Random.value <= chanceToShootFireball;
        if (!willShoot) return false;

        parentTasks.SpendStamina(1);
        if (pillarPowerup) character.ReceivePowerup(pillarPowerup);
        parentTasks.animator.SetTrigger("Shoot");
        return true;

    }

    [Task]
    bool Slap() {
        bool willSlap = Random.value <= chanceToSlap;
        if (!willSlap) return false;

        parentTasks.SpendStamina(1);
        if (punchPowerup) character.ReceivePowerup(punchPowerup);
        parentTasks.animator.SetTrigger("Shoot");
        return true;
    }

    [Task]
    public bool ShootOnAir() {
        bool willShoot = Random.value <= 1;
        if (!willShoot) return false;

        parentTasks.SpendStamina(1);
        if (fireballPowerup) character.ReceivePowerup(fireballPowerup);
        //Animator animator = parentTasks.animator;
        // if (shootingSpeedMultiplier) _shootSpeed = shootingSpeedMultiplier.Value;

        if(character) {
           character.Attack(true);
        }

        return true;
    }

    [Task]
    public bool IsAligned(float threshold) {
        return IsAlignedWith(threshold);
    }

    [Task]
    public bool SpawnPillar() {
        return SpawnPillar(0);
    }

    [Task]
    public bool SpawnPillar(float offset) {
        if (!halfDemonPillar) return false;
        bool willShoot = Random.value <= parentTasks.chanceToShoot;
        if (!willShoot) return false;

        float direction = parentTasks.animator.GetBool("FacingRight") ? 1 : -1;

        parentTasks.animator.SetTrigger("Cast");
        float spot = parentTasks.target.transform.position.x + (offset * direction);
        Vector3 position = new Vector3(spot, transform.position.y,0);
        GameObject instance = GameObject.Instantiate(halfDemonPillar, position, Quaternion.identity);
        parentTasks.SpendStamina(2);

        return true;
    }

    [Task]
    bool IsBelowLife(float percentage) {
       int life = character.Hitpoints()[0];
        int maxLife = character.Hitpoints()[1];
        Log.Text("LIFE: ", life, maxLife);

        return ((float)life / (float)maxLife) <= percentage;
    }

    [Task]
    bool SetChargeLevel(int level) {
        eventHub.chargeLevel = level;
        return true;
    }
}
