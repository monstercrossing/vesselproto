﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoShoot : MonoBehaviour {
    public float chanceToShoot = 1;
    public float cooldownMultiplier = 1;

    private Animator _animator;
    private float _cooldown = 2;

    // Use this for initialization
    void Start () {
        _animator = GetComponent<Animator>();
        if(_animator) {
            _cooldown = _animator.GetFloat("FireRate") * cooldownMultiplier;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (_cooldown > 0) {
            _cooldown -= Time.deltaTime;
            _animator.ResetTrigger("Shoot");
        }  else {
            if (_animator.GetBool("Attacking")) {
                _animator.SetBool("Attacking", false);
                return;
            }
            _cooldown = _animator.GetFloat("FireRate") * cooldownMultiplier;
            if (Random.value <= chanceToShoot) {
                BaseCharacter character = GetComponent<BaseCharacter>();
                if (character) character.Attack();
                _animator.SetTrigger("Shoot");
            }
        }
	}
}
