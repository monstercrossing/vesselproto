﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FlipSprite))]
public class LookAtCharacter : MonoBehaviour {
    public Transform trackedCharacter;

    private FlipSprite _flipComponent;

    private void Start() {
        _flipComponent = GetComponent<FlipSprite>();
        if(!trackedCharacter) {
            trackedCharacter = GameObject.FindWithTag("Hero").transform;
        }
    }

    public void Update() {

        if (trackedCharacter) {
            bool atRight = trackedCharacter.position.x > transform.position.x;
            if (atRight && !_flipComponent.facingRight) {
                _flipComponent.Flip();
            }
            else if (!atRight && _flipComponent.facingRight) {
                _flipComponent.Flip();
            }
        }
    }
}
