﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDash : MonoBehaviour {
    public float chanceToDash = 1;
    public float cooldown = 1;

    private Animator _animator;
    private float _cooldown = 2;

    // Use this for initialization
    void Start() {
        _animator = GetComponent<Animator>();
        if (_animator) {
            _cooldown = cooldown;
        }
    }

    // Update is called once per frame
    void Update() {
        if (_cooldown > 0) {
            _cooldown -= Time.deltaTime;
            _animator.ResetTrigger("Dash");
        }
        else {
            _cooldown = cooldown;
            if (Random.value <= chanceToDash) {

                _animator.SetTrigger("Dash");
            }
        }
    }
}
