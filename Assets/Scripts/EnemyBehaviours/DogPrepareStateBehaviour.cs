﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogPrepareStateBehaviour : StateMachineBehaviour {

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.ResetTrigger("PrepareToBite");
        animator.SetBool("CanMove", false);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("CanMove", true);
    }
}
