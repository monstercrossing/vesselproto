﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;
using Panda;

[RequireComponent(typeof(DefaultEnemyTasks))]
public class RunawayTasks : MonoBehaviour {
    public float fearRange;

    private DefaultEnemyTasks parentTasks;
    private bool couldntEscape = false;

    private void Start() {
        parentTasks = GetComponent<DefaultEnemyTasks>();
    }
    // Use this for initialization
    [Task]
    bool EnemyInFearRange() {
        if (!parentTasks.target) return false;

        return parentTasks.IsTargetInLine(fearRange);
    }

    [Task]
    bool MoveAwayFromEnemy() {
        if (couldntEscape) return false;
        if (parentTasks.target) {
            //if (parentTasks.HasObstacleInFront()) return false;
            float direction = transform.position.x > parentTasks.target.transform.position.x ? 1 : -1;
            Vector3 safePoint = (transform.position + new Vector3(direction,0,0));
            parentTasks.MoveTowardPosition(safePoint);

            //Debug.Log(direction);
            return true;
        }

        return false;
    }

    [Task]
    bool LockTarget() {
        couldntEscape = true;
        return true;
    }

    [Task]
    bool UnlockTarget() {
        couldntEscape = false;
        return true;
    }

    [Task]
    bool IgnoreLocked() {
        return !couldntEscape;
    }


}
