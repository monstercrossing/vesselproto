﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;
using Panda;

[RequireComponent(typeof(DefaultEnemyTasks))]
public class CopyMachineTasks : MonoBehaviour
{
    public float preparationTime;
    public float stamina;
    public bool isActive;
    public SpawnPoint spawner;
    public RuntimeAnimatorController lowHealthAnimator;

    private Animator _animator;
    private DefaultEnemyTasks parentTasks;
    private bool isWaitingToAttack = false;
    private float _prepareTimer;
    private bool _rechargingStamina = false;
    private bool _timedout = false;
    [SerializeField]
    private float _staminaMeter;
    private bool canSpawn = true;
    private BaseCharacter baseChar;

    private float scaleMod = -1f;
    private float minScale = 1f;
    private float maxScale = 1.05f;
    private float scaleTime = 0.5f;

    // Use this for initialization
    void Start()
    {
        _prepareTimer = preparationTime;
        _animator = GetComponent<Animator>();
        parentTasks = GetComponent<DefaultEnemyTasks>();
        baseChar = GetComponent<BaseCharacter>();

        _staminaMeter = stamina;

        StartCoroutine(ScaleDistortion(scaleTime, false));
    }

    private void Update()
    {
        if (isWaitingToAttack)
        {
            _prepareTimer -= Time.deltaTime;
            if (_prepareTimer <= 0)
            {
                //_prepareTimer = preparationTime;
                _timedout = true;
                //isWaitingToAttack = false;
            }
        }

        if (_rechargingStamina) _staminaMeter += Time.deltaTime * 7;
        if (_staminaMeter >= stamina)
        {
            _staminaMeter = stamina;
            _rechargingStamina = false;
            canSpawn = true;
        }

        _animator.SetBool("CanSpawn", canSpawn);

        if (lowHealthAnimator)
        {
            int[] hp = baseChar.Hitpoints();
            if (hp[0] <= Mathf.Ceil(hp[1] / 3))
            {
                _animator.runtimeAnimatorController = lowHealthAnimator;
            }
        }
    }

    public void SetActive(bool active)
    {
        isActive = active;
    }

    public void KillAllSpawnies()
    {
        try
        {
            Transform holder = spawner.parentObj;

            if (!holder) return;

            for (int i = 0; i < holder.childCount; i++)
            {
                Transform child = holder.GetChild(i);
                if (child.tag == "Enemy" && !child.GetComponent<BaseCharacter>().isBoss)
                {
                    Destroy(child.gameObject);
                }
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    private IEnumerator ScaleDistortion(float time, bool reduce)
    {
        Vector3 originalScale = new Vector3(Mathf.Abs(transform.localScale.x) * scaleMod, transform.localScale.y, transform.localScale.y);
        Vector3 destinationScale;
        if (reduce)
        {
            destinationScale = new Vector3(minScale * scaleMod, maxScale, originalScale.z);
        }
        else
        {
            destinationScale = new Vector3(maxScale * scaleMod, minScale, originalScale.z);
        }

        float currentTime = 0.0f;

        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        StartCoroutine(ScaleDistortion(scaleTime, !reduce));
    }

    [Task]
    bool SpendStamina()
    {
        if (_rechargingStamina) return false;
        //_staminaMeter -= Time.deltaTime * 10;
        _staminaMeter -= 1;

        if (_staminaMeter <= 0)
        {
            _rechargingStamina = true;
            return false;
        }
        else return true;
    }

    [Task]
    bool WaitingForAttack()
    {
        return isWaitingToAttack;
    }

    [Task]
    bool IsActive()
    {
        return isActive;
    }

    [Task]
    bool WaitTimedOut()
    {
        return _timedout;
    }

    [Task]
    bool Spawn()
    {
        if (parentTasks.cooldown <= 0)
        {
            _prepareTimer = preparationTime;
            _timedout = false;
            isWaitingToAttack = true;

            if (canSpawn)
            {
                //canSpawn = false;
                _animator.SetTrigger("SpawnEnemy");
                if (!spawner) return false;

                spawner.Spawn(-1);

                return true;
            }
        }
        return false;
    }

    [Task]
    bool ReturnToIdle()
    {
        isWaitingToAttack = false;
        _animator.SetBool("WaitingForAttack", false);
        return false;
    }
}
