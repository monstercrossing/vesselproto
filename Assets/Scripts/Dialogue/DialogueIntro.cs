﻿using Fluent;

public class DialogueIntro : FluentScript {
    public override FluentNode Create() {
        return Yell("Hello World!");
    }
}