﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPrompt : MonoBehaviour
{
    public GameObject canvas;
    public GameObject button;
    private bool canDismiss = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        /*bool inJump = Input.GetButton("Jump");
        if (inJump && canDismiss)
        {
            GameObject hero = GameObject.FindGameObjectWithTag("Hero");
            //Animator animator = hero.GetComponent<Animator>();
            {
                Dismiss();
            }
        }*/
    }

    public void Display()
    {
        canvas.gameObject.SetActive(true);
        canDismiss = true;
        button.SetActive(true);
        //StartCoroutine(CanDismiss());
        UIPauseMenuController.anyMenuOpen = true;
        Time.timeScale = 0;
    }

    public void Dismiss()
    {
        if (canDismiss)
        {
            canvas.gameObject.SetActive(false);
            button.gameObject.SetActive(false);
            //gameObject.SetActive(false);
            UIPauseMenuController.anyMenuOpen = false;
            Time.timeScale = 1;
        }
    }

    /*private IEnumerator CanDismiss()
    {
        canDismiss = true;
        button.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
    }*/
}
