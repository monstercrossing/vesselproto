﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public bool isTutorial = false;

    public GameObject tutorialChunk;
    public GameObject startChunk;
    public GameObject endChunk;
    public GameObject boss1Chunk;
    public GameObject boss2Chunk;
    public GameObject boss3Chunk;
    public GameObject boss4Chunk;
    public GameObject closedChunk;

    public List<GameObject> generalChunks;
    public List<GameObject> lvl1Chunks;
    public List<GameObject> lvl2Chunks;
    public List<GameObject> lvl3Chunks;
    public List<GameObject> lvl4Chunks;
    public List<GameObject> testChunks;
    public List<GameObject> chunksToFindBoss;

    public PermanentPool generalEgos;
    public PermanentPool lvl1Egos;
    public PermanentPool lvl2Egos;
    public PermanentPool lvl3Egos;
    public PermanentPool lvl4Egos;

    public GameEvent onLevelLoad;
    public GameEvent onLevelFinishedRender;
    public static bool generatingLevel = false;

    [SerializeField]
    private int level;
    public int enemyLevel {
        get {
            return level;
        }
    }

    private List<GameObject> activeList;

    // Valores são inicializados no RenderReset
    private Vector3 position;
    private bool searchingBoss;
    private bool bossFound;
    private bool beginAgain;

    private int[,] matrix;
    private int matrixX;
    private int matrixY;

    private GameObject player; 

    private List<string> usedChunks;

    private int maxChunksPerLvl;
    private int chunksLvlCount;
    private float chunkBaseSizeX = 36f;
    private float chunkBaseSizeY = 20f;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Hero");
        isTutorial = SaveSlotController.shouldLoadTutorial;

        level = 1;

        RenderReset();
        Render();
    }

    private void RenderReset()
    {
        Time.timeScale = 0;
        generatingLevel = true;
        //player.SetActive(false);

        foreach (Transform child in transform)
        {
            //if (level > 1 && child.tag != "LevelEnd")
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        if (level > 1) {
            isTutorial = false;
        }
        beginAgain = false;

        position = new Vector3(0f, 0f, 0f);
        searchingBoss = false;
        bossFound = false;

        matrix = new int[60, 120];
        matrixX = 30;
        matrixY = 60;

        usedChunks = new List<string>();

        maxChunksPerLvl = 15;
        switch(level) {
            default:
            case 1:
                maxChunksPerLvl = 15;
                break;
            case 2:
                maxChunksPerLvl = 20;
                break;
            case 3:
                maxChunksPerLvl = 25;
                break;
            case 4:
                maxChunksPerLvl = 30;
                break;
        }
        chunksLvlCount = 0;
    }

    void Render()
    {
        GameObject chunkToPlace;
        GameObject bossChunk;

        // Monta primeiro Chunk

        if (isTutorial)
        {
            chunkToPlace = (GameObject)Instantiate(tutorialChunk, transform);
            GameObject player = GameObject.FindGameObjectWithTag("Hero");
            player.transform.position = new Vector3(-3f, 3f, player.transform.localPosition.z);
        }
        else
        {
            chunkToPlace = (GameObject)Instantiate(startChunk, transform);
            GameObject player = GameObject.FindGameObjectWithTag("Hero");
            player.transform.position = new Vector3(16.7f, -3f, player.transform.localPosition.z);
        }

        if (!chunkToPlace) { return; }
        chunkToPlace.transform.position = position;
        chunkToPlace.transform.rotation = Quaternion.identity;

        if (isTutorial)
        {
            matrix[matrixX, matrixY] = 1;
            matrixY++;
            matrix[matrixX, matrixY] = 1;
        }
        else
        {
            matrix[matrixX, matrixY] = 1;
        }

        List<GameObject> thisLevelList;
        int listSize;

        // Monta Chunks do Level

        thisLevelList = new List<GameObject>();
        switch (level)
        {
            default:
            case 1:
                bossChunk = boss1Chunk;
                lvl1Chunks.Shuffle();
                thisLevelList.AddRange(lvl1Chunks);
                break;
            case 2:
                bossChunk = boss2Chunk;
                lvl2Chunks.Shuffle();
                thisLevelList.AddRange(lvl2Chunks);
                break;
            case 3:
                bossChunk = boss3Chunk;
                lvl3Chunks.Shuffle();
                thisLevelList.AddRange(lvl3Chunks);
                break;
            case 4:
                bossChunk = boss4Chunk;
                lvl4Chunks.Shuffle();
                thisLevelList.AddRange(lvl4Chunks);
                break;
        }
        //thisLevelList.AddRange(testChunks); bossChunk = boss1Chunk; // Selected chunks for test
        listSize = thisLevelList.Count;

        activeList = new List<GameObject>();
        activeList.AddRange(thisLevelList);

        // Percorre o total de chunks
        ChunkMagic(chunkToPlace);

        // Monta boss
        if (bossFound)
        {
            chunkToPlace = MontaBoss(bossChunk);
        }

        // Verifica se houve dead end
        if(beginAgain)
        {
            RenderReset();
            Render();

            return;
        }

        // Monta ending
        position += new Vector3(chunkBaseSizeX * boss4Chunk.GetComponent<LevelChunk>().blocksX, 0f, 0f);
        chunkToPlace = (GameObject)Instantiate(endChunk, transform);
        if (!chunkToPlace) { return; }
        chunkToPlace.transform.position = position;
        chunkToPlace.transform.rotation = Quaternion.identity;

        matrixY++;
        matrix[matrixX, matrixY] = 1;

        // Escreve a matrix em um arquivo
        //ShowMeTheMatrix();

        Time.timeScale = 1;
        generatingLevel = false;
        onLevelFinishedRender.Raise();
        //player.SetActive(true);
    }

    private void ChunkMagic(GameObject thisChunk) {

        // Pega o chunk atual e verifica as saídas, se não houver nenhum chunk disponível para elas interrompe
        LevelChunk levelChunk = thisChunk.GetComponent<LevelChunk>();
        if (!levelChunk) return;
        //Debug.Log("Chunk " + thisChunk.name + " exits:");

        // Gera uma lista aleatória de chunks com base na lista ativa
        List<GameObject> chunksList = new List<GameObject>();
        if (searchingBoss)
        {
            chunksList.AddRange(chunksToFindBoss);
        }
        else
        {
            chunksList = MergeList(activeList, generalChunks);
        }
        //ShuffleList(chunksList);
        //chunksList.Shuffle();

        // Pega uma saída aleatória e para cada uma delas procura um chunk
        List<int> exits = levelChunk.GetExits();
        exits.Shuffle();

        int entrance = -1;
        bool bFoundChunk = false;

        // Se está para esquerda, marca que encontrou o boss e retorna
        if(exits.Contains(1) && levelChunk.exitedBy != 1 && searchingBoss)
        {
            // Verifica se há espaço para a sala do boss
            if (IsMatrixFree(matrixX, matrixY + 1) &&
                IsMatrixFree(matrixX, matrixY + 2) &&
                IsMatrixFree(matrixX, matrixY + 3) &&
                IsMatrixFree(matrixX, matrixY + 4) &&
                (IsMatrixFree(matrixX, matrixY + 5) || level != 2))
            {
                // Percorre outras saídas colocando chunk de fechamento
                for (int y = 0; y < exits.Count; y++)
                {
                    if (exits[y] != 1)
                    {
                        CloseExit(thisChunk, matrixX, matrixY, exits[y]);
                    }
                }

                float chunkSizeX = levelChunk.blocksX * chunkBaseSizeX;
                float chunkSizeY = levelChunk.blocksX * chunkBaseSizeY;
                float chunkBlocksX = levelChunk.blocksX;
                float chunkBlocksY = levelChunk.blocksX;

                position += new Vector3(chunkSizeX, 0f, 0f);

                searchingBoss = false;
                bossFound = true;
            }
        }

        // Se já terminou de percorrer a quantidade necessária de chunks
        if ((chunksLvlCount >= maxChunksPerLvl && !searchingBoss) || bossFound)
        {
            return;
        }
        else
        {
            int thisMatrixX = matrixX;
            int thisMatrixY = matrixY;
            for (int j = 0; j < exits.Count; j++)
            {
                if(bossFound)
                {
                    break;
                }

                // Check for dead end
                if (!LeadToDeadEnd(exits[j]))
                {
                    entrance = ConvertExitEntrance(exits[j]);
                    if (entrance != levelChunk.enteredBy) // Não pega possíveis para mesma saída da última entrada
                    {
                        //Debug.Log("Exit " + exits[j]);
                        for (int k = 0; k < chunksList.Count; k++)
                        {
                            if (bossFound)
                            {
                                break;
                            }

                            // Se o chunk atual é diferente do chunk anterior
                            if (chunksList[k].name != levelChunk.realName)
                            {
                                // Se o chunk do índice atual requer perms que o player ainda não tem
                                bool hasRequirements = true;
                                List<PermanentDefinition> requirements = chunksList[k].GetComponent<LevelChunk>().GetRequirements();
                                for(int y = 0; y < requirements.Count; y++)
                                {
                                    /*
                                    if (!player.GetComponent<CharacterPermanents>().equippedPassives.Contains(requirements[y]))
                                    */
                                    if (!player.GetComponent<CharacterPermanents>().HasEquipped(requirements[y]))
                                    {
                                        hasRequirements = false;
                                    }
                                }

                                // Se tem requerimentos E há uma entrada no chunk do índice atual compatível com a saída
                                if (hasRequirements && chunksList[k].GetComponent<LevelChunk>().GetEntrances().Contains(entrance))
                                {
                                    //Debug.Log("Instantiate chunk #" + chunksLvlCount);
                                    GameObject newChunk = (GameObject)Instantiate(chunksList[k], transform);
                                    //Debug.Log(chunksList[k].name + " can enter by " + entrance);

                                    // Se o novo chunk pode repetir ou não foi usado
                                    if (newChunk.GetComponent<LevelChunk>().canRepeat || !usedChunks.Contains(chunksList[k].name))
                                    {
                                        Vector3 thisPosition = thisChunk.transform.position;

                                        int tempY = matrixY;
                                        int tempX = matrixX;
                                        bool mustDestroy = false;

                                        float chunkSizeX = levelChunk.blocksX * chunkBaseSizeX;
                                        float chunkSizeY = levelChunk.blocksY * chunkBaseSizeY;
                                        float chunkBlocksX = levelChunk.blocksX;
                                        float chunkBlocksY = levelChunk.blocksY;
                                        
                                        float newChunkSizeX = newChunk.GetComponent<LevelChunk>().blocksX * chunkBaseSizeX;
                                        float newChunkSizeY = newChunk.GetComponent<LevelChunk>().blocksY * chunkBaseSizeY;
                                        float newChunkBlocksX = newChunk.GetComponent<LevelChunk>().blocksX;
                                        float newChunkBlocksY = newChunk.GetComponent<LevelChunk>().blocksY;

                                        List<int> coordsX = new List<int>();
                                        List<int> coordsY = new List<int>();

                                        // Verifica qual é a saída e pega os tamanhos de chunks
                                        switch (exits[j])
                                        {
                                            default:
                                                //Debug.Log("Chunk " + newChunk.name + " no entrance");
                                                break;
                                            case 0:
                                                // Verifica quantos blocos Y da matriz este chunk ocupa
                                                for (int y = 0; y < newChunkBlocksY; y++)
                                                {
                                                    // Se é saída TOP, reduz uma ROW e verifica se há chunk ali E se há saída posterior livre
                                                    tempX -= 1;
                                                    if (matrix[tempX, tempY] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), tempX, tempY))
                                                    {
                                                        mustDestroy = true;
                                                    }
                                                    coordsX.Add(tempX);
                                                    coordsY.Add(tempY);
                                                }
                                                // Verifica quantos blocos X da matriz, além do padrão, este chunk ocupa
                                                for (int y = 1; y < newChunkBlocksX; y++)
                                                {
                                                    // Se é saída TOP, aumenta uma COLUMN e verifica se há chunk ali E se há saída posterior livre
                                                    tempY += 1; // Entrada é sempre por BOTTOM LEFT

                                                    for (int z = tempX; z < matrixX; z++)
                                                    {
                                                        if (matrix[z, tempY] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), z, tempY))
                                                        {
                                                            mustDestroy = true;
                                                        }
                                                        coordsX.Add(z);
                                                        coordsY.Add(tempY);
                                                    }
                                                }

                                                // Posiciona em tela
                                                thisPosition += new Vector3((chunkBlocksX - 1) * chunkBaseSizeX, 0f, 0f);
                                                thisPosition += new Vector3(0f, chunkSizeY, 0f);
                                                //Debug.Log("Chunk " + newChunk.name + " by " + ConvertExitEntrance(exits[j]));
                                                break;
                                            case 1:
                                                // Verifica quantos blocos X da matriz este chunk ocupa
                                                for (int y = 0; y < newChunkBlocksX; y++)
                                                {
                                                    // Se é saída RIGHT, aumenta uma COLUMN e verifica se há chunk ali E se há saída posterior livre
                                                    tempY += 1;
                                                    if (matrix[tempX, tempY] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), tempX, tempY))
                                                    {
                                                        mustDestroy = true;
                                                    }
                                                    coordsX.Add(tempX);
                                                    coordsY.Add(tempY);
                                                }
                                                // Verifica quantos blocos Y da matriz, além do padrão, este chunk ocupa
                                                for (int y = 1; y < newChunkBlocksY; y++)
                                                {
                                                    // Se é saída RIGHT, aumenta uma ROW e verifica se há chunk ali E se há saída posterior livre
                                                    tempX -= 1; // Entrada é sempre por BOTTOM LEFT

                                                    for (int z = matrixY + 1; z <= tempY; z++)
                                                    {
                                                        if (matrix[tempX, z] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), tempX, z))
                                                        {
                                                            mustDestroy = true;
                                                        }
                                                        coordsX.Add(tempX);
                                                        coordsY.Add(z);
                                                    }
                                                }

                                                // Posiciona em tela
                                                thisPosition += new Vector3(chunkSizeX, 0f, 0f);
                                                //thisPosition += new Vector3(0f, (chunkBlocksY - 1) * chunkBaseSizeY, 0f);
                                                //Debug.Log("Chunk " + newChunk.name + " by " + ConvertExitEntrance(exits[j]));
                                                break;
                                            case 2:
                                                // Verifica quantos blocos da matriz este chunk ocupa
                                                for (int y = 0; y < newChunkBlocksY; y++)
                                                {
                                                    // Se é saída BOTTOM, aumenta uma ROW e verifica se há chunk ali E se há saída posterior livre
                                                    tempX += 1;
                                                    if (matrix[tempX, tempY] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), tempX, tempY))
                                                    {
                                                        mustDestroy = true;
                                                    }
                                                    coordsX.Add(tempX);
                                                    coordsY.Add(tempY);
                                                }
                                                // Verifica quantos blocos X da matriz, além do padrão, este chunk ocupa
                                                for (int y = 1; y < newChunkBlocksX; y++)
                                                {
                                                    // Se é saída TOP, aumenta uma COLUMN e verifica se há chunk ali E se há saída posterior livre
                                                    tempY += 1; // Entrada é sempre por BOTTOM LEFT

                                                    for (int z = matrixX + 1; z <= tempX; z++)
                                                    {
                                                        if (matrix[z, tempY] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), z, tempY))
                                                        {
                                                            mustDestroy = true;
                                                        }
                                                        coordsX.Add(z);
                                                        coordsY.Add(tempY);
                                                    }
                                                }

                                                // Posiciona em tela
                                                thisPosition += new Vector3((chunkBlocksX - 1) * chunkBaseSizeX, 0f, 0f);
                                                thisPosition -= new Vector3(0f, chunkSizeY + (newChunkBlocksY - 1) * chunkBaseSizeY, 0f);
                                                //Debug.Log("Chunk " + newChunk.name + " by " + ConvertExitEntrance(exits[j]));
                                                break;
                                            case 3:
                                                // Verifica quantos blocos da matriz este chunk ocupa
                                                for (int y = 0; y < newChunkBlocksX; y++)
                                                {
                                                    // Se é saída LEFT, reduz uma COLUMN e verifica se há chunk ali E se há saída posterior livre
                                                    tempY -= 1;
                                                    if (matrix[tempX, tempY] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), tempX, tempY))
                                                    {
                                                        mustDestroy = true;
                                                    }
                                                    coordsX.Add(tempX);
                                                    coordsY.Add(tempY);
                                                }
                                                // Verifica quantos blocos Y da matriz, além do padrão, este chunk ocupa
                                                for (int y = 1; y < newChunkBlocksY; y++)
                                                {
                                                    // Se é saída RIGHT, aumenta uma ROW e verifica se há chunk ali E se há saída posterior livre
                                                    tempX -= 1; // Entrada é sempre por BOTTOM LEFT

                                                    for (int z = tempY; z < matrixY; z++)
                                                    {
                                                        if (matrix[tempX, z] == 1 || !VerifyExits(newChunk.GetComponent<LevelChunk>(), tempX, z))
                                                        {
                                                            mustDestroy = true;
                                                        }
                                                        coordsX.Add(tempX);
                                                        coordsY.Add(z);
                                                    }
                                                }

                                                // Posiciona em tela
                                                thisPosition -= new Vector3(newChunkBlocksX * chunkBaseSizeX, 0f, 0f);
                                                thisPosition += new Vector3(0f, (chunkBlocksY - 1) * chunkBaseSizeY, 0f);
                                                //Debug.Log("Chunk " + newChunk.name + " by " + ConvertExitEntrance(exits[j]));
                                                break;
                                        }

                                        // Se marcou para remover, destroi
                                        if (mustDestroy)
                                        {
                                            //Debug.Log("Chunk " + newChunk.name + " will collide");
                                            GameObject.Destroy(newChunk);
                                        }
                                        else
                                        {
                                            // Adiciona coords a matrix
                                            for (int y = 0; y < coordsX.Count; y++)
                                            {
                                                matrix[coordsX[y], coordsY[y]] = 1;
                                            }

                                            // Atualiza matrix
                                            matrixX = tempX;
                                            matrixY = tempY;
                                            //Debug.Log(matrix);

                                            // Posiciona na cena
                                            newChunk.transform.position = thisPosition;
                                            //Debug.Log("Chunk " + newChunk.name + " at " + thisPosition.x + "," + thisPosition.y + "," + thisPosition.z);
                                            newChunk.transform.rotation = Quaternion.identity;
                                            position = thisPosition;

                                            // Adiciona chunk nos usados
                                            usedChunks.Add(chunksList[k].name);
                                            chunksLvlCount++;
                                            if (chunksLvlCount >= maxChunksPerLvl)
                                            {
                                                searchingBoss = true;
                                                bossFound = false;
                                            }

                                            // Faz marcações no novo chunk
                                            newChunk.GetComponent<LevelChunk>().enteredBy = exits[j];
                                            newChunk.GetComponent<LevelChunk>().exitedBy = ConvertExitEntrance(exits[j]);
                                            newChunk.GetComponent<LevelChunk>().realName = chunksList[k].name;
                                            newChunk.GetComponent<LevelChunk>().previousChunk = thisChunk.name;
                                            newChunk.GetComponent<LevelChunk>().chunkListIndex = k;

                                            // Escreve matrix
                                            //ShowMeTheMatrix();

                                            // Percorre outras saídas colocando chunk de fechamento
                                            for (int y = 0; y < exits.Count; y++)
                                            {
                                                if (y != j)
                                                {
                                                    CloseExit(thisChunk, thisMatrixX, thisMatrixY, exits[y]);
                                                }
                                            }

                                            // Chamada recursiva a partir do novo chunk
                                            ChunkMagic(newChunk);

                                            // Interrompe laços
                                            bFoundChunk = true;
                                        }
                                    }
                                    else
                                    {
                                        // Caso não tenha
                                        //Debug.Log("Chunk " + newChunk.name + " cannot repeat");
                                        GameObject.Destroy(newChunk);
                                    }
                                }
                            }

                            if (bFoundChunk)
                            {
                                break;
                            }
                        }
                    }
                }
                else
                {
                    Debug.Log(thisChunk.name + " exit " + exits[j] + " lead to dead end");
                    if (exits.Count <= 1)
                    {
                        Debug.Log("It's the only exit. Take this step. Reboot!");
                        beginAgain = true;
                        break;
                    }
                }

                if (bFoundChunk)
                {
                    break;
                }
            }
            if (!bFoundChunk)
            {
                Debug.Log("Dead end. Take this step. Reboot!");
                beginAgain = true;
            }
        }

        return;
    }

    private void CloseExit(GameObject thisChunk, int thisMatrixX, int thisMatrixY, int exit)
    {
        LevelChunk levelChunk = thisChunk.GetComponent<LevelChunk>();
        if (exit != levelChunk.exitedBy)
        {
            float chunkSizeX = levelChunk.blocksX * chunkBaseSizeX;
            float chunkSizeY = levelChunk.blocksY * chunkBaseSizeY;
            float chunkBlocksX = levelChunk.blocksX;
            float chunkBlocksY = levelChunk.blocksY;

            bool canClose = false;

            Vector3 thisPosition = thisChunk.transform.position;
            switch (exit)
            {
                default:
                    break;
                case 0:
                    if (matrix[thisMatrixX - 1, thisMatrixY] != 1)
                    {
                        thisPosition += new Vector3((chunkBlocksX - 1) * chunkBaseSizeX, 0f, 0f);
                        thisPosition += new Vector3(0f, chunkSizeY, 0f);
                        canClose = true;
                    }
                    thisMatrixX--;
                    break;
                case 1:
                    if (matrix[thisMatrixX, thisMatrixY + 1] != 1)
                    {
                        thisPosition += new Vector3(chunkSizeX, 0f, 0f);
                        thisPosition += new Vector3(0f, (chunkBlocksY - 1) * chunkBaseSizeY, 0f);
                        canClose = true;
                    }
                    thisMatrixY++;
                    break;
                case 2:
                    if (matrix[thisMatrixX + 1, thisMatrixY] != 1)
                    {
                        thisPosition += new Vector3((chunkBlocksX - 1) * chunkBaseSizeX, 0f, 0f);
                        thisPosition -= new Vector3(0f, chunkSizeY, 0f);
                        canClose = true;
                    }
                    thisMatrixX++;
                    break;
                case 3:
                    if (matrix[thisMatrixX, thisMatrixY - 1] != 1)
                    {
                        thisPosition -= new Vector3(chunkSizeX, 0f, 0f);
                        thisPosition -= new Vector3(0f, (chunkBlocksY - 1) * chunkBaseSizeY, 0f);
                        canClose = true;
                    }
                    thisMatrixY--;
                    break;
            }

            if (canClose)
            {
                GameObject chunkToPlace = (GameObject)Instantiate(closedChunk, transform);
                if (!chunkToPlace) { return; }
                chunkToPlace.transform.position = thisPosition;
                chunkToPlace.transform.rotation = Quaternion.identity;

                matrix[thisMatrixX, thisMatrixY] = 1;
            }
        }

        return;
    }

    private GameObject MontaBoss(GameObject bossChunk)
    {
        // Reseta variáveis
        searchingBoss = false;
        bossFound = false;
        chunksLvlCount = 0;

        GameObject chunkToPlace = (GameObject)Instantiate(bossChunk, transform);
        if (!chunkToPlace) { return null; }
        chunkToPlace.transform.position = position;
        chunkToPlace.transform.rotation = Quaternion.identity;

        for (int i = 0; i < bossChunk.GetComponent<LevelChunk>().blocksX; i++)
        {
            matrixY++;
            matrix[matrixX, matrixY] = 1;
        }

        return chunkToPlace;
    }

    public void NewLevel()
    {
        onLevelLoad.Raise();
        level++;
        RenderReset();
        Render();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private int ConvertExitEntrance(int j)
    {
        // Pega o valor inverso entre saída e entrada (0 a 3)

        if (j == 0) return 2;
        else if (j == 1) return 3;
        else if (j == 2) return 0;
        else if (j == 3) return 1;
        else return -1;
    }

    private bool VerifyExits(LevelChunk chunk, int tempX, int tempY)
    {
        int thisRow = tempX;
        int thisCol = tempY;
        int countPossible = 0;

        // Percorre saídas do chunk de destino, verificando se há saídas livres de chunks
        try
        {
            List<int> exits = chunk.GetExits();
            for (int j = 0; j < exits.Count; j++)
            {
                switch (exits[j])
                {
                    default:
                        break;
                    case 0:
                        if (matrix[thisRow - 1, thisCol] != 1) countPossible++;
                        break;
                    case 1:
                        if (matrix[thisRow, thisCol + 1] != 1) countPossible++;
                        break;
                    case 2:
                        if (matrix[thisRow + 1, thisCol] != 1) countPossible++;
                        break;
                    case 3:
                        if (matrix[thisRow, thisCol - 1] != 1) countPossible++;
                        break;
                }
            }
        }
        catch (System.Exception e)
        {
            //Debug.Log(e.Message);
        }

        return (countPossible > 0);
    }

    private bool IsMatrixFree(int x, int y)
    {
        return (matrix[x, y] != 1);
    }

    private bool LeadToDeadEnd(int exit)
    {
        if (exit == 0)
        {
            if(//matrix[matrixX - 2,matrixY] == 1 &&
               matrix[matrixX - 1, matrixY - 1] == 1 &&
               matrix[matrixX - 1, matrixY + 1] == 1)
            {
                return true;
            }
        }
        else  if (exit == 1)
        {
            if (matrix[matrixX - 1, matrixY + 1] == 1 &&
                //matrix[matrixX, matrixY + 2] == 1 &&
                matrix[matrixX + 1, matrixY + 1] == 1)
            {
                return true;
            }
        }
        else if (exit == 2)
        {
            if (//matrix[matrixX + 2, matrixY] == 1 &&
                matrix[matrixX + 1, matrixY - 1] == 1 &&
                matrix[matrixX + 1, matrixY + 1] == 1)
            {
                return true;
            }
        }
        else if (exit == 3)
        {
            if (matrix[matrixX - 1, matrixY - 1] == 1 &&
                //matrix[matrixX, matrixY - 2] == 1 &&
                matrix[matrixX + 1, matrixY - 1] == 1)
            {
                return true;
            }
        }

        return false;
    }

    private void ShowMeTheMatrix()
    {
        string fileName = "Assets/matrix.txt";
        //Write some text to the matrix.txt file
        if (File.Exists(fileName))
        {
            File.Delete(fileName);
        }
        var sr = File.CreateText(fileName);
        string line;

        int rowLength = matrix.GetLength(0);
        int colLength = matrix.GetLength(1);

        for (int i = 0; i < rowLength; i++)
        {
            line = "";
            for (int j = 0; j < colLength; j++)
            {
                if(matrix[i, j] == 0) { line += "-"; }
                else { line += "X"; }
                //Debug.Log(string.Format("{0} ", matrix[i, j]));
            }
            //Debug.Log(Environment.NewLine + Environment.NewLine);

            sr.WriteLine(line);
        }
        sr.Close();

        return;
    }

    private void ShuffleList(List<GameObject> a)
    {
        // Loop array
        for (int i = 0; i < a.Count; i++)
        {
            // Randomize a number between 0 and i (so that the range decreases each time)
            int rnd = UnityEngine.Random.Range(0, a.Count-1);

            // Save the value of the current i, otherwise it'll overwrite when we swap the values
            GameObject temp = a[i];

            // Swap the new and old values
            a[i] = a[rnd];
            a[rnd] = temp;
        }
    }

    private List<GameObject> MergeList(List<GameObject> a, List<GameObject> b)
    {
        ShuffleList(a);
        ShuffleList(b);
        List<GameObject> c = new List<GameObject>();

        // Loop array
        int i = 0;
        int i1 = 0;
        int i2 = 0;
        int iTotal = (a.Count + b.Count);
        while (i < iTotal)
        {
            // Add 1 from list B
            if (i2 < b.Count)
            {
                c.Add(b[i2]);
                i2++;
            }

            // Add 1-3 from list A
            if (i1 < a.Count)
            {
                c.Add(a[i1]);
                i1++;
            }
            /*if (i1 < a.Count)
            {
                c.Add(a[i1]);
                i1++;
            }
            if (i1 < a.Count)
            {
                c.Add(a[i1]);
                i1++;
            }*/
            i++;
        }

        //c.AddRange(a);
        //c.AddRange(b);

        return c;
    }
}
