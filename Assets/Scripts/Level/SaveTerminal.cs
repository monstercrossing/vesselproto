﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveTerminal : MonoBehaviour {

    public bool canActivate = true;
    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        anim.SetBool("CanActivate", canActivate);
	}

    public void SetState(bool state)
    {
        if (state == false)
        {
            //gameObject.GetComponent<InteractionTrigger>().SetEnabled(false); // Ativa uso único
            StartCoroutine(Anim(state));
        } else
        {
            canActivate = state;
        }
    }

    IEnumerator Anim(bool state)
    {
        canActivate = state;
        yield return new WaitForSeconds(1);
        canActivate = true;
    }
}
