﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecretObject : MonoBehaviour
{
    public GameObject secretObject;
    GameObject target;
    CharacterEventHub hub;
    // Start is called before the first frame update
    void Start()
    {
        if (!target)
            target = GameObject.FindGameObjectWithTag("Hero");
        if (target)
            hub = target.GetComponent<CharacterEventHub>();
    }

    // Update is called once per frame
    void Update()
    {
        if(hub) {
            secretObject.SetActive(hub.secretEnabled);
        }
    }
}
