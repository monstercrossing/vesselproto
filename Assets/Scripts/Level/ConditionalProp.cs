﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PropRuleSet {
    public bool have;
    public PermanentDefinition permanent;
}

public class ConditionalProp : MonoBehaviour {
    public PropRuleSet[] rules;

    public void Initialize(PermanentDefinition[] unlockedPermanents) {
        List<PermanentDefinition> perms = new List<PermanentDefinition>(unlockedPermanents);

        bool passed = true;

        for (int i =0; i < rules.Length; i++) {
            if ( (rules[i].have && perms.Contains(rules[i].permanent)) ||
                (!rules[i].have && !perms.Contains(rules[i].permanent)) ) {
                passed &= true;
            } else {
                passed = false;
                break;
            }
        }

        gameObject.SetActive(passed);
    }
}
