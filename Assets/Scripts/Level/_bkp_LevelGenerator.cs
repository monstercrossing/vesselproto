﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _bkp_LevelGenerator : MonoBehaviour
{
    public GameObject tutorialChunk;
    public GameObject elevatorChunk;
    public GameObject boss1Chunk;
    public GameObject boss2Chunk;
    public GameObject boss3Chunk;
    public GameObject boss4Chunk;
    public GameObject closedChunk;

    public List<GameObject> generalList;
    public List<GameObject> lvl1List;
    public List<GameObject> lvl2List;
    public List<GameObject> lvl3List;
    public List<GameObject> lvl4List;
    public List<GameObject> testList;

    private Vector3 position = new Vector3(0f, 0f, 0f);

    // Start is called before the first frame update
    void Start()
    {

        // Monta primeiro Chunk

        int intAux = -1;
        GameObject chunk;

        /*if(tutorial) {
            chunk = (GameObject)Instantiate(tutorialChunk, transform);
            GameObject player = GameObject.FindGameObjectWithTag("Hero");
            player.transform.position = new Vector3(-3f, 3f, player.transform.localPosition.z);
        }
        else*/
        {
            chunk = (GameObject)Instantiate(elevatorChunk, transform);
            GameObject player = GameObject.FindGameObjectWithTag("Hero");
            player.transform.position = new Vector3(22f, -3f, player.transform.localPosition.z);
        }

        if(!chunk) { return; }
        chunk.transform.position = position;
        chunk.transform.rotation = Quaternion.identity;

        GameObject lastChunk = chunk;
        int lastExit = -1;
        int lastEnter = -1;

        // Monta Chunks do Level 1
        List<GameObject> thisList = lvl1List;
        //List<GameObject> thisList = testList;
        thisList.AddRange(generalList);

        int listSize = thisList.Count;

        chunk = null;

        // Percorre o total de chunks
        for (int i=0; i<10; i++)
        {
            // Se o i é maior que o tamanho da lista, para
            if(listSize <= i)
            {
                break;
            }

            // Gera posição de acordo com a cena e o chunk anterior

            // Pega o chunk atual e verifica as saídas, se não houver nenhum chunk disponível para elas interrompe
            LevelChunk thisChunk = lastChunk.GetComponent<LevelChunk>();
            if (!thisChunk) return;

            Debug.Log("Chunk " + lastChunk.name + " exits:");
            List<int> exits = new List<int>();
            List<int> possibleIndexes = new List<int>();
            for (int j = 0; j < thisChunk.allowedExits.Length; j++)
            {
                if (thisChunk.allowedExits[j] && j != lastEnter) // Não pega possíveis para mesma saída da última entrada
                {
                    Debug.Log("Exit " + j);
                    exits.Add(j);

                    for(int k = 0; k < thisList.Count; k++)
                    {
                        intAux = -1;
                        if (j == 0) intAux = 2;
                        else if (j == 1) intAux = 3;
                        else if (j == 2) intAux = 0;
                        else if (j == 3) intAux = 1;
                        if (thisList[k].GetComponent<LevelChunk>().allowedEntrances[intAux])
                        {
                            Debug.Log(thisList[k].name + " can enter by " + intAux);
                            if (!possibleIndexes.Contains(k)) possibleIndexes.Add(k);
                        }
                    }
                }
            }

            // Se tem possibilidades de Chunk, pega um index aleatório para bater saída e entrada
            if (possibleIndexes.Count > 0)
            {
                bool bfound = false;
                int icount = 0;

                while (!bfound && icount < possibleIndexes.Count)
                {
                    // Com a lista final, busca um objeto para inserir
                    int randIndex = Random.Range(0, possibleIndexes.Count - 1);
                    int index = possibleIndexes[randIndex];
                    chunk = (GameObject)Instantiate(thisList[index], transform);
                    if (thisList.Count > 1)
                    {
                        while (chunk.Equals(lastChunk))
                        {
                            index = Random.Range(0, thisList.Count - 1);
                            chunk = (GameObject)Instantiate(thisList[index], transform);
                        }
                    }
                    else if (chunk.Equals(lastChunk))
                    {
                        chunk = null;
                    }

                    // Se o chunk é inválido, para
                    if (chunk != null)
                    {
                        // Pega uma saída aleatória e verifica se é a mesma entrada. Se só tiver uma saída e for a mesma da entrada, não mostra o chunk

                        exits = new List<int>();
                        for (int l = 0; l < chunk.GetComponent<LevelChunk>().allowedEntrances.Length; l++)
                        {
                            if (chunk.GetComponent<LevelChunk>().allowedEntrances[l])
                            {
                                intAux = -1;
                                if (l == 0) intAux = 2;
                                else if (l == 1) intAux = 3;
                                else if (l == 2) intAux = 0;
                                else if (l == 3) intAux = 1;

                                if (thisChunk.allowedExits[intAux])
                                {
                                    exits.Add(intAux);
                                }
                            }
                        }

                        // Tendo todas as saídas possíveis do objeto aleatório, monta uma delas
                        int exit = -1;
                        if (exits.Count > 0)
                        {
                            exit = exits[Random.Range(0, exits.Count - 1)];

                            if (exits.Count > 1)
                            {
                                while (exit == lastEnter)
                                {
                                    exit = exits[Random.Range(0, exits.Count - 1)];
                                }
                            }
                            else if (exit == lastEnter)
                            {
                                chunk = null;
                            }
                        }
                        else
                        {
                            chunk = null;
                        }

                        // Posisiona na saída
                        if (chunk != null && exit >= 0)
                        {
                            //LevelChunk nextChunk = chunk.GetComponent<LevelChunk>();

                            // TODO verificação adicional se há chunk no local que gerar
                            int enter = lastEnter;
                            if (exit == 0)
                            {
                                float chunkSize = Mathf.Ceil(thisChunk.GetBounds().size.y);
                                position += new Vector3(0f, chunkSize, 0f);
                                enter = 2;
                                Debug.Log("Chunk " + chunk.name + " by BOTTOM");
                            }
                            else if (exit == 1)
                            {
                                float chunkSize = Mathf.Ceil(thisChunk.GetBounds().size.x);
                                position += new Vector3(chunkSize, 0f, 0f);
                                enter = 3;
                                Debug.Log("Chunk " + chunk.name + " by RIGHT");
                            }
                            else if (exit == 2)
                            {
                                float chunkSize = Mathf.Ceil(thisChunk.GetBounds().size.y);
                                position -= new Vector3(0f, chunkSize, 0f);
                                enter = 0;
                                Debug.Log("Chunk " + chunk.name + " by LEFT");
                            }
                            else if (exit == 3)
                            {
                                float chunkSize = Mathf.Ceil(thisChunk.GetBounds().size.x);
                                position -= new Vector3(chunkSize, 0f, 0f);
                                enter = 1;
                                Debug.Log("Chunk " + chunk.name + " by TOP");
                            }

                            // Se não pode repetir, já remove da lista
                            if (!chunk.GetComponent<LevelChunk>().canRepeat)
                            {
                                thisList.RemoveAt(index);
                            }

                            // Posiciona na cena
                            chunk.transform.position = position;
                            Debug.Log("Chunk " + chunk.name + " at " + position.x + "," + position.y + "," + position.z);
                            chunk.transform.rotation = Quaternion.identity;

                            lastChunk = chunk;

                            lastEnter = enter;
                            lastExit = exit;

                            bfound = true;
                        }
                    }

                    if(chunk == null)
                    {
                        possibleIndexes.RemoveAt(randIndex);
                        icount--;
                    }

                    icount++;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
