﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Hero");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPermanent(PermanentDefinition perm)
    {
        if (player)
        {
            CharacterPermanents egos = player.GetComponent<CharacterPermanents>();
            egos.ReceiveAndEquipEgo(perm);
        }

    }

    public void SetPowerup(PowerupDefinition power) {
        if(player)
        {
            player.GetComponent<BasePlayer>().ReceivePowerup(power);
        }
    }

    public GameObject GetPlayer()
    {
        return player;
    }
}
