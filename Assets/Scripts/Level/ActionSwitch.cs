﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionSwitch : MonoBehaviour
{
    public bool isActive = false;
    public bool oneTime = false;
    public bool isEnabled = true;
    public float coolDown = 0f;
    private Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("IsActive", isActive);
        gameObject.GetComponent<InteractionTrigger>().SetEnabled(isEnabled);
    }

    public void SetState(bool state)
    {
        if (isEnabled)
        {
            if (oneTime)
            {
                isEnabled = false;
                gameObject.GetComponent<InteractionTrigger>().SetEnabled(isEnabled);
            }

            if (coolDown > 0f)
            {
                StartCoroutine(Anim(state));
            }
            else
            {
                isActive = state;
            }
        }
    }

    public void ChangeState()
    {
        if (isEnabled)
        {
            if (oneTime)
            {
                isEnabled = false;
                gameObject.GetComponent<InteractionTrigger>().SetEnabled(isEnabled);
            }

            bool newState = !isActive;
            if (coolDown > 0f)
            {
                StartCoroutine(Anim(newState));
            }
            else
            {
                isActive = newState;
            }
        }
    }

    IEnumerator Anim(bool state)
    {
        isActive = state;
        isEnabled = false;
        anim.SetBool("IsEnabled", isEnabled);
        yield return new WaitForSeconds(coolDown);
        isEnabled = true;
        anim.SetBool("IsEnabled", isEnabled);
        isActive = !state;
    }
}