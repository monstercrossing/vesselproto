﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Animations;
using UnityEngine;

public class NPCRand : MonoBehaviour
{
    //public List<AnimatorController> controllers = new List<AnimatorController>();
    public List<RuntimeAnimatorController> animators = new List<RuntimeAnimatorController>();

    private Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        if(animators.Count > 0)
        {
            //anim.runtimeAnimatorController = controllers[Random.Range(0, controllers.Count)];
            anim.runtimeAnimatorController = animators[Random.Range(0, animators.Count)];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
