﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoScroll : MonoBehaviour
{
    public float scrollSpeed;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start()
    {
    }

    void Update()
    {
        // Define a target position above and behind the target transform
        Vector3 targetPosition = transform.TransformPoint(new Vector3(-scrollSpeed/100f, 0f, 0f));
        // Smoothly move the camera towards that target position
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, Time.deltaTime);
    }

    private void FixedUpdate()
    {
    }
}
