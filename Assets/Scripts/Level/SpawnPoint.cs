﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class SpawnPoint : MonoBehaviour {

    public GameObject prefabOrInstance;
    public GameObjectPool prefabList;

    public int spawnMode = 0; //0 = Once, 1 = Continuous; 2 = OnTrigger
    public int maxOfSimultaneousEnemies = 1;
    public float spawnWait = 5.0f;
    public ContactTrigger triggerObject;
    public float spawnProb = 1f;
    public Transform parentObj;

    private float _timer = 0;
    private int _spawnCount = 0;
    float _rand = 1f;

    void Start() {
        if (spawnMode == 0) {
            Spawn();
            Destroy(gameObject);
        }
        if (spawnMode == 2 && triggerObject)
            triggerObject.OnContact.AddListener(Spawn);

        if(spawnMode != 2)
        {
            _rand = Random.value; //Debug.Log(_rand);
        }

        if(!parentObj)
        {
            parentObj = transform.parent;
        }
    }

    private void Update() {
        if(spawnMode == 1) {
            _timer -= Time.deltaTime;

            if (_timer <= 0) {
                Spawn();
                _timer = spawnWait;
            }
        }
    }

    public void Spawn(int direction)
    {
        if (maxOfSimultaneousEnemies > 0 && _spawnCount >= maxOfSimultaneousEnemies) return;
        if (prefabList != null)
        {
            if (prefabList.fullList.Count > 0)
            {
                prefabOrInstance = prefabList.fullList[Random.Range(0, (prefabList.fullList.Count - 1))];
            }
        }
        if (!prefabOrInstance) return;

        if (direction < -1) direction = -1;
        else if (direction > 1) direction = 1;

        if (_rand <= spawnProb)
        { // Spawn probability
            GameObject instance = GameObject.Instantiate(prefabOrInstance);
            instance.SetActive(true);
            instance.transform.parent = parentObj;
            instance.transform.position = transform.position;

            instance.transform.localScale = new Vector3(direction, instance.transform.localScale.y, instance.transform.localScale.z);
            instance.GetComponent<FlipSprite>().facingRight = (direction==1);

            _spawnCount++;
            BaseCharacter _char = instance.transform.GetOrAddComponent<BaseCharacter>();
            _char.onDeath.AddListener(DecreaseSpawnCount);
        }
    }

    public void Spawn() {
        Spawn(1);
    }

    void DecreaseSpawnCount() {
        _spawnCount--;
    }

     void OnDrawGizmos() {
        Gizmos.DrawIcon(transform.position, "spawn.png", true);
    }
}
