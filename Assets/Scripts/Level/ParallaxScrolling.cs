﻿using UnityEngine;
using System.Collections;
public class ParallaxScrolling : MonoBehaviour
{
    public Transform[] backgrounds; // Array (list) of all the back- and foregrounds to be parallaxed
    private float[] parallaxScales; // The proportion of the camera's movement to move the backgrounds by
    public float smoothing = 1f; // How smooth the parallax is going to be. Make sure to set this above 0
    private Transform cam; // reference to the main cameras transform
    private Vector3 previousCamPos; // the position of the camera in the previous frame
    private BoxCollider2D groundCollider;       //This stores a reference to the collider attached to the Ground.
    private float groundHorizontalLength;       //A float to store the x-axis length of the collider2D attached to the Ground GameObject.
    

    //Is called before Start(). Great for references.
    void Awake()
    {
        // set up camera the reference
        cam = Camera.main.transform;

        //Get and store a reference to the collider2D attached to Ground.
        groundCollider = GetComponent<BoxCollider2D>();
        //Store the size of the collider along the x axis (its length in units).
        groundHorizontalLength = groundCollider.size.x;
    }
    // Use this for initialization
    void Start()
    {
        // The previous frame had the current frame's camera position
        previousCamPos = cam.position;
        // asigning coresponding parallaxScales
        parallaxScales = new float[backgrounds.Length];
        for (int i = 0; i < backgrounds.Length; i++)
        {
            parallaxScales[i] = backgrounds[i].position.z * -1;
        }
    }
    // Update is called once per frame
    void Update()
    {
        // for each background
        for (int i = 0; i < backgrounds.Length; i++)
        {
            // the parallax is the opposite of the camera movement because the previous frame multiplied by the scale
            float parallax = (previousCamPos.x - cam.position.x) * parallaxScales[i];
            // set a target x position which is the current position plus the parallax
            float backgroundTargetPosX = backgrounds[i].position.x + parallax;
            // create a target position which is the background's current position with it's target x position
            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgrounds[i].position.y, backgrounds[i].position.z);
            // fade between current position and the target position using lerp
            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
            
            //Check if the difference along the x axis between the main Camera and the position of the object this is attached to is greater than groundHorizontalLength.
            if (backgrounds[i].localPosition.x < -groundHorizontalLength)
            {
                //If true, this means this object is no longer visible and we can safely move it forward to be re-used.
                RepositionBackground(backgrounds[i], groundHorizontalLength);
            } else if (backgrounds[i].localPosition.x > groundHorizontalLength)
            {
                //If true, this means this object is no longer visible and we can safely move it forward to be re-used.
                RepositionBackground(backgrounds[i], -groundHorizontalLength);
            }
        }

        // set the previousCamPos to the camera's position at the end of the frame
        previousCamPos = cam.position;
    }

    //Moves the backgrounds to right and left in order to create our looping background effect.
    private void RepositionBackground(Transform background, float offset)
    {
        //This is how far to the right we will move our background object, in this case, twice its length. This will position it directly to the right of the currently visible background object.
        Vector3 groundOffSet = new Vector3(offset * 2f, 0f, 0f);

        //Move this object from it's position offscreen, behind the player, to the new position off-camera in front of the player.
        background.position = (Vector3)background.position + groundOffSet;
    }
}