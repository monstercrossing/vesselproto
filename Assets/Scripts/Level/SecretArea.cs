﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class SecretArea : MonoBehaviour
{
    private GameObject obj;
    public bool reverse = false;
    private bool canFade = false;
    private bool audioPlay = false;
    private Color alphaColor;
    private float timeToFade = 1.0f;

    public void Start()
    {
        obj = gameObject;

        alphaColor = obj.GetComponent<Tilemap>().color;
        if (reverse)
        {
            alphaColor.a = 0;
            obj.GetComponent<Tilemap>().color = alphaColor;
            alphaColor.a = 1;
        } else
        {
            alphaColor.a = 0;
        }
    }
    public void Update()
    {
        if (canFade)
        {
            obj.GetComponent<Tilemap>().color = Color.Lerp(obj.GetComponent<Tilemap>().color, alphaColor, timeToFade * Time.deltaTime);
            if (!audioPlay)
            {
                audioPlay = true;
                obj.GetComponent<AudioSource>().Play();
            }
        }
    }

    public void Reveal()
    {
        canFade = true;
    }
}
