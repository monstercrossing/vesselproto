﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelElevator : MonoBehaviour
{
    private LevelGenerator level;

    // Start is called before the first frame update
    void Start()
    {
        GameObject lvl = GameObject.FindGameObjectWithTag("Level");
        level = lvl.GetComponent<LevelGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Elevate()
    {
        if (level)
        {
            level.NewLevel();
        }
    }
}
