﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PropSet {
    public Biome biome;
    public GameObject prop;

    public bool Equals(PropSet other) {
        if (other == null) return false;
        return this.biome == other.biome;
    }
}

public class BiomeProp : MonoBehaviour
{
    public PropSet[] props;

    // Start is called before the first frame update
    void Start() {
        
    }

    public void SetBiome(Biome currentBiome) {
        for (int i = 0; i < props.Length; i++) {
            props[i].prop.SetActive(props[i].biome == currentBiome);
        }
    }

}
