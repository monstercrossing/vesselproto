﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoffeeMachine : MonoBehaviour {

    public bool canActivate = true;
    public int healingAmount = 1;
    private Animator anim;

    // Use this for initialization
    void Start () {
        anim = gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        anim.SetBool("CanActivate", canActivate);
	}

    public void SetState(bool state)
    {
        canActivate = state;
        gameObject.GetComponent<InteractionTrigger>().SetEnabled(false);
    }

    public void Heal()
    {
        PlayerInteract pi = GetComponent<PlayerInteract>();
        GameObject player = pi.GetPlayer();
        player.GetComponent<BasePlayer>().FullHeal();

        SetState(false);
    }
}
