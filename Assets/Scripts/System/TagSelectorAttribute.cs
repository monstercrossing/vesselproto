﻿using UnityEngine;

public class TagSelectorAttribute : PropertyAttribute {
    public bool UseDefaultTagFieldDrawer = false;
}

public class EnumFlagAttribute : PropertyAttribute {
    public string enumName;

    public EnumFlagAttribute() { }

    public EnumFlagAttribute(string name) {
        enumName = name;
    }
}