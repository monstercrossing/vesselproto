﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventCounterTrigger : MonoBehaviour {
    public int counterThreshold;
    public bool resetOnThreshold;

    public UnityEvent localEvent;
    public GameEvent globalEvent;
    private int _currentCounter = 0;

    public int count { get { return _currentCounter; } }
    public void RaiseCounter() {
        _currentCounter++;
        if(_currentCounter >= counterThreshold) {
            localEvent.Invoke();
            if (globalEvent) globalEvent.Raise();
            if (resetOnThreshold) _currentCounter = 0;
        }
    }
    public void DecreaseCounter() {
        _currentCounter--;
    }

}
