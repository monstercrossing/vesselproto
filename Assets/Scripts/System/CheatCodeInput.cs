﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatCodeInput : MonoBehaviour
{
    public string[] keys;
    public float timeout = 0.9f;
    public UnityEngine.Events.UnityEvent OnSuccess;

    public int currentIndex = 0;
    private float timer = 0;
    private bool lastFrameInput = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bool keydown = false;// Input.GetButtonDown(keys[currentIndex]);
        if(keys[currentIndex] == "up" && Input.GetAxis("Vertical") > 0) keydown = true;
        if(keys[currentIndex] == "down" && Input.GetAxis("Vertical") < 0) keydown = true;
        if(keys[currentIndex] == "right" && Input.GetAxis("Horizontal") > 0) keydown = true;
        if(keys[currentIndex] == "left" && Input.GetAxis("Horizontal") < 0) keydown = true;
        if(keydown && !lastFrameInput) {
            currentIndex++;
            timer = timeout;
            lastFrameInput = true;
        } else {
            if(Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0) lastFrameInput = false;
        }
        if(currentIndex >= keys.Length) {
             OnSuccess.Invoke();
            Destroy(this);
        }

        timer -= Time.deltaTime;
        if(timer < 0) currentIndex = 0;
    }
}
