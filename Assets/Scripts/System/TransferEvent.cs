﻿using UnityEngine.Events;
using UnityEngine;

[System.Serializable]
public class TransferEvent : UnityEvent<MonoBehaviour> {}