﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

[System.Serializable]
public class LanguageTuple {
    public string key;
    public string name;
}

public class GameSettings : MonoBehaviour {
    public Dropdown languageSelector;
    public List<LanguageTuple> languageList;
    public AudioMixerGroup bgmMixer;
    public Slider bgmSlider;
    public AudioMixerGroup sfxMixer;
    public Slider sfxSlider;

    public void Start() {
        if(languageSelector) {
            languageSelector.ClearOptions();
            if( !PlayerPrefs.HasKey("longway.lang")) PlayerPrefs.SetString("longway.lang","pt_BR");
            string currentLanguage = PlayerPrefs.GetString("longway.lang");
            for(int i=0; i<languageList.Count; i++) {
                languageSelector.options.Add(new Dropdown.OptionData(languageList[i].name));
                if(currentLanguage == languageList[i].key) {
                    languageSelector.value = i;
                }
            }
        }

        if( !PlayerPrefs.HasKey("longway.bgm")) PlayerPrefs.SetFloat("longway.bgm",1);
        if (bgmMixer) {
            float currentBGM = PlayerPrefs.GetFloat("longway.bgm");
            bgmMixer.audioMixer.SetFloat("BGMVolume", Mathf.Log(currentBGM) * 20);
            bgmSlider.value = currentBGM;
        }

        if( !PlayerPrefs.HasKey("longway.sfx")) PlayerPrefs.SetFloat("longway.sfx",1);
        if (sfxMixer) {
            float currentSFX = PlayerPrefs.GetFloat("longway.sfx");
            sfxMixer.audioMixer.SetFloat("SFXVolume", (Mathf.Log(currentSFX) * 20) + 4.0f);
            sfxSlider.value = currentSFX;
        }
    }

    public void SetInt(string key, int value) {
        PlayerPrefs.SetInt(key, value);
    }

    public void ChangeLanguage(int value) {
        Log.Text(value);
        PlayerPrefs.SetString("longway.lang", languageList[value].key);
    }

    public void SetBGMLevel(float value) {
        if(bgmMixer) {
            bgmMixer.audioMixer.SetFloat("BGMVolume", Mathf.Log(value) * 20);
            PlayerPrefs.SetFloat("longway.bgm", value);
        }
    }

    public void SetSFXLevel(float value) {
        if (sfxMixer) {
            sfxMixer.audioMixer.SetFloat("SFXVolume", (Mathf.Log(value) * 20) + 4.0f);
            PlayerPrefs.SetFloat("longway.sfx", value);
        }
    }
}
