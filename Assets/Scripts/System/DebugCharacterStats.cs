﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugCharacterStats : MonoBehaviour {
    public Transform framesBar;
    public Transform hpBar;
    public Transform lifeBar;
    public Transform attackBar;
    private BaseCharacter _character;
    
	// Use this for initialization
	void Start () {
        _character = FindParentCharacter();
	}
	
	// Update is called once per frame
	void Update () {
        if (_character) {
            int[] hitpoints = _character.Hitpoints();
            float[] iframes = _character.IFrames();
            float[] cooldown = _character.CooldownFrames();

            if (hpBar) {
                float hpPercent = (float)hitpoints[0] / hitpoints[1];
                hpBar.localScale = new Vector3(hpPercent, hpBar.localScale.y);
            }
            if (framesBar) {
                float framesPercent = (float)iframes[0] / iframes[1];
                framesBar.localScale = new Vector3(framesPercent, framesBar.localScale.y);
            }
            if (attackBar) {
                float attackPercent = (float)cooldown[0] / cooldown[1];
                attackBar.localScale = new Vector3(attackPercent, attackBar.localScale.y);
            }
            if (lifeBar && _character is BasePlayer) {
                int[] lives = (_character as BasePlayer).Lives();
                float livesPercent = (float)lives[0] / lives[1];
                lifeBar.localScale = new Vector3(livesPercent, lifeBar.localScale.y);
            }
        }
	}
    
    public BaseCharacter FindParentCharacter() {
        Transform t = transform;
        while (t.parent != null) {
            if (t.parent.GetComponent<BaseCharacter>()) {
                return t.parent.GetComponent<BaseCharacter>();
            }
            t = t.parent.transform;
        }
        return null; // Could not find a parent with given tag.
    }
}
