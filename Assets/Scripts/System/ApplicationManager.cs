﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationManager : MonoBehaviour {
    public GameEvent OnReturnToMenu;

    public void QuitGame() {
        Application.Quit();
    }

    public void ReturnToMenu() {
        if (OnReturnToMenu) OnReturnToMenu.Raise();
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void GoToSceneByName(string scene) {
         UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }

    public void SetTimeScale(float timescale) {
        Time.timeScale = timescale;
    }

    public void SetScreenMode(int option) {
        FullScreenMode mode = (FullScreenMode)option;
        if (mode != Screen.fullScreenMode) {
            Screen.fullScreenMode = mode;
        }
    }
}
