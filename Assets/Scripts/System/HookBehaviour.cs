﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HookBehaviour : ScriptableObject {
    public virtual void Run(MonoBehaviour callerObject) {
        Log.Text("<color=magenta>Running Hook", name, "for object", callerObject.name, "</color>");
     }
    public virtual void Run(MonoBehaviour callerObject, MonoBehaviour selfObject) { }
    public virtual void Remove(MonoBehaviour callerObject) { }
}
