﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnStateEnter : StateMachineBehaviour
{
    public AudioClip clip;
    // Start is called before the first frame update
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.transform.GetComponent<AudioSource>().PlayOneShot(clip);
    }
}
