﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpAttackBehaviour : StateMachineBehaviour {
    public float dashImpulse;
    public float jumpImpulse;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        Rigidbody2D _body = animator.gameObject.GetComponent<Rigidbody2D>();
        BaseCharacter _char = animator.gameObject.GetComponent<BaseCharacter>();

        float direction = animator.GetBool("FacingRight") ? 1 : -1;

        if (_body && _char) {
            _char.invulnerable = true;
            animator.SetFloat("AbsoluteVelocity", Mathf.Abs(dashImpulse));
            animator.SetBool("CanMove", false);
            _body.velocity = new Vector2(0, 0);
            Vector2 impulse = (animator.transform.right * dashImpulse * direction) + (animator.transform.up * jumpImpulse);
            _body.AddForce(impulse, ForceMode2D.Impulse);

        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        BaseCharacter _char = animator.gameObject.GetComponent<BaseCharacter>();
        if (_char) _char.invulnerable = false;
        animator.SetBool("CanMove", true);
        animator.SetBool("IsJumpAttack", false);
    }
}
