﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEventOnStateEnter : StateMachineBehaviour {
    public GameEvent triggeredEvent;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        if(triggeredEvent)
            triggeredEvent.Raise();
    }
}
