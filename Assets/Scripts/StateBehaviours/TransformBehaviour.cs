﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformBehaviour : StateMachineBehaviour {
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.ResetTrigger("Transform");
        animator.SetBool("Transforming", true);
        animator.SetBool("CanMove", false);

        Rigidbody2D _body = animator.gameObject.GetComponent<Rigidbody2D>();
        if(_body) {
            _body.velocity = Vector2.zero;
        }
        BaseCharacter character = animator.gameObject.GetComponent<BaseCharacter>();
        if (character) {
            character.invulnerable = true;
        }
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        
    }
    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("Transforming", false);
        animator.SetBool("CanMove", true);
        BaseCharacter character = animator.gameObject.GetComponent<BaseCharacter>();
        if (character) {
            character.invulnerable = false;
            character.ResetIframes();
        }
    }
}
