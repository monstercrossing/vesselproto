﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoboRyanTron.Unite2017.Variables;

public class StompStart : StateMachineBehaviour {
    public FloatVariable stompImpulse;
    private float _stompImpulse = 5;
    private CharacterEventHub eventHub;
    Rigidbody2D _body;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        eventHub = animator.transform.GetComponent<CharacterEventHub>();
        
        if (stompImpulse) _stompImpulse = stompImpulse.Value;
        _body = animator.gameObject.GetComponent<Rigidbody2D>();
        //animator.transform.GetOrAddComponent<CharacterEventHub>().onStompStart.Invoke(animator.GetComponent<BaseCharacter>());

        if (_body) {
            animator.SetFloat("AbsoluteVelocity", Mathf.Abs(_stompImpulse));
            animator.ResetTrigger("Stomp");
            animator.SetBool("Dashing", true);
            animator.SetBool("Stomping", true);
            animator.SetBool("CanMove", false);
            //_body.velocity = new Vector2(0, _body.velocity.y);
            float scale = 1 + eventHub.timeScale;
            
            _body.AddForce((animator.transform.up * -_stompImpulse) * scale, ForceMode2D.Impulse);
            
        }
    }

    void OnStateUpdate(Animator animator) {
        float scale = 1+eventHub.timeScale;
        //_body.velocity = new Vector2(0, _body.velocity.y);
        //_body.AddForce( (animator.transform.right * _dashImpulse * direction) * scale , ForceMode2D.Force);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        //animator.transform.GetOrAddComponent<CharacterEventHub>().onStompEnd.Invoke(animator.GetComponent<BaseCharacter>());
        animator.SetBool("Dashing", false);
        animator.SetBool("Stomping", false);
        
    }
}
