﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

public class JumpStart : StateMachineBehaviour {
    public FloatVariable jumpImpulse;
    public FloatVariable jumpImpulseModifier;
    public AudioClip jumpSound;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        Rigidbody2D _body = animator.gameObject.GetComponent<Rigidbody2D>();
        float direction = animator.GetBool("FacingRight") ? 1 : -1;
        if (_body) {
            int jumpCount = animator.GetInteger("JumpCount");
            animator.SetInteger("JumpCount", jumpCount + 1);
            if(jumpCount > 0)
                _body.velocity = new Vector2(_body.velocity.x, 0);

            float impulseMod = 1.0f;
            if (jumpCount > 0) impulseMod = jumpImpulseModifier.Value;
            
            if(animator.GetBool("IsClinging") || animator.GetBool("IsHuggingWall")) {
                Vector3 ximpulse = new Vector3(-direction, 1, 0);
                _body.velocity = new Vector2(0,0);
                 animator.gameObject.GetComponent<FlipSprite>().Flip(0.25f);
                _body.AddForce((ximpulse * jumpImpulse.Value * impulseMod) , ForceMode2D.Impulse);
                 animator.SetBool("WallJumping", true);
                 animator.SetBool("IsClinging", false);
                animator.ResetTrigger("ClingToWall");
            }else{
                _body.AddForce((animator.transform.up * jumpImpulse.Value * impulseMod) , ForceMode2D.Impulse);
            }
            animator.ResetTrigger("Jump");
            animator.SetBool("Jumping", true);
            
            SFXPlayer sfx = animator.gameObject.GetComponent<SFXPlayer>();
            if (sfx && jumpSound) {
                sfx.PlaySound(jumpSound);
            }
        }
    }

    void OnStateUpdate(Animator animator) {
        
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("Jumping", false);
        animator.SetBool("WallJumping", false);
        animator.SetBool("IsClinging", false);
    }
}
