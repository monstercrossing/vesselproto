﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockbackBehaviour : StateMachineBehaviour {
    private float drag;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        drag = animator.gameObject.GetComponent<Rigidbody2D>().drag;
        animator.gameObject.GetComponent<Rigidbody2D>().drag = 5;
        animator.SetBool("CanMove", false);
        animator.SetBool("KnockedBack", true);
        animator.ResetTrigger("Knockback");
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.gameObject.GetComponent<Rigidbody2D>().drag = drag;
        animator.SetBool("CanMove", true);
        animator.SetBool("KnockedBack", false);
    }
}
