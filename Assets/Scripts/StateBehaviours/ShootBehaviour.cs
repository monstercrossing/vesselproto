﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

public class ShootBehaviour : StateMachineBehaviour {
    //public FloatVariable shootingSpeedMultiplier;
    private GameObject projectile;
    private float _shootSpeed = 1;
    public bool doComboReset = true;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        // if (shootingSpeedMultiplier) _shootSpeed = shootingSpeedMultiplier.Value;
        Log.Text("<color=green>Begin  OnStateEnter for Shoot</color>");
        animator.ResetTrigger("Shoot");
        animator.SetBool("Shooting", true);
        animator.SetBool("Attacking", true);

        if (!animator.GetBool("CanAttackWhileMoving")) {
           // Rigidbody2D _body = animator.gameObject.GetComponent<Rigidbody2D>();
            //if (_body) _body.velocity = Vector2.zero;

            animator.SetBool("CanMove", false);
        }

        int combo = animator.GetInteger("ComboCount");

        if (doComboReset)
            animator.SetInteger("ComboCount", 0);
        else
            animator.SetInteger("ComboCount", combo + 1);

        Log.Text("<color=green>Done OnStateEnter for Shoot</color>");

        BaseCharacter character = animator.gameObject.GetComponent<BaseCharacter>();
        if(character) {
            character.PostAnimShoot();
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("Shooting", false);
        animator.SetBool("CanMove", true);
        animator.SetBool("Attacking", false);

    }
}
