﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAndUnsetBoolStateBehaviour : StateMachineBehaviour {
    public string parameter;
    public bool entryState;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool(parameter, entryState);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool(parameter, !entryState);
    }
}