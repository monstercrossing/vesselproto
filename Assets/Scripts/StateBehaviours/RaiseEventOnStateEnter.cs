﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaiseEventOnStateEnter : StateMachineBehaviour
{
    public GameEvent gameEvent;
    // Start is called before the first frame update
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        gameEvent.Raise();
    }   
}
