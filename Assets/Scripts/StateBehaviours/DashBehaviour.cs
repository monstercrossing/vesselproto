﻿using System.Collections;
using System.Collections.Generic;
using RoboRyanTron.Unite2017.Variables;
using UnityEngine;

public class DashBehaviour : StateMachineBehaviour {
    public FloatVariable dashImpulse;
    public int MaxAirDashCount = 1;
    private float _dashImpulse = 5;
    private CharacterEventHub eventHub;
    Rigidbody2D _body;

    public bool continuousForce = false;
    public bool unsetGravity = false;
    private float originalGravity;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        eventHub = animator.transform.GetComponent<CharacterEventHub>();
        if (!animator.GetBool("IsGrounded")) {
            int count = animator.GetInteger("AirDashCount");
            count++;
            animator.SetInteger("AirDashCount", count);
            if (count > MaxAirDashCount) return;
        }

        if (dashImpulse) _dashImpulse = dashImpulse.Value;
        _body = animator.gameObject.GetComponent<Rigidbody2D>();
        if(unsetGravity) {
            originalGravity = _body.gravityScale;
            _body.gravityScale = 0;
        }

        animator.transform.GetOrAddComponent<CharacterEventHub>().onDashStart.Invoke(animator.GetComponent<BaseCharacter>());

        float direction = animator.GetBool("FacingRight") ? 1 : -1;

        if (_body) {
            animator.SetFloat("AbsoluteVelocity", Mathf.Abs(_dashImpulse));
            animator.ResetTrigger("Dash");
            animator.SetBool("Dashing", true);
            animator.SetBool("CanMove", false);

            

            //_body.velocity = new Vector2(0, _body.velocity.y);
            float scale = 1 + eventHub.timeScale;
            
            _body.AddForce((animator.transform.right * _dashImpulse * direction) * scale, ForceMode2D.Impulse);
            
        }
    }

    void OnStateUpdate(Animator animator) {
        float scale = 1/eventHub.timeScale;
        float direction = animator.GetBool("FacingRight") ? 1 : -1;

        if (continuousForce) {
            _body.velocity = new Vector2(0, _body.velocity.y);
            _body.AddForce( (animator.transform.right * _dashImpulse * direction) * scale , ForceMode2D.Force);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.transform.GetOrAddComponent<CharacterEventHub>().onDashEnd.Invoke(animator.GetComponent<BaseCharacter>());
        animator.SetBool("Dashing", false);
        animator.SetBool("CanMove", true);

        if (unsetGravity) {
            _body.gravityScale = originalGravity;
        }
    }
}
