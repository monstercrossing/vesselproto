﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StompEnd : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("PostStomping", true);
        Rigidbody2D _body = animator.gameObject.GetComponent<Rigidbody2D>();

        if (_body) {
                _body.velocity = Vector2.zero;
        }
    }
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("CanMove", true);
        animator.SetBool("PostStomping", false);

    }
}
