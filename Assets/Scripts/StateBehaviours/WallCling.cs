﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCling : StateMachineBehaviour
{
    public float drag = 8;
    private float originalGravity;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //originalGravity = animator.gameObject.GetComponent<Rigidbody2D>().gravityScale;
        animator.gameObject.GetComponent<Rigidbody2D>().drag = drag;
        animator.ResetTrigger("ClingToWall"); 
        animator.SetBool("IsClinging", true);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.gameObject.GetComponent<Rigidbody2D>().drag = 0;
        //animator.SetBool("IsClinging", false);
    }
}
