﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckFalling : StateMachineBehaviour
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.ResetTrigger("Jump");
        animator.SetBool("Falling", true);
        animator.SetBool("IsClinging", false);

        int jumpCount = animator.GetInteger("JumpCount");
        if(jumpCount == 0)
         animator.SetInteger("JumpCount", jumpCount + 1);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("Falling", false);
    }
}