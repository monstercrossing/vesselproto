﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareAttack : StateMachineBehaviour {
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("PreparingAttack", true);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        animator.SetBool("PreparingAttack", false);

    }
}
