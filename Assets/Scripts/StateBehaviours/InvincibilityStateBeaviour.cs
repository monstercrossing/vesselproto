﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityStateBeaviour : StateMachineBehaviour {
    BaseCharacter character;
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        character = animator.transform.GetOrAddComponent<BaseCharacter>();
        character.invulnerable = true;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        character.invulnerable = false;
    }
}