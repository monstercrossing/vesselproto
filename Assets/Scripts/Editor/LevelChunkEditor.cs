﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelChunk))]
public class LevelChunkEditor : Editor {
    
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if(GUILayout.Button("Mutate")){
            ((LevelChunk)serializedObject.targetObject).MutateChunk();
        }
    }

}
