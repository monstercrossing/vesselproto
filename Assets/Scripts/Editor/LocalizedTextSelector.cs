﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MonsterCrossing.System.UI;
using System.Reflection;

[CustomPropertyDrawer(typeof(LocalizedTextAttribute))]
public class LocalizedTextSelector : PropertyDrawer {
    private LocalizationTemplate _template;
    public string translatedString;
    private int index = 0;
    SerializedProperty _propTemplate;
    
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        _propTemplate = property.serializedObject.FindProperty("template");
        //GUILayout.BeginHorizontal();
        //EditorGUI.PropertyField(position, _propTemplate);
        if (_propTemplate != null) {

            //EditorGUI.BeginProperty(position, label, property);

            EditorGUI.LabelField(position, property.displayName);
            EditorGUI.indentLevel = 10;

            /*
            LocalizedTextRenderer _renderer = _propTemplate.serializedObject.targetObject as LocalizedTextRenderer;
            _template = _renderer.template as LocalizationTemplate;
            StringTable _table = _renderer.language;
            */
            _template = _propTemplate.serializedObject.FindProperty("template").objectReferenceValue as LocalizationTemplate;
            StringTable _table = _propTemplate.serializedObject.FindProperty("language").objectReferenceValue as StringTable;

            //SerializedProperty stringIndex = _propTemplate.serializedObject.FindProperty("stringIndex");
            SerializedProperty stringIndex = property;
            //SerializedProperty translatedString = _propTemplate.serializedObject.FindProperty("translatedString");
            if (_template != null && _table != null) {
                string[] labels = new string[_template.stringKeys.Length];
                for (int i = 0; i < labels.Length; i++) {
                    labels[i] = _template.stringKeys[i] + ": " +_table.translatedTexts[i];
                }
                stringIndex.intValue = EditorGUI.Popup(position, stringIndex.intValue, labels);
                
                //translatedString.stringValue = _table.translatedTexts[index];
            } else {
                //_renderer.translatedString = "";
                //_propTemplate.serializedObject.FindProperty("translatedString").stringValue = "";
            }

            //LocalizedTextAttribute attrib = this.attribute as LocalizedTextAttribute;
            _propTemplate.serializedObject.ApplyModifiedProperties();
            //EditorGUI.EndProperty();
            
        } else {
            EditorGUI.PropertyField(position, property, label);
        }
       // GUILayout.EndHorizontal();
    }
 }