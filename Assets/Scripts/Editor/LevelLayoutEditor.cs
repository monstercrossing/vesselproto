﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelLayoutDefinition))]
public class LevelLayoutEditor : Editor {

    Material mat;
    Texture2D _texture;
    LevelLayoutDefinition level;
    int TEX_WID = 96;
    int TEX_HEI = 54;
    /*
    Vector2[] directions = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, -1), new Vector2(-1, 0),
                                          new Vector2( -1, -1 ),  new Vector2( 1, -1 ),
                                           new Vector2( -1, 1),  new Vector2( 1, 1 ) };
                                           */

    List<Vector2Int> directions;


    private void OnEnable() {
        var shader = Shader.Find("Hidden/Internal-Colored");
        mat = new Material(shader);

        directions = new List<Vector2Int>{ new Vector2Int(0, 1), new Vector2Int(1, 0), new Vector2Int(0, -1), new Vector2Int(-1, 0),
                                          new Vector2Int(-1, -1), new Vector2Int(1, -1),
                                           new Vector2Int(-1, 1), new Vector2Int(1, 1) };

        RenderMap();
    }
    private void OnDisable() {
        DestroyImmediate(mat);
        DestroyImmediate(_texture);
    }

    public override void OnInspectorGUI() {
        if (!level.generated) level.GenerateMap();
        DrawDefaultInspector();
        GUILayout.BeginHorizontal(EditorStyles.helpBox);

        // Reserve GUI space with a width from 10 to 10000, and a fixed height of 200, and 
        // cache it as a rectangle.
        Rect layoutRectangle = GUILayoutUtility.GetAspectRect(1.777777777777778f);

        if(_texture)
        EditorGUI.DrawPreviewTexture(layoutRectangle, _texture);
        // End our horizontal 
        GUILayout.EndHorizontal();
        if(GUILayout.Button("GenerateMap")){
            level.GenerateMap();
            RenderMap();
        }
    }

    public void RenderMap() {
        level = (LevelLayoutDefinition)serializedObject.targetObject;
        RoomDefinition[,] rooms = level.rooms;
        int levelWidth = (int)level.levelSize.x;
        int levelHeight = (int)level.levelSize.y;

        //_texture = (Texture2D)serializedObject.FindProperty("texture").objectReferenceValue;
        _texture = new Texture2D(TEX_WID, TEX_HEI);
        _texture.anisoLevel = 0;
        _texture.filterMode = FilterMode.Point;
        Color[] colors = new Color[TEX_WID * TEX_HEI];
        //for (int i = 0; i < TEX_WID * TEX_HEI; i++)
        //    colors[i] = Color.black;
        int index;
        for (int i = 1, li = 0; i < TEX_WID && li < levelWidth; i += 3, li++) {
            for (int j = 1, lj = 0; j < TEX_HEI && lj < levelHeight; j += 3, lj++) {
                RoomDefinition currentRoom = rooms[li, lj];

                
                for (int dir = 0; dir < directions.Count; dir++) {
                    index = (i + (int)directions[dir].x) + ((j + (int)directions[dir].y) * TEX_WID);

                    if(dir < 4 && currentRoom.openings[dir])
                        colors[index] = Color.green;
                    else
                        colors[index] = Color.white;
                }

                Color colorToPaint;
                if ((currentRoom.flags & RoomDefinition.RoomFlags.Starting) != 0) {
                    colorToPaint = Color.yellow;
                }else if( (currentRoom.flags & RoomDefinition.RoomFlags.Ending) != 0) {
                    colorToPaint = Color.red;
                }else if( (currentRoom.flags & RoomDefinition.RoomFlags.MainPath) != 0) {
                    colorToPaint = Color.green;
                } else {
                    colorToPaint = Color.black;
                }
                index = i + (j * TEX_WID);
                colors[index] = colorToPaint;
            }
        }
        _texture.SetPixels(colors);
        _texture.Apply();

        
    }
}
