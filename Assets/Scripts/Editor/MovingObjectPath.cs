﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MovingObject)), CanEditMultipleObjects]    
public class MovingObjectPath : Editor {
    public int activationMode;
    protected virtual void OnSceneGUI() {
        MovingObject movingObject = (MovingObject)target;

        Vector3 start;
        Vector3 end;
        Handles.color = Color.yellow;

        if (movingObject.waypoints == null || movingObject.waypoints.Length == 0) return;
        for (int i = 0; i < movingObject.waypoints.Length; i++) {
            if (i == 0) end = movingObject.transform.position;
            else end = movingObject.transform.TransformPoint(movingObject.waypoints[i - 1]);
            start = movingObject.transform.TransformPoint(movingObject.waypoints[i]);
            Handles.DrawDottedLine(start, end, 5);

            EditorGUI.BeginChangeCheck();
            Vector3 newTargetPosition = Handles.PositionHandle(movingObject.transform.TransformPoint(movingObject.waypoints[i]), Quaternion.identity);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(movingObject, "Change Look At Target Position");
                movingObject.waypoints[i] = movingObject.transform.InverseTransformPoint(newTargetPosition);
                movingObject.Update();
            }
        }

        if (movingObject.triggerObject && movingObject._isTriggered) {
            Handles.color = Color.blue;
            Handles.DrawDottedLine(movingObject.transform.position, movingObject.triggerObject.transform.position, 5);
        }
    }

    public override void OnInspectorGUI() {
        MovingObject movingObject = (MovingObject)target;

        EditorGUI.BeginChangeCheck();
        float speed = EditorGUILayout.FloatField("Default speed", movingObject.speed);
        if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Changed Activation Mode");
            movingObject.speed = speed;
        }

        EditorGUI.BeginChangeCheck();
        float delay = EditorGUILayout.FloatField("Default wait time", movingObject.delay);
        if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Changed Activation Mode");
            movingObject.delay = delay;
        }

        EditorGUI.BeginChangeCheck();
        bool collision = EditorGUILayout.ToggleLeft("Can't collide with terrain?", movingObject.returnOnCollision);
        if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Changed Activation Mode");
            movingObject.returnOnCollision = collision;
        }

        string[] options = new string[]
        {
            "Always On", "Triggered Once", "Continuous After Trigger",
        };
        EditorGUILayout.Space();

        activationMode = movingObject.activationMode;
        EditorGUI.BeginChangeCheck();
        int mode = EditorGUILayout.Popup("Activation Mode", movingObject.activationMode, options);
        
        if (EditorGUI.EndChangeCheck()) {
            Undo.RegisterCompleteObjectUndo(target, "Changed Activation Mode");
            Undo.FlushUndoRecordObjects();
            movingObject.activationMode = mode;
            // activationMode = movingObject.activationMode;
        }
        
        if (movingObject.activationMode == 0) {
            movingObject.continuousAfterTrigger = false;
            movingObject._isTriggered = false;

            EditorGUI.BeginChangeCheck();
            bool cycle = EditorGUILayout.ToggleLeft("Cycle At End?", movingObject.cycleAtEnd);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Cycle Mode");
                movingObject.cycleAtEnd = cycle;
            }

            EditorGUI.BeginChangeCheck();
            bool wait = EditorGUILayout.ToggleLeft("Wait at each waypoint?", movingObject.stopAtEachWaypoint);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Wait Mode");
                movingObject.stopAtEachWaypoint = wait;
            }

        } else if (movingObject.activationMode == 1) {
            movingObject.continuousAfterTrigger = false;
            movingObject._isTriggered = true;
            SerializedProperty m_GameObjectProp = serializedObject.FindProperty("triggerObject");

            EditorGUILayout.PropertyField(m_GameObjectProp, new GUIContent("Trigger Object"));

            EditorGUI.BeginChangeCheck();
            bool stay = EditorGUILayout.ToggleLeft("Stay open after triggered?", movingObject.stayOpenOnTrigger);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Stay Mode");
                movingObject.stayOpenOnTrigger = stay;
            }

            EditorGUI.BeginChangeCheck();
            float fspeed = EditorGUILayout.FloatField("Activation speed", movingObject.triggeredSpeed);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Activation Speed");
                movingObject.triggeredSpeed = fspeed;
            }
        }
        else if (movingObject.activationMode == 2) {
            SerializedProperty m_GameObjectProp = serializedObject.FindProperty("triggerObject");

            EditorGUILayout.PropertyField(m_GameObjectProp, new GUIContent("Trigger Object"));

            EditorGUI.BeginChangeCheck();
            bool cycle = EditorGUILayout.Toggle("Cycle At End?", movingObject.cycleAtEnd);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Cycle Mode");
                movingObject.cycleAtEnd = cycle;
            }

            EditorGUI.BeginChangeCheck();
            bool wait = EditorGUILayout.ToggleLeft("Wait at each waypoint?", movingObject.stopAtEachWaypoint);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Wait Mode");
                movingObject.stopAtEachWaypoint = wait;
            }

            movingObject.continuousAfterTrigger = true;
            movingObject._isTriggered = true;
        }

        EditorGUILayout.Space();
        SerializedProperty waypoints = serializedObject.FindProperty("waypoints");
        EditorGUILayout.PropertyField(waypoints, new GUIContent("Waypoints"), true);

        serializedObject.ApplyModifiedProperties();
    }
}
