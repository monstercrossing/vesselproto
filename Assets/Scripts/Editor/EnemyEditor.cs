﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DefaultEnemyTasks)), CanEditMultipleObjects]
public class EnemyEditor : Editor {

    protected virtual void OnSceneGUI() {

    }

    public override void OnInspectorGUI() {
        DefaultEnemyTasks enemy = (DefaultEnemyTasks)target;
        SerializedProperty _view = serializedObject.FindProperty("viewRangeConstant");
        SerializedProperty _patrol = serializedObject.FindProperty("patrolRangeConstant");
        SerializedProperty _hear = serializedObject.FindProperty("hearingRangeConstant");
        SerializedProperty _shoot = serializedObject.FindProperty("shootRangeConstant");
        SerializedProperty _event = serializedObject.FindProperty("OnAttackAlert");

        EditorGUILayout.PropertyField(_view, new GUIContent("View Range (Constant)"));
        if (!enemy.viewRangeConstant) {
            EditorGUI.BeginChangeCheck();
            float view = EditorGUILayout.FloatField("View Range", enemy.viewRange);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed View Range");
                enemy.viewRange = view;
            }
        } else {
            EditorGUILayout.FloatField("View Range value", enemy.viewRangeConstant.Value);
        }

        EditorGUILayout.PropertyField(_shoot, new GUIContent("Shoot Range (Constant)"));
        if (!enemy.shootRangeConstant) {
            EditorGUI.BeginChangeCheck();
            float shoot = EditorGUILayout.FloatField("Shoot Range", enemy.shootRange);
            if(EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Shoot Range");
                enemy.shootRange = shoot;
            }
        } else {
            EditorGUILayout.FloatField("Shoot Range value", enemy.shootRangeConstant.Value);
        }

        EditorGUILayout.PropertyField(_patrol, new GUIContent("Patrol Range (Constant)"));
        if (!enemy.patrolRangeConstant) {
            EditorGUI.BeginChangeCheck();
            float patrol = EditorGUILayout.FloatField("Patrol Range", enemy.patrolRange);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Patrol Range");
                enemy.patrolRange = patrol;
            }
        } else {
            EditorGUILayout.FloatField("Patrol Range value", enemy.patrolRangeConstant.Value);
        }

        EditorGUILayout.PropertyField(_hear, new GUIContent("Hearing Range (Constant)"));
        if (!enemy.hearingRangeConstant) {
            EditorGUI.BeginChangeCheck();
            float patrol = EditorGUILayout.FloatField("Hearing Range", enemy.hearingRange);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Hearing Range");
                enemy.hearingRange = patrol;
            }
        } else {
            EditorGUILayout.FloatField("Hearing Range value", enemy.hearingRangeConstant.Value);
        }

        EditorGUILayout.PropertyField(_event, new GUIContent("On Atack Alert Event"));

        EditorGUI.BeginChangeCheck();
        float chance = EditorGUILayout.FloatField("Chance to shoot", enemy.chanceToShoot);
        if(EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Changed Chance to shoot");
            enemy.chanceToShoot = chance;
        }

        EditorGUI.BeginChangeCheck();
        float stamina = EditorGUILayout.FloatField("Attack Stamina", enemy._maxAttackStamina);
        if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Changed Attack Stamina");
            enemy._maxAttackStamina = stamina;
        }

        serializedObject.ApplyModifiedProperties();
    }

}
