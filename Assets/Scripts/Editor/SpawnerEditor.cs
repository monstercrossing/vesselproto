﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpawnPoint)), CanEditMultipleObjects]
public class SpawnerEditor : Editor {

    private bool listVisibility = true;

    public override void OnInspectorGUI() {
        SpawnPoint _spawn = (SpawnPoint)target;

        SerializedProperty m_GameObjectProp = serializedObject.FindProperty("prefabOrInstance");
        EditorGUILayout.PropertyField(m_GameObjectProp, new GUIContent("Prefab or GameObject"));

        SerializedProperty m_ListProp = serializedObject.FindProperty("prefabList");
        EditorGUILayout.PropertyField(m_ListProp, new GUIContent("List of Prefab or GameObject"));

        SerializedProperty m_ParentProp = serializedObject.FindProperty("parentObj");
        EditorGUILayout.PropertyField(m_ParentProp, new GUIContent("Parent Object to Append"));

        string[] options = new string[]
            {
                "Spawn Once", "Continuously Spawn", "Spawn on Trigger"
            };


        EditorGUI.BeginChangeCheck();
        int mode = EditorGUILayout.Popup("Spawn Mode", _spawn.spawnMode, options);
        if (EditorGUI.EndChangeCheck()) {
            Undo.RecordObject(target, "Changed Spawn Mode");
            _spawn.spawnMode = mode;
        }
        EditorGUILayout.Space();
        if (_spawn.spawnMode > 0) {
            EditorGUILayout.HelpBox(
                new GUIContent("Maximum number of simultaneous enemies - zero for no limit")
                );

            EditorGUI.BeginChangeCheck();
            int maxe = EditorGUILayout.IntField("Max Simultaneous Enemies", _spawn.maxOfSimultaneousEnemies);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Max Enemies");
                _spawn.maxOfSimultaneousEnemies = maxe;
            }

            EditorGUI.BeginChangeCheck();
            float sprob = EditorGUILayout.FloatField("Probability of Spawn", _spawn.spawnProb);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Changed Pabability of Spawn");
                _spawn.spawnProb = sprob;
            }
        }
        if (_spawn.spawnMode == 1) {
            EditorGUI.BeginChangeCheck();
            float wait = EditorGUILayout.FloatField("Wait between spawns", _spawn.spawnWait);
            if (EditorGUI.EndChangeCheck()) {
                Undo.RecordObject(target, "Changed Spawn Wait");
                _spawn.spawnWait = wait;
            }
        }
        if (_spawn.spawnMode == 2) {
            SerializedProperty trigger = serializedObject.FindProperty("triggerObject");

            EditorGUILayout.PropertyField(trigger, new GUIContent("Trigger Object"));
        }

        serializedObject.ApplyModifiedProperties();
    }
}
