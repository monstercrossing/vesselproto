﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MonsterCrossing.System.UI;

[CustomEditor(typeof(StringTable)), CanEditMultipleObjects]
public class StringTableEditor : Editor {

    private SerializedProperty _template;
    private SerializedProperty _array;
    private StringTable table;
    private LocalizationTemplate templateObject;

    private void OnEnable() {
        table = (StringTable)target;
        _template = serializedObject.FindProperty("template");
        _array = serializedObject.FindProperty("translatedTexts");
        templateObject = table.template;
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();

        table.language = EditorGUILayout.TextField("Language", table.language);

        EditorGUILayout.PropertyField(_template, new GUIContent("Language Template"));

        int listSize = 0;
        if (templateObject)
            listSize = templateObject.stringKeys.Length;
        ///*
        if (listSize != _array.arraySize) {
            while (listSize > _array.arraySize) {
                _array.InsertArrayElementAtIndex(_array.arraySize);
            }
            while (listSize < _array.arraySize) {
                _array.DeleteArrayElementAtIndex(_array.arraySize - 1);
            }
        }
        //*/

        //Show keys from template;

        EditorGUILayout.Space();
        for(int i=0; i < _array.arraySize; i++) {
            SerializedProperty _element = _array.GetArrayElementAtIndex(i);
            EditorGUILayout.PropertyField(_element, new GUIContent(templateObject.stringKeys[i]));
        }

        serializedObject.ApplyModifiedProperties();
    }   
}
