﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourChangePowerup : MonoBehaviour {
    public PowerupDefinition newPowerup;

    public void ChangePowerup() {
        BaseCharacter character = GetComponent<BaseCharacter>();
        if(character && newPowerup) {
            character.ReceivePowerup(newPowerup);
        }
    }
}
