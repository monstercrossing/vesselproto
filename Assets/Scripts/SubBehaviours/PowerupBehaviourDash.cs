﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupBehaviourDash : MonoBehaviour {
    public float dashImpulse;

    private void Start() {
        BaseAttack attack = GetComponent<BaseAttack>();
        BaseCharacter character = attack.GetOwner();
        if (character) {
            Rigidbody2D _body = character.gameObject.GetComponent<Rigidbody2D>();

            float direction = character.GetComponent<Animator>().GetBool("FacingRight") ? 1 : -1;

            if (_body) {
                 character.GetComponent<Animator>().SetFloat("AbsoluteVelocity", Mathf.Abs(dashImpulse));
                _body.velocity = new Vector2(0, _body.velocity.y);
                _body.AddForce(character.transform.right * dashImpulse * direction, ForceMode2D.Impulse);

            }
        }
    }
}
