﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public static class Log {
    public static bool debugMode = true;

    public static string Text(params object[] data) {
        if (!debugMode) return "false";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.Length; i++) {
            sb.Append(data[i].ToString());
            sb.Append("\t");
        }
        string s = sb.ToString();
        Debug.Log(s);
        return s;
    }
}