// Version 1.0
// By Alexander Ocias
// https://ocias.com

Shader "Ocias/Pixel Art Sprite Swap"
{
	Properties
	{
		[PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_PaletteTex("Palette", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		_palsize ("Palette Size", Float) = 4
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
            #pragma target 4.5
            #pragma only_renderers metal
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR0;
				half2 texcoord  : TEXCOORD1;
				float2 uv : TEXCOORD2;
			};
			
			fixed4 _Color;
			uniform float PIXELS_PER_UNIT;

			inline float4 ViewSpacePixelSnap (float4 pos) {
				float pixelSize = _ScreenParams.xy;
				float2 halfScreenRes = _ScreenParams.xy * 0.5f;

				// // View space Pixel Snapping
				float2 pixelPos = floor(pos * halfScreenRes + 1 / halfScreenRes) / halfScreenRes; // put back in that half pixel offset when you're done
				pos.xy = pixelPos;
				
				// Odd resolution handling
				float2 odd = _ScreenParams.xy % 2;
				pos.x += odd.x * 0.5 / halfScreenRes.x;
				pos.y += odd.y * 0.5 / halfScreenRes.y;

				return pos;
			}

			inline float4 WorldSpacePixelSnap (float4 pos) {

				//float ppu = _PixelsPerUnit;
				// float zoom = unity_OrthoParams.y;
				// float ppu = _ScreenParams.y / zoom / 2;
				float ppu = PIXELS_PER_UNIT;

				pos = mul(unity_ObjectToWorld, pos);
				

				// World space Pixel Snapping
				pos = floor(pos * ppu + 1 / ppu) / ppu;
				// Adjust to pixel relative to camera position
				// float3 snappedCameraPosition = floor(_WorldSpaceCameraPos * ppu + 1 / ppu) / ppu;
				// float3 cameraSubpixelOffset = snappedCameraPosition - _WorldSpaceCameraPos;
				// pos.x -= cameraSubpixelOffset.x;
				// pos.y -= cameraSubpixelOffset.y;
				// Odd resolution handling
				float2 odd = _ScreenParams.xy % 2;
				pos.x += odd.x * 0.5 / ppu;
				pos.y += odd.y * 0.5 / ppu;

				pos = mul(unity_WorldToObject, pos);

				return pos;
			}
            
			v2f vert(appdata_t vertIn)
			{
                
				v2f vertOut = (v2f)0;
                UNITY_INITIALIZE_OUTPUT(v2f, vertOut);
                vertIn.vertex = WorldSpacePixelSnap(vertIn.vertex);

                // Offset position based on distance from camera to nearest pixel
                // float zoom = unity_OrthoParams.y;
                // float ppu = _ScreenParams.y / zoom / 2;
                float ppu = PIXELS_PER_UNIT;
                float3 snappedCameraPosition = floor(_WorldSpaceCameraPos * ppu + 1 / ppu) / ppu;
                float3 cameraSubpixelOffset = snappedCameraPosition - _WorldSpaceCameraPos;
                vertIn.vertex.x -= cameraSubpixelOffset.x;
                vertIn.vertex.y -= cameraSubpixelOffset.y;

                vertOut.vertex = UnityObjectToClipPos(vertIn.vertex);
                #ifdef UNITY_HALF_TEXEL_OFFSET
                vertOut.vertex.x -= (float)1 / _ScreenParams.x;
                vertOut.vertex.y += (float)1 / _ScreenParams.y;
                #endif
                vertOut.texcoord = vertIn.texcoord;
                /*
                */
                //vertOut.color = vertIn.color;
                //OUT.vertex = ViewSpacePixelSnap(OUT.vertex);
                vertOut.color = vertIn.color * _Color;
				return vertOut;
			}

			sampler2D _MainTex;
			sampler2D _PaletteTex;
			float4 _PaletteTex_ST;
			float _palsize;

			fixed4 frag(v2f fragIn) : SV_Target
			{
                half4 c = tex2D(_MainTex, fragIn.texcoord) * fragIn.color;
                c.rgb *= c.a;
                
                float paletteX = tex2D(_MainTex, fragIn.texcoord).r;
                float2 paletteUV = float2(paletteX, 0.0f);
                // Get the palette's UV accounting for texture tiling and offset
                float2 paletteUVTransformed = TRANSFORM_TEX(paletteUV, _PaletteTex);
                
                // Get the color from the palette key
                fixed4 outColor = tex2D(_PaletteTex, paletteUVTransformed);
                outColor.rgb *= c.a;
                outColor.a = c.a;
                // Apply the tint to the final color
                outColor *= fragIn.color;
                return outColor;
                return c;
			}
		ENDCG
		}
	}
}